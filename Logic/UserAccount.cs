﻿using System.Collections.Generic;

namespace Quaestor.Logic
{
    /// <summary>
    ///     An value account that contains additional user information.
    /// </summary>
    public class UserAccount : ValueAccount
    {
        /// <summary>
        ///     Constructor method for an user account.
        /// </summary>
        /// <param name="startValue">The value of the account that cannot be found within the transactions</param>
        /// <param name="planValue">The planned value for the account</param>
        /// <param name="name">The name of the account</param>
        /// <param name="accountType">The type of the account</param>
        /// <param name="referenceSystem">The list of transactions that the account adds up</param>
        /// <param name="printToOverview">Decides wheter the account should be printed to the overview</param>
        /// <param name="email">The email-adress of the user</param>
        public UserAccount(int startValue, int planValue, string name, AccountTypes accountType,
            List<Transaction> referenceSystem, bool printToOverview, string email)
            : base(startValue, planValue, name, accountType, referenceSystem, printToOverview)
        {
            Email = email;
        }

        /// <summary>
        ///     The email-adress of the user.
        /// </summary>
        public string Email { get; private set; }
    }
}