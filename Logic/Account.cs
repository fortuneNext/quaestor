﻿using System;

namespace Quaestor.Logic
{
    /// <summary>
    ///     An account adds up all values of transactions for the specified account or its subaccounts.
    /// </summary>
    public abstract class Account
    {
        /// <summary>
        ///     Creates an account with given information
        /// </summary>
        /// <param name="name">The account name</param>
        /// <param name="accountType">The account type, either "Active", "Passive" or "User"</param>
        /// <param name="printToOverview">True if the account should be printed</param>
        protected Account(string name, AccountTypes accountType, bool printToOverview)
        {
            Name = name;
            AccountType = accountType;
            PrintToOverview = printToOverview;
        }

        //TODO NEW_ERA
        /**
         * GetValue, GetValue(DateTime) und GetStartValue aller Account Typen müssen so verändert werden,
         * dass StartValue durch die neue LegacyTransaction ersetzt wird
         * 
         * StructuralAccount Fertig (muss nichts gemacht werden)
         * UserAccount Fertig (muss nichts gemacht werden)
         * ValueAccount ??
         */

        /// <summary>
        ///     True if account should be printed on overview page.
        /// </summary>
        public bool PrintToOverview { get; private set; }

        /// <summary>
        ///     The name of the account.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        ///     The type of the account.
        /// </summary>
        public AccountTypes AccountType { get; private set; }

        /// <summary>
        ///     Returns the value of the account.
        /// </summary>
        /// <returns>The value of the account</returns>
        public abstract int GetValue();


        /// <summary>
        ///     Returns the Startvalue of the account; exists so that old saves without a LegacyTransaction can correctly be loaded into the program
        /// </summary>
        /// <returns>the account value with start value</returns>
        public abstract int GetStartValueOldSave();

        /// <summary>
        ///     Returns the value of the account at a specific date.
        /// </summary>
        /// <param name="until">The last date that will be counted</param>
        /// <returns>The value of the account at the specified date</returns>
        public abstract int GetValue(DateTime until);

        /// <summary>
        ///     Returns the plan value of the account.
        /// </summary>
        /// <returns>The plan value of the account</returns>
        public abstract int GetPlanValue();

        /// <summary>
        ///     Returns the start value of an account.
        /// </summary>
        /// <returns>THe start value of the account</returns>
        public abstract int GetStartValue();
    }
}