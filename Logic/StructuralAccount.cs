﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Quaestor.Logic
{
    /// <summary>
    ///     An account that adds up all values of its subaccounts.
    /// </summary>
    public class StructuralAccount : Account
    {
        /// <summary>
        ///     Constructor method for a structural account.
        /// </summary>
        /// <param name="name">The name of the account</param>
        /// <param name="subaccounts">The list of subaccounts this account adds up</param>
        /// <param name="accountType">The type of the account</param>
        /// <param name="printToOverview">Decides wheter the account should be printed to the overview</param>
        public StructuralAccount(string name, List<Account> subaccounts, AccountTypes accountType, bool printToOverview)
            : base(name, accountType, printToOverview)
        {
            Subaccounts = subaccounts;
        }

        /// <summary>
        ///     Constructor method for a structural account.
        /// </summary>
        /// <param name="name">The name of the account</param>
        /// <param name="accountType">The type of the account</param>
        /// <param name="printToOverview">Decides wheter the account should be printed to the overview</param>
        public StructuralAccount(string name, AccountTypes accountType, bool printToOverview)
            : base(name, accountType, printToOverview)
        {
            Subaccounts = new List<Account>();
        }

        /// <summary>
        ///     The list of subaccounts this account adds up.
        /// </summary>
        public List<Account> Subaccounts { get; private set; }

        /// <summary>
        ///     Adds a new subaccount for this structural account.
        /// </summary>
        /// <param name="account">New subaccount for this structural account</param>
        public void AddAccount(Account account)
        {
            Subaccounts.Add(account);
            Subaccounts = Subaccounts.OrderBy(o => o.Name).ToList();
        }

        /// <summary>
        ///     Removes a subaccount for this structural account.
        /// </summary>
        /// <param name="account">The account that will be removed from this structural account</param>
        /// <returns>True if the account was found and removed; false otherwise</returns>
        private bool RemoveAccount(Account account)
        {
            foreach (Account a in Subaccounts.Where(a => a.Equals(account)))
            {
                Subaccounts.Remove(a);
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Returns the cummulated values of all subaccounts of this account.
        /// </summary>
        /// <returns>The value of the account</returns>
        public override int GetValue()
        {
            return Subaccounts.Sum(account => account.GetValue());
        }

        /// <summary>
        ///     Returns the cummulated (obsolete)start values of all subaccounts of this account.
        /// </summary>
        /// <returns>The start value of the account</returns>
        public override int GetStartValueOldSave()
        {
            return Subaccounts.Sum(account => account.GetStartValueOldSave());
        }

        /// <summary>
        ///     Returns the cummulated values of all subaccounts of this account at a specific date.
        /// </summary>
        /// <param name="until">The last date that will be counted</param>
        /// <returns>The value of the account</returns>
        public override int GetValue(DateTime until)
        {
            return Subaccounts.Sum(account => account.GetValue(until));
        }

        /// <summary>
        ///     Returns the cummulated plan value of all subaccounts of this account.
        /// </summary>
        /// <returns>The plan value of the account</returns>
        public override int GetPlanValue()
        {
            return Subaccounts.Sum(account => account.GetPlanValue());
        }

        /// <summary>
        ///     Returns the cummulated start value of all subaccounts of this account.
        /// </summary>
        /// <returns>The start vlaue of the account</returns>
        public override int GetStartValue()
        {
            return Subaccounts.Sum(account => account.GetStartValue());
        }
    }
}