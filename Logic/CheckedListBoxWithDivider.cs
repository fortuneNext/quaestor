﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quaestor
{
    /// <summary>
    /// Eine Checklistbox mit der zusätzlichen Funktion, Teiler (Divider) einzufügen, um den Inhalt sortieren zu können
    /// </summary>
    public partial class CheckedListBoxWithDivider: CheckedListBox
    {
        public List<int> _DividerIndices { get; private set; }
        public CheckedListBoxWithDivider()
        {
            InitializeComponent();
            _DividerIndices = new List<int>();
        }


        /// <summary>
        /// Definiert das Element an Position index als Divider oder hebt Definiton als Divider auf
        /// </summary>
        /// <param name="item">The name of the element</param>
        /// <param name="divide">true if element should be added to Divider List, false if it should be removed</param>
        public void SetDivider(string item, bool divide)
        {

            int index = Items.IndexOf(item);
            if (divide)
                _DividerIndices.Add(index);
            else
                _DividerIndices.Remove(index);
            this.Refresh();
        }

        /// <summary>
        /// Definiert das Element an Position index als Divider oder hebt Definiton als Divider auf
        /// </summary>
        /// <param name="index">The index of the element</param>
        /// <param name="divide">true if element should be added to Divider List, false if it should be removed</param>
        public void SetDivider(int index, bool divide)
        {
            if (divide)
                _DividerIndices.Add(index);
            else
                _DividerIndices.Remove(index);
            this.Refresh();
        }

        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            if (_DividerIndices.Contains(e.Index))
            {
                Color col = new Color();
                e.DrawBackground();
                TextRenderer.DrawText(
                    e.Graphics,
                    Items[e.Index].ToString(),//String value of currently drawn element
                    this.Font,
                    new Rectangle(new Point(e.Bounds.X + 1, e.Bounds.Y + 1), new Size(e.Bounds.Width, e.Bounds.Height)),
                    col,//color
                    TextFormatFlags.HorizontalCenter);//Divider are centered
            }
            else
            {
                base.OnDrawItem(e);
            }
        }

        protected override void OnClick(EventArgs e)
        {
            //Deselect Item if it is a Divider; if not done, it will cause errors
            //because item is set as checked, even if it has no Checkbox
            if (_DividerIndices.Contains(this.SelectedIndex))
                this.SetSelected(this.SelectedIndex, false);
            base.OnClick(e);
        }
    }
}
