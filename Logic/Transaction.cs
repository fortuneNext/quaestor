﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Quaestor.Logic
{
    /// <summary>
    ///     A transaction for double bookkeeping.
    /// </summary>
    public class Transaction
    {
        /// <summary>
        ///     Constructor method for a transaction.
        /// </summary>
        /// <param name="accounts">An array of accounts which are used within the transaction</param>
        /// <param name="values">An array of values that belong to the accounts in accounts</param>
        /// <param name="id">The Id of the transaction</param>
        /// <param name="timeStamp">The DateTime of the transaction</param>
        /// <param name="receiptNr">The Id of the receipt belonging to this transaction</param>
        /// <param name="category">The category of the transaction</param>
        /// <param name="comment">A freetext comment to the transaction</param>
        public Transaction(List<ValueAccount> accounts, List<int> values, int id, DateTime timeStamp, int receiptNr,
            string category, string comment)
        {
            Accounts = accounts;
            Values = values;
            Id = id;
            TimeStamp = timeStamp;
            ReceiptNr = receiptNr;
            Category = category;
            Comment = comment;
        }

        /// <summary>
        ///     The unique Id for every transaction.
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        ///     An array of accounts which are used within the transaction.
        /// </summary>
        public List<ValueAccount> Accounts { get; private set; }

        /// <summary>
        ///     An array of values that belong to the accounts in Accounts[].
        /// </summary>
        public List<int> Values { get; private set; }

        /// <summary>
        ///     The DateTime of the transaction.
        /// </summary>
        public DateTime TimeStamp { get; private set; }

        /// <summary>
        ///     The Id of the receipt belonging to this transaction.
        /// </summary>
        public int ReceiptNr { get; private set; }

        /// <summary>
        ///     The category of the transaction.
        /// </summary>
        public string Category { get; private set; }

        /// <summary>
        ///     A comment for the transaction.
        /// </summary>
        public string Comment { get; private set; }

        /// <summary>
        ///     Checks if a transaction is valid for double bookkeeping.
        /// </summary>
        /// <returns>True if the transaction is valid for double bookkeeping; false otherwise.</returns>
        public bool CheckValidity()
        {
            // TODO randfälle (leere arrays)

            // Prüfe Größe
            if (!(Accounts.Count > 0) || !(Values.Count > 0) || (Accounts.Count != Values.Count))
                return false;
            // Prüfe 0-Wert
            int res = 0;
            for (int i = 0; i < Accounts.Count; i++)
            {
                if (Accounts[i].AccountType == AccountTypes.Passive)
                    res += Values[i];
                else
                    res -= Values[i];
            }
            return res == 0;
        }

        /// <summary>
        ///     Checks wheter the receipe for this transactions exists in the receipt directory.
        /// </summary>
        /// <returns>True if the receipt exists; false otherwise</returns>
        public bool ReceiptExists()
        {
            return ReceiptNr == 0 || Directory.EnumerateFiles(strings.RecipeDir, ReceiptNr + "_*").Any();
        }
    }
}