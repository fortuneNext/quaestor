﻿namespace Quaestor.Logic
{
    /// <summary>
    ///     Contains the three types of accounts "Active", "Passive" and "User".
    /// </summary>
    public enum AccountTypes
    {
        /// <summary>
        /// Active account
        /// </summary>
        Active,
        /// <summary>
        /// Passive account
        /// </summary>
        Passive,
        /// <summary>
        ///  User account (active)
        /// </summary>
        User
    };
}