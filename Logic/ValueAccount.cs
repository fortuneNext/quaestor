﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Quaestor.Logic
{
    /// <summary>
    ///     An account that adds up all values of transactions for the specified account.
    /// </summary>
    public class ValueAccount : Account
    {
        /// <summary>
        ///     Constructor method for a value account.
        /// </summary>
        /// <param name="startValue">The value of the account that cannot be found within the transactions</param>
        /// <param name="planValue">The planned value for the account</param>
        /// <param name="name">The name of the account</param>
        /// <param name="accountType">The type of the account</param>
        /// <param name="referenceSystem">The list of transactions that the account adds up</param>
        /// <param name="printToOverview">Decides wheter the account should be printed to the overview</param>
        public ValueAccount(int startValue, int planValue, string name, AccountTypes accountType,
            List<Transaction> referenceSystem, bool printToOverview) : base(name, accountType, printToOverview)
        {
            //TODO NEW_ERA Keep startValue here, as this is used to load old semesters
            //StartValue = 0;
            StartValue = startValue;
            PlanValue = planValue;
            ReferenceSystem = referenceSystem;
        }

        /// <summary>
        ///     The value of the account that cannot be found within the transactions.
        /// </summary>
        private int StartValue { get; set; }

        /// <summary>
        ///     The plan value of the account.
        /// </summary>
        private int PlanValue { get; set; }

        /// <summary>
        ///     The list of transactions that the account adds up.
        /// </summary>
        private List<Transaction> ReferenceSystem { get; set; }

        /// <summary>
        ///     Returns the cummulated costs of all transactions in the list that contain this account.
        /// </summary>
        /// <returns>The value of the account</returns>
        public override int GetValue()
        {
            //TODO NEW_ERA--
            int res = 0;
            //int res = StartValue;
            foreach (Transaction transaction in ReferenceSystem)
            {
                for (int i = 0; i < transaction.Accounts.Count; i++)
                {
                    if (transaction.Accounts[i].Equals(this))
                        res += transaction.Values[i];
                }
            }
            return res;
        }


        /// <summary>
        ///     Returns the cummulated costs of all transactions in the list that contain this account with the (obsolete) start Value.
        /// </summary>
        /// <returns>The value of the account</returns>
        public override int GetStartValueOldSave()
        {
            return StartValue;
        }

        /// <summary>
        ///     Does the same as GetValue(DateTime until); is therefore obsolete, please use GetValue
        /// </summary>
        /// <param name="date">The date to get the value</param>
        /// <returns></returns>
        public int GetValueAtDate(DateTime date)
        {
            //TODO NEW_ERA--
            int res = 0;
            //int res = StartValue;
            foreach (Transaction transaction in ReferenceSystem.Where(transaction => DateTime.Compare(transaction.TimeStamp, date) <= 0))
                {
                for (int i = 0; i < transaction.Accounts.Count; i++)
                {
                    if (transaction.Accounts[i].Equals(this))
                        res += transaction.Values[i];
                }
            }
            return res;
        }

        /// <summary>
        ///     Returns the cumulated costs of all transactions in the list that contain this account up to the specified date.
        /// </summary>
        /// <param name="until">The last date that will be counted</param>
        /// <returns></returns>
        public override int GetValue(DateTime until)
        {
            //TODO NEW_ERA--
            int res = 0;
            //int res = StartValue;
            foreach (Transaction transaction in ReferenceSystem.Where(transaction => transaction.TimeStamp <= until))
            {
                for (int i = 0; i < transaction.Accounts.Count; i++)
                {
                    if (transaction.Accounts[i].Equals(this))
                        res += transaction.Values[i];
                }
            }
            return res;
        }

        /// <summary>
        ///     Returns the plan value of the account.
        /// </summary>
        /// <returns>The plan value of the account</returns>
        public override int GetPlanValue()
        {
            return PlanValue;
        }

        /// <summary>
        ///     Returns the start value of the account.
        /// </summary>
        /// <returns>The start value of the account</returns>
        public override int GetStartValue()
        {
            //TODO NEW_ERA--
            Transaction transaction = ReferenceSystem[0];
            for (int i = 0; i < transaction.Accounts.Count; i++)
            {
                if (transaction.Accounts[i].Equals(this))
                    return transaction.Values[i];
            }
            return 0;
        }
    }
}