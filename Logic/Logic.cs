﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using Quaestor.Properties;
using System.Reflection;
using Quaestor.XmlSerialization;
using System.Windows.Documents;
using Quaestor.GUI;

#if DEBUG
using System.Diagnostics;

#endif

namespace Quaestor.Logic
{
    /// <summary>
    ///     Contains all the programs functionality.
    /// </summary>
    public class Logic
    {
        /// <summary>
        ///     The name of the admin-user
        /// </summary>
        public string AdminName;

        /// <summary>
        ///     A name the admin-user gives to the interval of time where he is active (e.g. WS14/15 or SS14)
        /// </summary>
        public string TimeName;

        /// <summary>
        ///     Constructor method for Logic with initialization.
        /// </summary>
        public Logic()
        {
            Transactions = new List<Transaction>();
            Accounts = new List<Account>();
            OverviewColumns = new List<DateTime>();
        }

        /// <summary>
        ///     List of all transactions in the system
        /// </summary>
        private List<Transaction> Transactions { get; set; }

        /// <summary>
        ///     List of all accounts in the system
        /// </summary>
        private List<Account> Accounts { get; set; }

        /// <summary>
        ///     List of all columns for the overview
        /// </summary>
        public List<DateTime> OverviewColumns { get; private set; }

        /// <summary>
        ///     Returns a read-only-list of all transactions in the system
        /// </summary>
        public ReadOnlyCollection<Transaction> GetTransactions
        {
            get { return Transactions.AsReadOnly(); }
        }

        /// <summary>
        ///     Returns a read-only-list of all accounts in the system
        /// </summary>
        public ReadOnlyCollection<Account> GetAccounts
        {
            get { return Accounts.AsReadOnly(); }
        }

        /// <summary>
        ///     Creates a list of all categories used yet.
        /// </summary>
        /// <returns>A list of all categories used yet</returns>
        public List<string> GetExistingCategories()
        {
#if DEBUG
            Console.WriteLine(@"Calling " + MethodBase.GetCurrentMethod().Name + @"...");
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            try
            {
#endif
                var categories = new List<string>();
                foreach (
                    Transaction transaction in
                        Transactions.Where(transaction => !(categories.Contains(transaction.Category))))
                    categories.Add(transaction.Category);
                categories.Sort();
                return categories;
#if DEBUG
            }
            finally
            {
                stopWatch.Stop();
                Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" took " + stopWatch.Elapsed);
            }
#endif
        }

        /// <summary>
        ///     Creates a new transaction.
        /// </summary>
        /// <param name="accounts">Array of accounts involved into the transaction</param>
        /// <param name="values">Array of values belonging to the accounts involved into the transaction</param>
        /// <param name="timeStamp">Date of the transaction</param>
        /// <param name="receiptNr">Number of the receipt for the transaction</param>
        /// <param name="category">Category of the transaction</param>
        /// <param name="comment">Comment to the transaction</param>
        /// <returns>True if successful, false if invalid transaction</returns>
        public bool NewTransaction(List<ValueAccount> accounts, List<int> values, DateTime timeStamp, int receiptNr,
            string category, string comment)
        {
            var newTransaction = new Transaction(accounts, values, GetNewTransactionId(), timeStamp, receiptNr, category,
                comment);
            // Check Validity
            if (!newTransaction.CheckValidity())
                return false;
            // Check for unique Id
            if (Transactions.Any(transaction => transaction.Id == newTransaction.Id))
                return false;
            Transactions.Add(newTransaction);
            return true;
        }

        /// <summary>
        ///     Registers a new transaction.
        /// </summary>
        /// <param name="newTransaction">The transaction to be registered</param>
        /// <returns>True if successful, false if invalid transaction or Id already in use</returns>
        public bool NewTransaction(Transaction newTransaction)
        {
            // Check Validity
            if (!newTransaction.CheckValidity())
                return false;
            if (Transactions.Any(transaction => transaction.Id == newTransaction.Id))
                return false;
            Transactions.Add(newTransaction);
            return true;
        }

        /// <summary>
        ///     Returns the Id a new created transaction will get.
        /// </summary>
        /// <returns>The Id of a new transaction</returns>
        public int GetNewTransactionId()
        {
            int res = Transactions.Select(transaction => transaction.Id).Concat(new[] { -1 }).Max();
            return ++res;
        }

        /// <summary>
        ///     Deletes the transaction with the specified Id.
        /// </summary>
        /// <param name="id">The Id of the transaction to be deleted</param>
        /// <returns>True if successful; false if not (e.g. Id not found)</returns>
        public bool DeleteTransaction(int id)
        {
            Transaction transaction = GetTransaction(id);
            if (transaction == null)
                return false;
            Transactions.Remove(transaction);
            return true;
        }

        /// <summary>
        ///     Deletes all transactions.
        /// </summary>
        /// <returns>True if successful</returns>
        public bool DeleteAllTransactions()
        {
            //This doesn't update the Transactions reference in Accounts, they still use the old Transactions list
            //Sadly this is necessary for the NewTimeInterval Section, so i cannot fix it
            Transactions = new List<Transaction>();
            return true;
        }

        /// <summary>
        ///     Deletes the account with specified name if its value is 0 and it is not involved into transactions.
        /// </summary>
        /// <param name="name">The name of the account to be deleted</param>
        /// <returns>True if successful; false if value is not 0 or inolved transactions exist</returns>
        public bool DeleteAccount(string name)
        {
            //LegacyAccount, which replaces Start Values cannot be removed
            //Technically redundant, as LegacyAccount is always part of a transaction
            if (name == Settings.Default.LegacyAccount)
                return false;
            Account delAccount = StrToAccount(name);
            if (((delAccount.AccountType != AccountTypes.Passive) && (delAccount is ValueAccount) &&
                 (delAccount.GetValue() != 0)) || GetInvolvedTransactions(delAccount.Name).Any())
                return false;
            foreach (StructuralAccount account in Accounts.OfType<StructuralAccount>())
                account.Subaccounts.Remove(delAccount);
            Accounts.Remove(delAccount);
            return true;
        }

        /// <summary>
        ///     Creates a new account that contains money.
        /// </summary>
        /// <param name="startValue">The initial amount of money</param>
        /// <param name="planValue">The plan amount of money</param>
        /// <param name="name">The account name</param>
        /// <param name="accountType">The account type, either "Active", "Passive" or "User"</param>
        /// <param name="printToOverview">True if the account should be printed</param>
        /// <returns>True if successful, false if name already in use</returns>
        public bool NewValueAccount(int startValue, int planValue, string name, AccountTypes accountType,
            bool printToOverview)
        {
#if DEBUG
            Console.WriteLine(@"Calling " + MethodBase.GetCurrentMethod().Name + @"...");
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            try
            {
#endif
                if (Accounts.Any(account => account.Name == name))
                    return false;
                Accounts.Add(accountType == AccountTypes.User
                    ? new UserAccount(startValue, planValue, name, accountType, Transactions, printToOverview, "")
                    : new ValueAccount(startValue, planValue, name, accountType, Transactions, printToOverview));
                Accounts.Sort((x, y) => String.CompareOrdinal(x.Name, y.Name));
                return true;
#if DEBUG
            }
            finally
            {
                stopWatch.Stop();
                Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" took " + stopWatch.Elapsed);
            }
#endif
        }

        /// <summary>
        ///     Replaces an old account with a new value account.
        /// </summary>
        /// <param name="oldName">The name of the account to replace</param>
        /// <param name="startValue">The initial amount of money</param>
        /// <param name="planValue">The plan amount of money</param>
        /// <param name="name">The account name</param>
        /// <param name="accountType">The account type, either "Active", "Passive" or "User"</param>
        /// <param name="printToOverview">True if the account should be printed</param>
        /// <returns>True if successful, false if oldName not found</returns>
        public bool ReplaceWithValueAccount(string oldName, int planValue, string name,
            AccountTypes accountType, bool printToOverview)
        {
            //NEW_ERA-- StartValue = 0
            foreach (Account account in Accounts.Where(account => account.Name == oldName))
            {
                if (accountType == AccountTypes.User)
                {
                    var newAccount = new UserAccount(0, planValue, name, accountType, Transactions,
                        printToOverview, "");
                    if (!ReplaceAccount(oldName, newAccount)) continue;
                    Accounts[Accounts.IndexOf(account)] = newAccount;
                    return true;
                }
                else
                {
                    var newAccount = new ValueAccount(0, planValue, name, accountType, Transactions,
                        printToOverview);
                    if (!ReplaceAccount(oldName, newAccount)) continue;
                    Accounts[Accounts.IndexOf(account)] = newAccount;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        ///     Creates a new user account including additional parameters.
        /// </summary>
        /// <param name="startValue">The initial amount of money</param>
        /// <param name="planValue">The plan amount of money</param>
        /// <param name="name">The account name</param>
        /// <param name="printToOverview">True if the account should be printed</param>
        /// <param name="email">The account email adress</param>
        /// <returns>True if successful, false if name already in use</returns>
        public bool NewUserAccount(int startValue, int planValue, string name, bool printToOverview, string email)
        {
            if (Accounts.Any(account => account.Name == name))
                return false;
            //NEW_ERA startValue = 0
            Accounts.Add(new UserAccount(startValue, planValue, name, AccountTypes.User, Transactions, printToOverview,
                email));
            Accounts.Sort((x, y) => String.CompareOrdinal(x.Name, y.Name));
            return true;
        }

        /// <summary>
        ///     Replaces an old account with a new user account.
        /// </summary>
        /// <param name="oldName">The name of the account to replace</param>
        /// <param name="startValue">The initial amount of money</param>
        /// <param name="planValue">The plan amount of money</param>
        /// <param name="name">The account name</param>
        /// <param name="printToOverview">True if the account should be printed</param>
        /// <param name="email">The account email adress</param>
        /// <returns>True if successful, false if oldName not found</returns>
        public bool ReplaceWithUserAccount(string oldName, int planValue, string name,
            bool printToOverview, string email)
        {
            //NEW_ERA-- startValue = 0
            foreach (Account account in Accounts.Where(account => account.Name == oldName))
            {
                var newAccount = new UserAccount(0, planValue, name, AccountTypes.User, Transactions,
                    printToOverview, email);
                if (!ReplaceAccount(oldName, newAccount)) continue;
                Accounts[Accounts.IndexOf(account)] = newAccount;
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Creates a new structural account that cumulates a list of subaccounts.
        /// </summary>
        /// <param name="name">The name of the structural account</param>
        /// <param name="subaccounts">The list of subaccounts</param>
        /// <param name="accountType">The account type, either "Active", "Passive" or "User"</param>
        /// <param name="printToOverview">True if the account should be printed</param>
        /// <returns>True if successful, false if name already in use</returns>
        public bool NewStructuralAccount(string name, List<Account> subaccounts, AccountTypes accountType,
            bool printToOverview)
        {
            if (Accounts.Any(account => account.Name == name))
                return false;
            Accounts.Add(new StructuralAccount(name, subaccounts, accountType, printToOverview));
            return true;
        }

        /// <summary>
        ///     Creates an empty new structural account that cumulates a list of subbacounts.
        /// </summary>
        /// <param name="name">The name of the structural account</param>
        /// <param name="accountType">The account type, either "Active", "Passive" or "User"</param>
        /// <param name="printToOverview">True if the account should be printed</param>
        /// <returns>True if successful, false if name already in use</returns>
        public bool NewStructuralAccount(string name, AccountTypes accountType, bool printToOverview)
        {
            return NewStructuralAccount(name, new List<Account>(), accountType, printToOverview);
        }

        /// <summary>
        ///     Deletes an old account by its name and replaces it with a new structural account.
        /// </summary>
        /// <param name="oldName">The name of the account to be replaced</param>
        /// <param name="name">The name of the new structural account</param>
        /// <param name="accountType">The account type of the new structural account, either "Active", "Passive" or "User"</param>
        /// <param name="subaccounts">List of subaccounts of the new strucutral account</param>
        /// <param name="printToOverview">Determines wheter the new account should be printed to the overview</param>
        /// <returns>True if successful; false otherwise</returns>
        public bool ReplaceWithStructuralAccount(string oldName, string name, List<Account> subaccounts,
            AccountTypes accountType, bool printToOverview)
        {
            // Suche nach Account mit altem Namen
            foreach (Account account in Accounts.Where(account => account.Name == oldName))
            {
                // Erstelle neuen Account
                var newAccount = new StructuralAccount(name, subaccounts, accountType, printToOverview);
                // Ersetze Account, brich ab falls nicht möglich
                if (!ReplaceAccount(oldName, newAccount)) continue;
                Accounts[Accounts.IndexOf(account)] = newAccount;
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Deletes an old account by its name and replaces it with a new structural account.
        /// </summary>
        /// <param name="oldName">The name of the account to be replaced</param>
        /// <param name="name">The name of the new structural account</param>
        /// <param name="accountType">The account type of the new structural account, either "Active", "Passive" or "User"</param>
        /// <param name="printToOverview">Determines wheter the new account should be printed to the overview</param>
        /// <returns>True if successful; false otherwise</returns>
        public bool ReplaceWithStructuralAccount(string oldName, string name, AccountTypes accountType,
            bool printToOverview)
        {
            return ReplaceWithStructuralAccount(oldName, name, new List<Account>(), accountType, printToOverview);
        }

        /// <summary>
        ///     Deletes an old account by its name and replaces it with the given one.
        /// </summary>
        /// <param name="oldName">The name of the account to be replaced</param>
        /// <param name="newAccount">The new account</param>
        /// <returns>True if successful; false otherwise</returns>
        private bool ReplaceAccount(string oldName, Account newAccount)
        {
#if DEBUG
            Console.WriteLine(@"Calling " + MethodBase.GetCurrentMethod().Name + @"...");
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            try
            {
#endif
                // Falls neuer Account kein ValueAccount, darf er in keinem Vorgang vorkommen bisher, falls doch brich ab
                if (!(newAccount is ValueAccount))
                {
                    if (
                        Transactions.SelectMany(transaction => transaction.Accounts)
                            .Any(valueAccount => valueAccount.Name == oldName))
                    {
                        MessageBox.Show(strings.AccountConversionNotPossible);
                        return false;
                    }
                }
                // Falls neuer Account ValueAccount, ersetze ihn in allen Vorgängen
                else
                {
                    foreach (Transaction transaction in Transactions)
                    {
                        for (int i = 0; i < transaction.Accounts.Count; i++)
                        {
                            if (transaction.Accounts[i].Name == oldName)
                                transaction.Accounts[i] = (ValueAccount)newAccount;
                        }
                    }
                    //Parallel.ForEach(Transactions, transaction =>
                    //{
                    //    for (int i = 0; i < transaction.Accounts.Count; i++)
                    //    {
                    //        if (transaction.Accounts[i].Name == oldName)
                    //            transaction.Accounts[i] = (ValueAccount)newAccount;
                    //    }
                    //}
                    //    );
                }
                // Ersetze neuen Account in allen Strukturaccounts
                foreach (StructuralAccount structuralAccount in Accounts.OfType<StructuralAccount>())
                {
                    for (int i = 0; i < structuralAccount.Subaccounts.Count; i++)
                    {
                        if (structuralAccount.Subaccounts[i].Name == oldName)
                            structuralAccount.Subaccounts[i] = newAccount;
                    }
                }
#if DEBUG
            }
            finally
            {
                stopWatch.Stop();
                Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" took " + stopWatch.Elapsed);
            }
#endif
            return true;
        }

        /// <summary>
        ///     Searchs for an account with a specified name and returns it.
        /// </summary>
        /// <param name="name">The name of the account</param>
        /// <returns>The searched account; null otherwise</returns>
        public Account StrToAccount(string name)
        {
            return StrToAccount(name, Accounts);
        }

        /// <summary>
        ///     Searchs for an account with a specified name within a specified list of accounts and returns it.
        /// </summary>
        /// <param name="name">The name of the account</param>
        /// <param name="accounts">The list of accounts</param>
        /// <returns>The searched account; null otherwise</returns>
        private static Account StrToAccount(string name, List<Account> accounts)
        {
            return accounts.FirstOrDefault(account => account.Name == name);
        }

        /// <summary>
        ///     Creates an XmL wrapper on the basis of the current Program data
        /// </summary>
        /// <returns>an XmlLogic wrapper with all program data</returns>
        private XmlLogic CreateXmlWrapper()
        {
            var xmlAccounts = new List<XmlAccount>();
            // Für jeden Account, füge einen entsprechenden XmlAccount in xmlAccounts hinzu 
            foreach (Account account in Accounts)
            {
                if (account is UserAccount)
                    xmlAccounts.Add(new XmlUserAccount((UserAccount)account));
                else if (account is ValueAccount)
                    xmlAccounts.Add(new XmlValueAccount((ValueAccount)account));
                else if (account is StructuralAccount)
                    xmlAccounts.Add(new XmlStructuralAccount((StructuralAccount)account));
            }

            // Für jeden Vorgang, füge eine entsprechende XmlTransaction in xmlTransactions hinzu
            List<XmlTransaction> xmlTransactions =
                Transactions.Select(transaction => new XmlTransaction(transaction)).ToList();

            // Erstelle und fülle Wrapper
            var wrapper = new XmlLogic
            {
                XmlTimeName = TimeName,
                XmlAdminName = AdminName,
                XmlAccounts = xmlAccounts,
                XmlTransactions = xmlTransactions,
                XmlOverviewColumns = OverviewColumns
            };

            return wrapper;
        }

        /// <summary>
        ///     Checks if changes were made since the last save
        /// </summary>
        /// <returns>true if changes were made; false otherwhise</returns>
        public bool CheckForChanges()
        {
            //this one uses newly written .Equals functions in the XmlSerialization classes
            //I didn't override the Hash functions, so you should probably do that if there are any problems
            //it creates the save wrapper from the program data and the save data and compares them
            bool equal;
#if DEBUG
            Console.WriteLine(@"Calling " + MethodBase.GetCurrentMethod().Name + @"...");
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            try
            {
#endif
            // Ungültige Pfade abfangen
            string path = Environment.CurrentDirectory + @"\" + Settings.Default.MainFile;
            string dirPath;
            if (!File.Exists(path))
                return false;
            try
            {
                dirPath = Path.GetDirectoryName(path);
            }
            catch
            {
                return false;
            }
            if (dirPath == null)
                return false;
            // Lade Wrapper
            var serializer = new XmlSerializer(typeof(XmlLogic));
            var streamReader = new StreamReader(path);
            var SavedState = (XmlLogic)serializer.Deserialize(streamReader);
            streamReader.Close();

            //create wrapper of current Program data
            var ProgramState = CreateXmlWrapper();

            //compare Program data with saved data
            equal = !ProgramState.Equals(SavedState);
#if DEBUG
            }
            finally
            {
                stopWatch.Stop();
                Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" took " + stopWatch.Elapsed);
            }
#endif
            return equal;
            /*//V2 of this Problem
            //this one creates a new savefile, compares it with the data and deletes it afterwards
            //it takes a bit longer, though not noticeable
            bool equal;
#if DEBUG
            Console.WriteLine(@"Calling " + MethodBase.GetCurrentMethod().Name + @"...");
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            try
            {
#endif
            // Ungültige Pfade abfangen
            string testPath = Environment.CurrentDirectory + @"\test.xml";
            string dataPath = Environment.CurrentDirectory + @"\" + Settings.Default.MainFile;
            string dirPath;
            try
            {
                dirPath = Path.GetDirectoryName(testPath);
            }
            catch
            {
                return false;
            }
            if (dirPath == null)
                return false;

            // Serialisiere, erstelle Ordner und schreibe in Datei
            var serializer = new XmlSerializer(typeof(XmlLogic));
            Directory.CreateDirectory(dirPath);
            var fileStream = new FileStream(testPath, FileMode.Create);
            serializer.Serialize(fileStream, CreateXmlWrapper());
            fileStream.Close();

            equal = File.ReadLines(testPath).SequenceEqual(File.ReadLines(dataPath));
            var file = new FileInfo(testPath);
            file.Delete();
#if DEBUG
            }
            finally
            {
                stopWatch.Stop();
                Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" took " + stopWatch.Elapsed);
            }
#endif

            if (!equal)
                return true;
            return false;*/


        }

        /// <summary>
        ///     Saves all current data to a specified path.
        /// </summary>
        /// <param name="path">The path the file will be safed</param>
        /// <returns>True if successful; false otherwise</returns>
        public bool SaveStateToFile(string path)
        {
#if DEBUG
            Console.WriteLine(@"Calling " + MethodBase.GetCurrentMethod().Name + @"...");
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            try
            {
#endif
                // Ungültige Pfade abfangen
                string dirPath;
                try
                {
                    dirPath = Path.GetDirectoryName(path);
                }
                catch
                {
                    return false;
                }
                if (dirPath == null)
                    return false;
                // Serialisiere, erstelle Ordner und schreibe in Datei
                var serializer = new XmlSerializer(typeof(XmlLogic));
                Directory.CreateDirectory(dirPath);
                var fileStream = new FileStream(path, FileMode.Create);
                serializer.Serialize(fileStream, CreateXmlWrapper());
                fileStream.Close();
#if DEBUG
            }
            finally
            {
                stopWatch.Stop();
                Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" took " + stopWatch.Elapsed);
            }
#endif
            return true;
        }

        /// <summary>
        ///     Saves all current data.
        /// </summary>
        /// <returns>True if successful; false otherwise</returns>
        public bool SaveState()
        {
                return (SaveStateToFile(Environment.CurrentDirectory + @"\" + Settings.Default.MainFile)) &&
                   (SaveStateToFile(Environment.CurrentDirectory + @"\" + Settings.Default.BackupDir + @"\" +
                                    DateTime.Now.ToString(CultureInfo.InvariantCulture).Replace(':', '_').Replace('/', '_') + ".xml"));
        }

        /// <summary>
        ///     Moves the data file and the receipts to a new directory in the archive directory, protects it and creates a new file and directory.
        /// </summary>
        /// <returns>True if successful; false otherwise, e.g. when a file does not exist</returns>
        public bool ArchiveState()
        {
            // Falls normales speichern nicht funktioniert, brich ab
            if (!SaveState()) return false;
            // Initialisiere Dateien und neue Pfade
            var file = new FileInfo(Environment.CurrentDirectory + @"\" + Settings.Default.MainFile);
            if (AdminName == null)
                AdminName = @"User";
            if (TimeName == null)
                TimeName = @"0000";
            string newFilePath = Environment.CurrentDirectory + @"\" + Settings.Default.ArchiveDir + @"\" + GetNewArchiveId() +
                                 @"_" + Regex.Replace(TimeName, @"[^\w]", "") + "_" +
                                 Regex.Replace(AdminName, @"[^\w]", "") +
                                 ".xml";
            var folder = new DirectoryInfo(Environment.CurrentDirectory + @"\" + strings.RecipeDir);
            string newFolderPath = Environment.CurrentDirectory + @"\" + Settings.Default.ArchiveDir + @"\" + GetNewArchiveId() +
                                   @"_" + Regex.Replace(TimeName, @"[^\w]", "") + "_" +
                                   Regex.Replace(AdminName, @"[^\w]", "") +
                                   "_Belege";
            // Falls alte Pfade nicht existieren oder neue Pfade bereits existieren, brich ab
            if (!file.Exists || File.Exists(newFilePath) || !folder.Exists || Directory.Exists(newFolderPath))
                return false;
            file.MoveTo(newFilePath); // move data.xml to history
            file.Refresh();
            folder.MoveTo(newFolderPath); // Verschiebe Belegeordner
            folder.Refresh();
            // Setze Schreibschutz für alle Dateien im Belegeordner
            foreach (FileInfo iterFile in folder.GetFiles("*", SearchOption.AllDirectories))
                iterFile.IsReadOnly = true;
            // Erstelle Belegeordner neu
            folder = new DirectoryInfo(Environment.CurrentDirectory + @"\" + strings.RecipeDir);
            folder.Create();

            //copy file back to data.xml
            file.CopyTo(Environment.CurrentDirectory + @"\" + Settings.Default.MainFile);
            file.IsReadOnly = true; // Setze Schreibschutz
            return true;
        }

        /// <summary>
        ///     Loads (and overwrites) all data from a specified path.
        /// </summary>
        /// <param name="path">The path of the file that will be loaded</param>
        /// <returns>True if successful; false otherwise</returns>
        public bool LoadStateFromFile(string path)
        {
#if DEBUG
            Console.WriteLine(@"Calling " + MethodBase.GetCurrentMethod().Name + @"...");
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            try
            {
#endif
                // Ungültige Pfade abfangen
                if (!File.Exists(path))
                    return false;
                string dirPath;
                try
                {
                    dirPath = Path.GetDirectoryName(path);
                }
                catch
                {
                    return false;
                }
                if (dirPath == null)
                    return false;
                // Lade Wrapper
                var serializer = new XmlSerializer(typeof(XmlLogic));
                var streamReader = new StreamReader(path);
                var logic = (XmlLogic)serializer.Deserialize(streamReader);
                streamReader.Close();
                // Entpacke Wrapper
                string timeName = logic.XmlTimeName;
                string adminName = logic.XmlAdminName;
                List<XmlAccount> xmlAccounts = logic.XmlAccounts; // Liste der XML Accounts
                List<XmlTransaction> xmlTransactions = logic.XmlTransactions; // Liste der XML Vorgänge
                OverviewColumns = logic.XmlOverviewColumns; // Liste der Datenspalten

                var newAccounts = new List<Account>(); // Liste der Accounts, die später die alte ersetzt
                var newTransactions = new List<Transaction>(); // Liste der Vorgänge, die später die alte ersetzt

#if DEBUG
                Console.WriteLine(MethodBase.GetCurrentMethod().Name +
                                  @" Initialization and HD-reading finished. Elapsed: " + stopWatch.Elapsed);
#endif

                // Für jeden XmlAccount, füge einen entsprechenden Account in newAccounts hinzu
                foreach (XmlAccount xmlAccount in xmlAccounts)
                {
                    if (xmlAccount is XmlUserAccount)
                        newAccounts.Add(new UserAccount(((XmlUserAccount)xmlAccount).StartValue,
                            ((XmlUserAccount)xmlAccount).PlanValue, xmlAccount.Name, xmlAccount.AccountType,
                            newTransactions, xmlAccount.PrintToOverview, ((XmlUserAccount)xmlAccount).Email));
                    else if (xmlAccount is XmlValueAccount)
                        newAccounts.Add(new ValueAccount(((XmlValueAccount)xmlAccount).StartValue,
                            ((XmlValueAccount)xmlAccount).PlanValue, xmlAccount.Name, xmlAccount.AccountType,
                            newTransactions, xmlAccount.PrintToOverview));
                    else if (xmlAccount is XmlStructuralAccount)
                        newAccounts.Add(new StructuralAccount(xmlAccount.Name, xmlAccount.AccountType,
                            xmlAccount.PrintToOverview));
                }

#if DEBUG
                Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" Loading accounts finished. Elapsed: " +
                                  stopWatch.Elapsed);
#endif

                // Setze Referenzen für StructuralAccounts
                foreach (XmlAccount xmlAccount in xmlAccounts)
                {
                    if (!(xmlAccount is XmlStructuralAccount)) continue;
                    foreach (string subAccount in ((XmlStructuralAccount)xmlAccount).Subaccounts)
                        ((StructuralAccount)StrToAccount(xmlAccount.Name, newAccounts)).AddAccount(
                            StrToAccount(subAccount, newAccounts));
                }

#if DEBUG
                Console.WriteLine(MethodBase.GetCurrentMethod().Name +
                                  @" Setting structural references finished. Elapsed: " + stopWatch.Elapsed);
#endif
                //TODO NEW_ERA
                /*
                 * Hier muss überprüft werden, ob eine LegacyTransaction existiert und wenn nein, muss diese erstellt
                 * und mit ID 0 abgespeichert werden (entsprechend alle anderen IDs +1)
                 */

                //does LegacyAccount exist
                bool LegacyExists = xmlTransactions.Exists(transaction => transaction.Id == 0
                                    && transaction.Accounts.Contains(Settings.Default.LegacyAccount));

                // Für jede XmlTransaction, füge einen entsprechenden Vorgang in newTransactions hinzu
                foreach (XmlTransaction xmlTransaction in xmlTransactions)
                {
                    var accounts = new List<ValueAccount>();
                    var values = new List<int>();
                    for (int i = 0; i < xmlTransaction.Accounts.Length; i++)
                    {
                        accounts.Add((ValueAccount)StrToAccount(xmlTransaction.Accounts[i], newAccounts));
                        values.Add(xmlTransaction.Values[i]);
                    }
                    //TODO NEW_ERA--
                    newTransactions.Add(new Transaction(accounts, values, !LegacyExists ? xmlTransaction.Id + 1 : xmlTransaction.Id, xmlTransaction.TimeStamp,
                        xmlTransaction.ReceiptNr, xmlTransaction.Category, xmlTransaction.Comment));
                }

#if DEBUG
                Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" Loading transactions finished. Elapsed: " +
                                  stopWatch.Elapsed);
#endif

                // Ersetze alte Listen
                TimeName = timeName;
                AdminName = adminName;
                Accounts = newAccounts;
                Accounts.Sort((x, y) => String.CompareOrdinal(x.Name, y.Name));
                Transactions = newTransactions;
                //TODO NEW_ERA--
                //Create LegacyTransaction, if it didn't exist before
                if (!LegacyExists)
                {
                    newTransactions.Insert(0, CreateLegacyTransaktionForLoading());
                }
                Transactions = newTransactions;
                Transactions.Sort((x, y) => x.Id.CompareTo(y.Id));
#if DEBUG
            }
            finally
            {
                stopWatch.Stop();
                Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" took " + stopWatch.Elapsed);
            }
#endif
            return true;
        }
        /// <summary>
        ///     Creates a Legacy Transaction from a File to load. It copies all startValues into the LegacyTransaction
        /// </summary>
        /// <returns>the new LegacyTransaction</returns>
        public Transaction CreateLegacyTransaktionForLoading()
        {
            var accounts = new List<ValueAccount>();
            ValueAccount newLegacyAccount;
            Transaction newLegacyTransaction;
            var values = new List<int>();
            int endValue = 0;

            //add all accounts exept for LegacyAccount
            foreach (ValueAccount account in this.GetAccounts.OfType<ValueAccount>()
                     .Where(a => a.Name != Settings.Default.LegacyAccount && a.AccountType != AccountTypes.Passive
                     && a.GetStartValueOldSave() != 0))
            {
                accounts.Add(account);
                values.Add(account.GetStartValueOldSave());
            }

            //add together current value of all Accounts
            foreach (int value in values)
                endValue += value;

            //book endValue to LegacyAccount, if it exists; else create it first
            newLegacyAccount = (ValueAccount)this.StrToAccount(Settings.Default.LegacyAccount);
            if (newLegacyAccount == null)
                this.NewValueAccount(0, 0, Settings.Default.LegacyAccount, AccountTypes.Passive, false);
            accounts.Add((ValueAccount)this.StrToAccount(Settings.Default.LegacyAccount));
            values.Add(endValue);

            //LegacyTransaction always has ID 0, as it is the first one
            int tempId = 0;
            newLegacyTransaction = new Transaction(accounts, values, tempId,
                new DateTime(), 0, strings.NewTimeIntervalTransactionType, strings.NewTimeIntervalTransactionComment);

            if (newLegacyTransaction.CheckValidity())
            {
                return newLegacyTransaction;
            }
            MessageBox.Show(strings.InvalidTransaction);
            return null;
        }

        /// <summary>
        ///     Loads (and overwrites) all data from the standard file.
        /// </summary>
        /// <returns>True if successful; false otherwise</returns>
        public bool LoadState()
        {
            return (LoadStateFromFile(Environment.CurrentDirectory + @"\" + Settings.Default.MainFile));
        }

        /// <summary>
        ///     Loads (and overwrites) all data from an archived file.
        /// </summary>
        /// <param name="id">The ID of the archived file</param>
        /// <returns>True if successful; false otherwise</returns>
        public bool LoadStateFromArchive(int id)
        {
            return LoadStateFromFile(GetArchiveFile(id).FullName);
        }

        /// <summary>
        ///     Loads the default data from the default file.
        /// </summary>
        /// <returns></returns>
        public bool LoadDefaultState()
        {
            return (LoadStateFromFile(Environment.CurrentDirectory + Path.DirectorySeparatorChar + Settings.Default.DefaultFile));
        }

        /// <summary>
        ///     Calculates the interest for a UserAccount with given interest rate.
        /// </summary>
        /// <param name="account">The UserAccount to calculate interest for</param>
        /// <param name="threshold">The number of days the User has to pay a debt</param>
        /// <param name="interest">The interest rate</param>
        /// <returns></returns>
        public int GetInterest(UserAccount account, int threshold, float interest)
        {
            // Reduced value: Only count payments in last 2 months
            int reducedValue = account.GetValue(DateTime.Now.AddDays(-threshold));
            foreach (Transaction transaction in Transactions)
            {
                for (int i = 0; i < transaction.Accounts.Count; i++)
                {
                    if (transaction.Accounts[i].Name == account.Name)
                        reducedValue += Math.Min(0, transaction.Values[i]);
                }
            }
            return (int)(interest * Math.Max(0, reducedValue));
        }

        /// <summary>
        ///     Gets a list of transactions in which the account with accountName appears.
        /// </summary>
        /// <param name="accountName">The name of the account</param>
        /// <returns>The list of transactions in which the account appears</returns>
        private List<Transaction> GetInvolvedTransactions(string accountName)
        {
            // Return a list of transactions from Transactions for which hold that its accountlist has at least one account with account.Name == accountName
            return
                Transactions.Where(transaction => transaction.Accounts.Any(account => account.Name == accountName))
                    .ToList();
        }

        /// <summary>
        ///     Gets a list of categories, comments and values which contain the account history.
        /// </summary>
        /// <param name="accountName">The name of the account</param>
        /// <returns>
        ///     A tuple with a list of strings with the names of categories, a list of strings with the names of commments and
        ///     a list of ints with the values.
        /// </returns>
        public Tuple<List<DateTime>, List<string>, List<string>, List<int>> GetAccountHistory(string accountName)
        {
#if DEBUG
            Console.WriteLine(@"Calling " + MethodBase.GetCurrentMethod().Name + @"...");
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            try
            {
#endif
                // Involvierte Vorgänge herausfinden
                List<Transaction> involvedTransactions = GetInvolvedTransactions(accountName);
                var dates = new List<DateTime>();
                var categories = new List<string>();
                var comments = new List<string>();
                var values = new List<int>();

                //TODO NEW_ERA--
                /*
                 * StartValue muss durch die neue LegacyTransaction ersetzt werden (hat ID 0)
                 * oder einfach code bis zur foreach Schleife entfernen
                 * oder bei GetStartValue Wert der ersten Transaktion zurückgeben und for erst ab der zweiten
                 */
                // Startvalue hinzufügen
                dates.Add(Transactions.Find(a => a.Id == 0).TimeStamp);
                categories.Add("Semesterbeginn");
                comments.Add("");
                values.Add(StrToAccount(accountName).GetStartValue());
                // Für jeden involvierten Vorgang
                foreach (Transaction transaction in involvedTransactions)
                {
                    if (transaction.Id == 0)
                        continue;
                    // Durchsuche Vorgang nach Account
                    for (int i = 0; i < transaction.Accounts.Count; i++)
                    {
                        if (transaction.Accounts[i].Name != accountName) continue;
                        // Wenn gefunden, füge Informationen hinzu
                        dates.Add(transaction.TimeStamp);
                        categories.Add(transaction.Category);
                        comments.Add(transaction.Comment);
                        values.Add(transaction.Values[i]);
                    }
                }
                return new Tuple<List<DateTime>, List<string>, List<string>, List<int>>(dates, categories, comments, values);
#if DEBUG
            }
            finally
            {
                stopWatch.Stop();
                Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" took " + stopWatch.Elapsed);
            }
#endif
        }

        /// <summary>
        ///     Sends the accounthistorys of a list of users to their emailaddress.
        /// </summary>
        /// <param name="accountNames">The list of accountnames</param>
        /// <returns>True if at least one email was successfully sent</returns>
        public bool SendEmailToUsers(List<string> accountNames)
        {
#if DEBUG
            Console.WriteLine(@"Calling " + MethodBase.GetCurrentMethod().Name + @"...");
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            try
            {
#endif
                bool res = false;//return argument
                // Initialisiere Mailclients
                var mailClients = new SmtpClient[Settings.Default.SendMailClientCount];
                var credentials = new NetworkCredential(Settings.Default.SendMailUsername, Settings.Default.SendMailPassword);
                // TODO PASSWORT AUS QUELLCODE
                for (int i = 0; i < Settings.Default.SendMailClientCount; i++)
                {
                    mailClients[i] = new SmtpClient(Settings.Default.SendMailServer, Settings.Default.SendMailPort)
                    {
                        Credentials = credentials,
                        Timeout = 30000,//so timeout occurs faster if there is an error
                        EnableSsl = Settings.Default.SendMailUseSSL,
                    };
                }
#if DEBUG
                Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" Mail Clients initialized " + stopWatch.Elapsed);
#endif
                var body = File.ReadAllText(Settings.Default.EmailTextFile, Encoding.Default);
                MessageBox.Show(body);

                // Server and Auth data
                /*var mailClient = new SmtpClient("mail.bergland-aachen.de", 465)
            {
                Credentials = new NetworkCredential("bergland-smtp", "dodoA01")
            };*/

                // Für jeden zu sendenen Account
                // foreach (string accountName in accountNames)
                Parallel.ForEach(accountNames, accountName =>
                {
#if DEBUG
                    Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" beginning iteration for " + accountName +
                                      @" on Thread " + Thread.CurrentThread.ManagedThreadId);
#endif
                    Account account = StrToAccount(accountName);
                    // Keine Nichtuser erlauben
                    if (!(account is UserAccount))
                        return;
                    var userAccount = (UserAccount)account;
                    // Nur vorhandene Emailadressen
                    if (userAccount.Email == "")
                        return;
                    // Nur gültige Emailadressen
                    if (!(IsValidEmail(userAccount.Email)))
                        return;
                    // History erstellen
                    Tuple<List<DateTime>, List<string>, List<string>, List<int>> history =
                        Program.Logic.GetAccountHistory(userAccount.Name);
                    string historyString = "";
                    for (int i = 0; i < history.Item4.Count; i++)
                        historyString += history.Item1[i] + @" " + history.Item2[i] + ": " + (((double)(history.Item4[i])) / 100).ToString("C") +
                                         " (" + history.Item3[i] + ")" + Environment.NewLine;
                    string value = (((double)(Program.Logic.StrToAccount(userAccount.Name).GetValue())) / 100).ToString("C");
                    // Mailadressen, Betreff und Text
                    var thisBody = body.Replace("%username%", userAccount.Name);
                    thisBody = thisBody.Replace("%history%", historyString);
                    thisBody = thisBody.Replace("%admin%", AdminName);
                    thisBody = thisBody.Replace("%value%", value);
                    var email = new MailMessage(Settings.Default.SendMailSenderAddress, userAccount.Email)
                    {
                        Subject = strings.EmailSubject.Replace(@"%subj%", Settings.Default.SendMailSubject),
                        Body = thisBody,
                        //if dept is high, set priority to high
                        Priority = Program.Logic.StrToAccount(userAccount.Name).GetValue() <= Settings.Default.OverviewDeptBorder*100 
                                    ? MailPriority.Normal : MailPriority.High
                    };
                    int curMailClient = 0;
                    // Versuche senden
                    try
                    {
                        for (int i = 0; i < Settings.Default.SendMailClientCount; i = (i + 1) % Settings.Default.SendMailClientCount)
                        {
                            if (Monitor.TryEnter(mailClients[i]))
                            {
#if DEBUG
                                Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" sending Email to " + email.To +
                                                    @" from client " + i);
#endif
                                curMailClient = i;
                                mailClients[i].Send(email);
                                res = true;
                                break;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(strings.FailedSendingEmailTo + email.To + ":" + Environment.NewLine + e.GetType() + ": " + e.Message);
#if DEBUG
                        MessageBox.Show("Gesamte Ausgabe: " + Environment.NewLine + e.ToString());
#endif
                        Monitor.Exit(mailClients[curMailClient]);
                    }
                });
                return res;
#if DEBUG
            }
            finally
            {
                stopWatch.Stop();
                Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" took " + stopWatch.Elapsed);
            }
#endif
        }

        /// <summary>
        ///     Sends the accounthistorys of a list of users to their emailaddress.
        /// </summary>
        /// <param name="accounts">The list of accounts</param>
        /// <returns>True if at least one email was successfully sent</returns>
        public bool SendEmailToUsers(List<UserAccount> accounts)
        {
            List<string> accountNames = accounts.Select(userAccount => userAccount.Name).ToList();
            return SendEmailToUsers(accountNames);
        }

        /// <summary>
        ///     Checks wheter a string is a valid emailaddress.
        /// </summary>
        /// <param name="email">The potential emailaddress</param>
        /// <returns>True if the string is a valid emailaddress</returns>
        public static bool IsValidEmail(string email)
        {
            try
            {
                var m = new MailAddress(email);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        /// <summary>
        ///     Gets the transaction with a specified ID.
        /// </summary>
        /// <param name="id">The ID of the returned Transaction</param>
        /// <returns>The transaction with the specified ID; null if not found</returns>
        public Transaction GetTransaction(int id)
        {
            return Transactions.FirstOrDefault(transaction => transaction.Id == id);
        }

        /// <summary>
        ///     Gets a list of old files in the history with its IDs, Names and Authors.
        /// </summary>
        /// <returns>A list of tuples with ID, Name and Author</returns>
        public static List<Tuple<int, string, string>> GetArchiveDataNames()
        {
            
            var archiveDir = new DirectoryInfo(Environment.CurrentDirectory + Path.DirectorySeparatorChar + Settings.Default.ArchiveDir);
            if (!archiveDir.Exists)
                archiveDir.Create();
            // Format: ID_TIME13/14_AUTHOR.xml
            var archiveFormat = new Regex(@"^(\d+)_([^_]+)_(.+)\.xml$");
            //Magic
            return (from file in archiveDir.GetFiles()
                    select archiveFormat.Match(file.Name)
                        into m
                        where m.Success
                        select
                            new Tuple<int, string, string>(Int32.Parse(m.Groups[1].Value), m.Groups[2].Value, m.Groups[3].Value))
                .ToList();
        }

        /// <summary>
        ///     Returns the first unused ID for a new history entry.
        /// </summary>
        /// <returns>The ID to be used by a new history entry</returns>
        public static int GetNewArchiveId()
        {
            return (GetArchiveDataNames().Select(searchItem => searchItem.Item1).Concat(new[] { 0 }).Max()) + 1;
        }

        private static FileInfo GetArchiveFile(int id)
        {
            var archiveDir = new DirectoryInfo(Environment.CurrentDirectory + @"\" + Settings.Default.ArchiveDir);
            var format = new Regex(@"^(" + id + @")_([^_]+)_(.+)\.xml$");
            return
                (from file in archiveDir.GetFiles() let m = format.Match(file.Name) where m.Success select file)
                    .FirstOrDefault();
        }

        /// <summary>
        ///     Returns a human readable string of all transactions in the data file.
        /// </summary>
        /// <returns>A human readable string of all transactions in the data file</returns>
        public string GetTransactionsPrint()
        {
            string res = String.Empty;
            foreach (Transaction transaction in Transactions)
            {
                res += @"ID: " + transaction.Id + Environment.NewLine
                       + @"Kategorie: " + transaction.Category + Environment.NewLine
                       + @"Kommentar: " + transaction.Comment + Environment.NewLine
                       + @"Inhalt:" + Environment.NewLine;
                for (int i = 0; i < transaction.Accounts.Count; i++)
                    res += transaction.Accounts[i].Name + ": " + transaction.Values[i] + Environment.NewLine;
                res += Environment.NewLine;
            }
            return res;
        }

        public string GetUsersPrint(List<Account> structAccounts)
        {
            int maxLength = 0;
            foreach (Account account in Program.Logic.GetAccounts.Where(
                        account => !(account is StructuralAccount) && account.AccountType == AccountTypes.User))
                foreach (StructuralAccount structAcc in structAccounts)
                    if (structAcc.Subaccounts.Contains(account))
                    {
                        maxLength = Math.Max(account.Name.Length, maxLength);
                    }



            string res = "Name" + space(maxLength) + "Schulden/Guthaben" + Environment.NewLine;
            string tempValue;
            foreach (Account account in Program.Logic.GetAccounts.Where(
                        account => !(account is StructuralAccount) && account.AccountType == AccountTypes.User))
                foreach (StructuralAccount structAcc in structAccounts)
                    if (structAcc.Subaccounts.Contains(account))
                    {
                        tempValue = MainForm.CleanMoneyInput(((double)account.GetValue() / 100).ToString());
                        res += account.Name + space(maxLength - account.Name.Length + 11-tempValue.Length)
                            + tempValue
                            + Environment.NewLine;
                    }
            return res;
        }

        public string space(int length)
        {
            string res = "";
            for (int i = 0; i < length; i++)
                res += " ";
            return res;
        }
    }
}