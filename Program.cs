﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Quaestor.GUI;
using System.Reflection;
#if DEBUG
using System.Diagnostics;
#endif

namespace Quaestor
{
    internal static class Program
    {
        public static Logic.Logic Logic;

        /// <summary>
        ///     Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        private static void Main()
        {
#if DEBUG
            Console.WriteLine(@"PROGRAM START");

            Console.WriteLine(@"Calling " + MethodBase.GetCurrentMethod().Name + @"...");
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            try
            {
#endif
            //Mutex to warn user, if he has multiple instances of this program open
            using (var mutex = new Mutex(false, "Questor Kassenprogramm"))
            {
                // TimeSpan.Zero to test the mutex's signal state and
                // return immediately without blocking
                bool isAnotherInstanceOpen = !mutex.WaitOne(TimeSpan.Zero);
                if (isAnotherInstanceOpen)
                {
                    MessageBox.Show(strings.ProgramStartMultipleInstances);
                }

            Logic = new Logic.Logic();
            var loadingTask = Task.Run( () =>
            {
                if (!Logic.LoadState())
                {
                    MessageBox.Show(strings.NoMainLoadFileFound);
                    if (!Logic.LoadDefaultState())
                        MessageBox.Show(strings.NoDefaultLoadFileFound);
                }
            }
            );
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var mainForm = new MainForm();
#if DEBUG
            Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" Waiting for loading completion. Elapsed: " + stopWatch.Elapsed);
#endif
            Task.WaitAll(loadingTask);
#if DEBUG
            Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" Loading complete. Elapsed: " + stopWatch.Elapsed);
#endif
            Application.Run(mainForm);

                mutex.ReleaseMutex();
            }
#if DEBUG
            }
            finally
            {
                stopWatch.Stop();
                Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" took " + stopWatch.Elapsed);
            }
#endif
        }
    }
}

/* TODO LISTE
 * Speichern/Laden
 * Vorgänge filtern
 * Vorgänge bearbeiten
 * Hilfefunktionen
 * Kassenprüfung
 * Globale Optionen (Nullkonten ausblenden)
 * Drucken
 * Ummodellieren: Aktiv Passiv User als Unterklassen statt als Felder
 * Logic neu sortieren
 * Dialogstruktur umbasteln: NeuerAccount etc sollen solchen ausgeben mit objResult == OK. Dazu bei Aufruf statt in Dialog auf Gültigkeit prüfen und Fenster evtl nicht schließen? Validation!?
 * Kassenübergabe
 * Verschönern: File ohne Streamreader mit File.ReadAll
 * Parallelisierung
 * Strukturaccounts einen echten Planungswert geben (z.B. für Veranstaltungen als Strukturaccount)
 * Debug Code (Zeitmessungen)
 * ? AccountReplace durch wirkliche Editierung ersetzen ?
 * Account ID statt Identifizierung über Namen
 * Funktion: Beleg einpflegen
 * Benutze mehr FileInfos statt Strings
 * systemStrings.res statt Strings.res für Dateien+
 * TxtBox Masken
 */