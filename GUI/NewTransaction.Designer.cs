﻿namespace Quaestor.GUI
{
    partial class NewTransaction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewTransaction));
            this.Btn_OK = new System.Windows.Forms.Button();
            this.Btn_Cancel = new System.Windows.Forms.Button();
            this.Lbl_receipt = new System.Windows.Forms.Label();
            this.TxtBox_receiptNr = new System.Windows.Forms.TextBox();
            this.Dtp_timeStamp = new System.Windows.Forms.DateTimePicker();
            this.Lbl_category = new System.Windows.Forms.Label();
            this.Lbl_comment = new System.Windows.Forms.Label();
            this.TxtBox_comment = new System.Windows.Forms.TextBox();
            this.Lbl_ID = new System.Windows.Forms.Label();
            this.Lbl_showID = new System.Windows.Forms.Label();
            this.Lbl_timeStamp = new System.Windows.Forms.Label();
            this.TxtBox_category = new System.Windows.Forms.ComboBox();
            this.Grp_active = new System.Windows.Forms.GroupBox();
            this.Lbl_SumActive = new System.Windows.Forms.Label();
            this.LayoutPan_active = new System.Windows.Forms.TableLayoutPanel();
            this.TxtBox_right1 = new System.Windows.Forms.TextBox();
            this.ComBox_right1 = new System.Windows.Forms.ComboBox();
            this.TxtBox_right2 = new System.Windows.Forms.TextBox();
            this.ComBox_right5 = new System.Windows.Forms.ComboBox();
            this.TxtBox_right4 = new System.Windows.Forms.TextBox();
            this.TxtBox_right3 = new System.Windows.Forms.TextBox();
            this.TxtBox_right5 = new System.Windows.Forms.TextBox();
            this.ComBox_right4 = new System.Windows.Forms.ComboBox();
            this.ComBox_right3 = new System.Windows.Forms.ComboBox();
            this.ComBox_right2 = new System.Windows.Forms.ComboBox();
            this.Btn_rightAdd = new System.Windows.Forms.Button();
            this.Grp_passive = new System.Windows.Forms.GroupBox();
            this.Lbl_SumPassive = new System.Windows.Forms.Label();
            this.LayoutPan_passive = new System.Windows.Forms.TableLayoutPanel();
            this.ComBox_left1 = new System.Windows.Forms.ComboBox();
            this.ComBox_left5 = new System.Windows.Forms.ComboBox();
            this.TxtBox_left5 = new System.Windows.Forms.TextBox();
            this.TxtBox_left1 = new System.Windows.Forms.TextBox();
            this.ComBox_left2 = new System.Windows.Forms.ComboBox();
            this.TxtBox_left2 = new System.Windows.Forms.TextBox();
            this.ComBox_left3 = new System.Windows.Forms.ComboBox();
            this.TxtBox_left4 = new System.Windows.Forms.TextBox();
            this.ComBox_left4 = new System.Windows.Forms.ComboBox();
            this.TxtBox_left3 = new System.Windows.Forms.TextBox();
            this.Btn_leftAdd = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.Grp_active.SuspendLayout();
            this.LayoutPan_active.SuspendLayout();
            this.Grp_passive.SuspendLayout();
            this.LayoutPan_passive.SuspendLayout();
            this.SuspendLayout();
            // 
            // Btn_OK
            // 
            this.Btn_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Btn_OK.Location = new System.Drawing.Point(170, 274);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(75, 23);
            this.Btn_OK.TabIndex = 34;
            this.Btn_OK.Text = "OK";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_OK_Click);
            // 
            // Btn_Cancel
            // 
            this.Btn_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Cancel.CausesValidation = false;
            this.Btn_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Btn_Cancel.Location = new System.Drawing.Point(265, 274);
            this.Btn_Cancel.Name = "Btn_Cancel";
            this.Btn_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Btn_Cancel.TabIndex = 35;
            this.Btn_Cancel.Text = "Abbrechen";
            this.Btn_Cancel.UseVisualStyleBackColor = true;
            this.Btn_Cancel.Click += new System.EventHandler(this.Btn_Cancel_Click);
            // 
            // Lbl_receipt
            // 
            this.Lbl_receipt.AutoSize = true;
            this.Lbl_receipt.Location = new System.Drawing.Point(15, 12);
            this.Lbl_receipt.Name = "Lbl_receipt";
            this.Lbl_receipt.Size = new System.Drawing.Size(51, 13);
            this.Lbl_receipt.TabIndex = 0;
            this.Lbl_receipt.Text = "Beleg Nr.";
            this.toolTip1.SetToolTip(this.Lbl_receipt, "Nummer des zugehörigen Beleges im Beleg-Ordner.\r\nDie nummer ist der erste Teil de" +
        "s Beleg-Namen: Nr_Name");
            // 
            // TxtBox_receiptNr
            // 
            this.TxtBox_receiptNr.Location = new System.Drawing.Point(73, 6);
            this.TxtBox_receiptNr.Name = "TxtBox_receiptNr";
            this.TxtBox_receiptNr.Size = new System.Drawing.Size(118, 20);
            this.TxtBox_receiptNr.TabIndex = 1;
            this.TxtBox_receiptNr.Text = "0";
            this.toolTip1.SetToolTip(this.TxtBox_receiptNr, "Nummer des zugehörigen Beleges im Beleg-Ordner.\r\nDie nummer ist der erste Teil de" +
        "s Beleg-Namen: Nr_Name");
            this.TxtBox_receiptNr.Validating += new System.ComponentModel.CancelEventHandler(this.txtBox_receiptNr_Validating);
            // 
            // Dtp_timeStamp
            // 
            this.Dtp_timeStamp.CustomFormat = "dd.MM.yyyy HH:mm";
            this.Dtp_timeStamp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Dtp_timeStamp.Location = new System.Drawing.Point(263, 6);
            this.Dtp_timeStamp.Name = "Dtp_timeStamp";
            this.Dtp_timeStamp.Size = new System.Drawing.Size(165, 20);
            this.Dtp_timeStamp.TabIndex = 3;
            // 
            // Lbl_category
            // 
            this.Lbl_category.AutoSize = true;
            this.Lbl_category.Location = new System.Drawing.Point(15, 35);
            this.Lbl_category.Name = "Lbl_category";
            this.Lbl_category.Size = new System.Drawing.Size(52, 13);
            this.Lbl_category.TabIndex = 6;
            this.Lbl_category.Text = "Kategorie";
            // 
            // Lbl_comment
            // 
            this.Lbl_comment.AutoSize = true;
            this.Lbl_comment.Location = new System.Drawing.Point(197, 35);
            this.Lbl_comment.Name = "Lbl_comment";
            this.Lbl_comment.Size = new System.Drawing.Size(60, 13);
            this.Lbl_comment.TabIndex = 8;
            this.Lbl_comment.Text = "Kommentar";
            // 
            // TxtBox_comment
            // 
            this.TxtBox_comment.Location = new System.Drawing.Point(263, 32);
            this.TxtBox_comment.Name = "TxtBox_comment";
            this.TxtBox_comment.Size = new System.Drawing.Size(227, 20);
            this.TxtBox_comment.TabIndex = 9;
            this.TxtBox_comment.TextChanged += new System.EventHandler(this.TxtBox_comment_TextChanged);
            // 
            // Lbl_ID
            // 
            this.Lbl_ID.AutoSize = true;
            this.Lbl_ID.Location = new System.Drawing.Point(434, 9);
            this.Lbl_ID.Name = "Lbl_ID";
            this.Lbl_ID.Size = new System.Drawing.Size(16, 13);
            this.Lbl_ID.TabIndex = 4;
            this.Lbl_ID.Text = "Id";
            this.toolTip1.SetToolTip(this.Lbl_ID, "Die vom Programm vergebene, eindeutige ID der Transaktion");
            // 
            // Lbl_showID
            // 
            this.Lbl_showID.AutoSize = true;
            this.Lbl_showID.Location = new System.Drawing.Point(458, 9);
            this.Lbl_showID.Name = "Lbl_showID";
            this.Lbl_showID.Size = new System.Drawing.Size(0, 13);
            this.Lbl_showID.TabIndex = 5;
            this.toolTip1.SetToolTip(this.Lbl_showID, "Die vom Programm vergebene, eindeutige ID der Transaktion");
            // 
            // Lbl_timeStamp
            // 
            this.Lbl_timeStamp.AutoSize = true;
            this.Lbl_timeStamp.Location = new System.Drawing.Point(197, 9);
            this.Lbl_timeStamp.Name = "Lbl_timeStamp";
            this.Lbl_timeStamp.Size = new System.Drawing.Size(25, 13);
            this.Lbl_timeStamp.TabIndex = 2;
            this.Lbl_timeStamp.Text = "Zeit";
            // 
            // TxtBox_category
            // 
            this.TxtBox_category.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.TxtBox_category.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.TxtBox_category.DropDownWidth = 118;
            this.TxtBox_category.FormattingEnabled = true;
            this.TxtBox_category.Location = new System.Drawing.Point(73, 32);
            this.TxtBox_category.Name = "TxtBox_category";
            this.TxtBox_category.Size = new System.Drawing.Size(118, 21);
            this.TxtBox_category.TabIndex = 7;
            // 
            // Grp_active
            // 
            this.Grp_active.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Grp_active.Controls.Add(this.Lbl_SumActive);
            this.Grp_active.Controls.Add(this.LayoutPan_active);
            this.Grp_active.Controls.Add(this.Btn_rightAdd);
            this.Grp_active.Location = new System.Drawing.Point(258, 58);
            this.Grp_active.Name = "Grp_active";
            this.Grp_active.Size = new System.Drawing.Size(240, 210);
            this.Grp_active.TabIndex = 11;
            this.Grp_active.TabStop = false;
            this.Grp_active.Text = "Aktiva";
            this.toolTip1.SetToolTip(this.Grp_active, "Liste der passiven Posten der Transaktion");
            // 
            // Lbl_SumActive
            // 
            this.Lbl_SumActive.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Lbl_SumActive.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.Lbl_SumActive.Location = new System.Drawing.Point(9, 183);
            this.Lbl_SumActive.Name = "Lbl_SumActive";
            this.Lbl_SumActive.Size = new System.Drawing.Size(222, 17);
            this.Lbl_SumActive.TabIndex = 57;
            this.Lbl_SumActive.Text = "0.00 €";
            this.Lbl_SumActive.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.Lbl_SumActive, "Summe der aktiven Posten");
            // 
            // LayoutPan_active
            // 
            this.LayoutPan_active.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LayoutPan_active.AutoScroll = true;
            this.LayoutPan_active.ColumnCount = 2;
            this.LayoutPan_active.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.LayoutPan_active.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.LayoutPan_active.Controls.Add(this.TxtBox_right1, 1, 0);
            this.LayoutPan_active.Controls.Add(this.ComBox_right1, 0, 0);
            this.LayoutPan_active.Controls.Add(this.TxtBox_right2, 1, 1);
            this.LayoutPan_active.Controls.Add(this.ComBox_right5, 0, 4);
            this.LayoutPan_active.Controls.Add(this.TxtBox_right4, 1, 3);
            this.LayoutPan_active.Controls.Add(this.TxtBox_right3, 1, 2);
            this.LayoutPan_active.Controls.Add(this.TxtBox_right5, 1, 4);
            this.LayoutPan_active.Controls.Add(this.ComBox_right4, 0, 3);
            this.LayoutPan_active.Controls.Add(this.ComBox_right3, 0, 2);
            this.LayoutPan_active.Controls.Add(this.ComBox_right2, 0, 1);
            this.LayoutPan_active.Location = new System.Drawing.Point(6, 16);
            this.LayoutPan_active.Name = "LayoutPan_active";
            this.LayoutPan_active.RowCount = 5;
            this.LayoutPan_active.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.LayoutPan_active.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.LayoutPan_active.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.LayoutPan_active.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.LayoutPan_active.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.LayoutPan_active.Size = new System.Drawing.Size(228, 137);
            this.LayoutPan_active.TabIndex = 47;
            // 
            // TxtBox_right1
            // 
            this.TxtBox_right1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TxtBox_right1.Location = new System.Drawing.Point(139, 3);
            this.TxtBox_right1.Name = "TxtBox_right1";
            this.TxtBox_right1.Size = new System.Drawing.Size(86, 20);
            this.TxtBox_right1.TabIndex = 47;
            this.TxtBox_right1.Text = "0";
            // 
            // ComBox_right1
            // 
            this.ComBox_right1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComBox_right1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComBox_right1.FormattingEnabled = true;
            this.ComBox_right1.Location = new System.Drawing.Point(3, 3);
            this.ComBox_right1.Name = "ComBox_right1";
            this.ComBox_right1.Size = new System.Drawing.Size(130, 21);
            this.ComBox_right1.TabIndex = 46;
            // 
            // TxtBox_right2
            // 
            this.TxtBox_right2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TxtBox_right2.Location = new System.Drawing.Point(139, 30);
            this.TxtBox_right2.Name = "TxtBox_right2";
            this.TxtBox_right2.Size = new System.Drawing.Size(86, 20);
            this.TxtBox_right2.TabIndex = 48;
            this.TxtBox_right2.Text = "0";
            // 
            // ComBox_right5
            // 
            this.ComBox_right5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComBox_right5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComBox_right5.FormattingEnabled = true;
            this.ComBox_right5.Location = new System.Drawing.Point(3, 111);
            this.ComBox_right5.Name = "ComBox_right5";
            this.ComBox_right5.Size = new System.Drawing.Size(130, 21);
            this.ComBox_right5.TabIndex = 53;
            // 
            // TxtBox_right4
            // 
            this.TxtBox_right4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TxtBox_right4.Location = new System.Drawing.Point(139, 84);
            this.TxtBox_right4.Name = "TxtBox_right4";
            this.TxtBox_right4.Size = new System.Drawing.Size(86, 20);
            this.TxtBox_right4.TabIndex = 52;
            this.TxtBox_right4.Text = "0";
            // 
            // TxtBox_right3
            // 
            this.TxtBox_right3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TxtBox_right3.Location = new System.Drawing.Point(139, 57);
            this.TxtBox_right3.Name = "TxtBox_right3";
            this.TxtBox_right3.Size = new System.Drawing.Size(86, 20);
            this.TxtBox_right3.TabIndex = 50;
            this.TxtBox_right3.Text = "0";
            // 
            // TxtBox_right5
            // 
            this.TxtBox_right5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TxtBox_right5.Location = new System.Drawing.Point(139, 111);
            this.TxtBox_right5.Name = "TxtBox_right5";
            this.TxtBox_right5.Size = new System.Drawing.Size(86, 20);
            this.TxtBox_right5.TabIndex = 54;
            this.TxtBox_right5.Text = "0";
            // 
            // ComBox_right4
            // 
            this.ComBox_right4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComBox_right4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComBox_right4.FormattingEnabled = true;
            this.ComBox_right4.Location = new System.Drawing.Point(3, 84);
            this.ComBox_right4.Name = "ComBox_right4";
            this.ComBox_right4.Size = new System.Drawing.Size(130, 21);
            this.ComBox_right4.TabIndex = 51;
            // 
            // ComBox_right3
            // 
            this.ComBox_right3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComBox_right3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComBox_right3.FormattingEnabled = true;
            this.ComBox_right3.Location = new System.Drawing.Point(3, 57);
            this.ComBox_right3.Name = "ComBox_right3";
            this.ComBox_right3.Size = new System.Drawing.Size(130, 21);
            this.ComBox_right3.TabIndex = 49;
            // 
            // ComBox_right2
            // 
            this.ComBox_right2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComBox_right2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComBox_right2.FormattingEnabled = true;
            this.ComBox_right2.Location = new System.Drawing.Point(3, 30);
            this.ComBox_right2.Name = "ComBox_right2";
            this.ComBox_right2.Size = new System.Drawing.Size(130, 21);
            this.ComBox_right2.TabIndex = 47;
            // 
            // Btn_rightAdd
            // 
            this.Btn_rightAdd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_rightAdd.Location = new System.Drawing.Point(6, 157);
            this.Btn_rightAdd.Name = "Btn_rightAdd";
            this.Btn_rightAdd.Size = new System.Drawing.Size(226, 23);
            this.Btn_rightAdd.TabIndex = 55;
            this.Btn_rightAdd.Text = "+";
            this.toolTip1.SetToolTip(this.Btn_rightAdd, "Liste der aktiven Posten erweitern");
            this.Btn_rightAdd.UseVisualStyleBackColor = true;
            this.Btn_rightAdd.Click += new System.EventHandler(this.Btn_rightAdd_Click);
            // 
            // Grp_passive
            // 
            this.Grp_passive.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Grp_passive.Controls.Add(this.Lbl_SumPassive);
            this.Grp_passive.Controls.Add(this.LayoutPan_passive);
            this.Grp_passive.Controls.Add(this.Btn_leftAdd);
            this.Grp_passive.Location = new System.Drawing.Point(12, 58);
            this.Grp_passive.Name = "Grp_passive";
            this.Grp_passive.Size = new System.Drawing.Size(240, 210);
            this.Grp_passive.TabIndex = 10;
            this.Grp_passive.TabStop = false;
            this.Grp_passive.Text = "Passiva";
            this.toolTip1.SetToolTip(this.Grp_passive, "Liste der passiven Posten der Transaktion");
            // 
            // Lbl_SumPassive
            // 
            this.Lbl_SumPassive.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Lbl_SumPassive.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.Lbl_SumPassive.Location = new System.Drawing.Point(9, 183);
            this.Lbl_SumPassive.Name = "Lbl_SumPassive";
            this.Lbl_SumPassive.Size = new System.Drawing.Size(222, 17);
            this.Lbl_SumPassive.TabIndex = 23;
            this.Lbl_SumPassive.Text = "0.00 €";
            this.Lbl_SumPassive.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.Lbl_SumPassive, "Summe der passiven Posten");
            // 
            // LayoutPan_passive
            // 
            this.LayoutPan_passive.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LayoutPan_passive.AutoScroll = true;
            this.LayoutPan_passive.ColumnCount = 2;
            this.LayoutPan_passive.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.LayoutPan_passive.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.LayoutPan_passive.Controls.Add(this.ComBox_left1, 0, 0);
            this.LayoutPan_passive.Controls.Add(this.ComBox_left5, 0, 4);
            this.LayoutPan_passive.Controls.Add(this.TxtBox_left5, 1, 4);
            this.LayoutPan_passive.Controls.Add(this.TxtBox_left1, 1, 0);
            this.LayoutPan_passive.Controls.Add(this.ComBox_left2, 0, 1);
            this.LayoutPan_passive.Controls.Add(this.TxtBox_left2, 1, 1);
            this.LayoutPan_passive.Controls.Add(this.ComBox_left3, 0, 2);
            this.LayoutPan_passive.Controls.Add(this.TxtBox_left4, 1, 3);
            this.LayoutPan_passive.Controls.Add(this.ComBox_left4, 0, 3);
            this.LayoutPan_passive.Controls.Add(this.TxtBox_left3, 1, 2);
            this.LayoutPan_passive.Location = new System.Drawing.Point(6, 16);
            this.LayoutPan_passive.Name = "LayoutPan_passive";
            this.LayoutPan_passive.RowCount = 5;
            this.LayoutPan_passive.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.LayoutPan_passive.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.LayoutPan_passive.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.LayoutPan_passive.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.LayoutPan_passive.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.LayoutPan_passive.Size = new System.Drawing.Size(228, 137);
            this.LayoutPan_passive.TabIndex = 20;
            // 
            // ComBox_left1
            // 
            this.ComBox_left1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComBox_left1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComBox_left1.FormattingEnabled = true;
            this.ComBox_left1.Location = new System.Drawing.Point(3, 3);
            this.ComBox_left1.Name = "ComBox_left1";
            this.ComBox_left1.Size = new System.Drawing.Size(130, 21);
            this.ComBox_left1.TabIndex = 11;
            // 
            // ComBox_left5
            // 
            this.ComBox_left5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComBox_left5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComBox_left5.FormattingEnabled = true;
            this.ComBox_left5.Location = new System.Drawing.Point(3, 111);
            this.ComBox_left5.Name = "ComBox_left5";
            this.ComBox_left5.Size = new System.Drawing.Size(130, 21);
            this.ComBox_left5.TabIndex = 19;
            // 
            // TxtBox_left5
            // 
            this.TxtBox_left5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TxtBox_left5.Location = new System.Drawing.Point(139, 111);
            this.TxtBox_left5.Name = "TxtBox_left5";
            this.TxtBox_left5.Size = new System.Drawing.Size(86, 20);
            this.TxtBox_left5.TabIndex = 20;
            this.TxtBox_left5.Text = "0";
            // 
            // TxtBox_left1
            // 
            this.TxtBox_left1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TxtBox_left1.Location = new System.Drawing.Point(139, 3);
            this.TxtBox_left1.Name = "TxtBox_left1";
            this.TxtBox_left1.Size = new System.Drawing.Size(86, 20);
            this.TxtBox_left1.TabIndex = 12;
            this.TxtBox_left1.Text = "0";
            // 
            // ComBox_left2
            // 
            this.ComBox_left2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComBox_left2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComBox_left2.FormattingEnabled = true;
            this.ComBox_left2.Location = new System.Drawing.Point(3, 30);
            this.ComBox_left2.Name = "ComBox_left2";
            this.ComBox_left2.Size = new System.Drawing.Size(130, 21);
            this.ComBox_left2.TabIndex = 13;
            // 
            // TxtBox_left2
            // 
            this.TxtBox_left2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TxtBox_left2.Location = new System.Drawing.Point(139, 30);
            this.TxtBox_left2.Name = "TxtBox_left2";
            this.TxtBox_left2.Size = new System.Drawing.Size(86, 20);
            this.TxtBox_left2.TabIndex = 14;
            this.TxtBox_left2.Text = "0";
            // 
            // ComBox_left3
            // 
            this.ComBox_left3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComBox_left3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComBox_left3.FormattingEnabled = true;
            this.ComBox_left3.Location = new System.Drawing.Point(3, 57);
            this.ComBox_left3.Name = "ComBox_left3";
            this.ComBox_left3.Size = new System.Drawing.Size(130, 21);
            this.ComBox_left3.TabIndex = 15;
            // 
            // TxtBox_left4
            // 
            this.TxtBox_left4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TxtBox_left4.Location = new System.Drawing.Point(139, 84);
            this.TxtBox_left4.Name = "TxtBox_left4";
            this.TxtBox_left4.Size = new System.Drawing.Size(86, 20);
            this.TxtBox_left4.TabIndex = 18;
            this.TxtBox_left4.Text = "0";
            // 
            // ComBox_left4
            // 
            this.ComBox_left4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComBox_left4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComBox_left4.FormattingEnabled = true;
            this.ComBox_left4.Location = new System.Drawing.Point(3, 84);
            this.ComBox_left4.Name = "ComBox_left4";
            this.ComBox_left4.Size = new System.Drawing.Size(130, 21);
            this.ComBox_left4.TabIndex = 17;
            // 
            // TxtBox_left3
            // 
            this.TxtBox_left3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TxtBox_left3.Location = new System.Drawing.Point(139, 57);
            this.TxtBox_left3.Name = "TxtBox_left3";
            this.TxtBox_left3.Size = new System.Drawing.Size(86, 20);
            this.TxtBox_left3.TabIndex = 16;
            this.TxtBox_left3.Text = "0";
            // 
            // Btn_leftAdd
            // 
            this.Btn_leftAdd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_leftAdd.Location = new System.Drawing.Point(6, 157);
            this.Btn_leftAdd.Name = "Btn_leftAdd";
            this.Btn_leftAdd.Size = new System.Drawing.Size(226, 23);
            this.Btn_leftAdd.TabIndex = 21;
            this.Btn_leftAdd.Text = "+";
            this.toolTip1.SetToolTip(this.Btn_leftAdd, "Liste der passiven Posten erweitern");
            this.Btn_leftAdd.UseVisualStyleBackColor = true;
            this.Btn_leftAdd.Click += new System.EventHandler(this.Btn_leftAdd_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 8000;
            this.toolTip1.InitialDelay = 500;
            this.toolTip1.ReshowDelay = 100;
            // 
            // NewTransaction
            // 
            this.AcceptButton = this.Btn_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Btn_Cancel;
            this.ClientSize = new System.Drawing.Size(510, 305);
            this.Controls.Add(this.Grp_passive);
            this.Controls.Add(this.Grp_active);
            this.Controls.Add(this.TxtBox_category);
            this.Controls.Add(this.Lbl_timeStamp);
            this.Controls.Add(this.Lbl_showID);
            this.Controls.Add(this.Lbl_ID);
            this.Controls.Add(this.TxtBox_comment);
            this.Controls.Add(this.Lbl_comment);
            this.Controls.Add(this.Lbl_category);
            this.Controls.Add(this.Dtp_timeStamp);
            this.Controls.Add(this.TxtBox_receiptNr);
            this.Controls.Add(this.Lbl_receipt);
            this.Controls.Add(this.Btn_Cancel);
            this.Controls.Add(this.Btn_OK);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(526, 9999);
            this.MinimumSize = new System.Drawing.Size(526, 344);
            this.Name = "NewTransaction";
            this.Text = "Neuer Vorgang";
            this.Load += new System.EventHandler(this.NewTransaction_Load);
            this.Grp_active.ResumeLayout(false);
            this.LayoutPan_active.ResumeLayout(false);
            this.LayoutPan_active.PerformLayout();
            this.Grp_passive.ResumeLayout(false);
            this.LayoutPan_passive.ResumeLayout(false);
            this.LayoutPan_passive.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.Button Btn_Cancel;
        private System.Windows.Forms.Label Lbl_receipt;
        private System.Windows.Forms.TextBox TxtBox_receiptNr;
        private System.Windows.Forms.DateTimePicker Dtp_timeStamp;
        private System.Windows.Forms.Label Lbl_category;
        private System.Windows.Forms.Label Lbl_comment;
        private System.Windows.Forms.TextBox TxtBox_comment;
        private System.Windows.Forms.Label Lbl_ID;
        private System.Windows.Forms.Label Lbl_showID;
        private System.Windows.Forms.Label Lbl_timeStamp;
        private System.Windows.Forms.ComboBox TxtBox_category;
        private System.Windows.Forms.GroupBox Grp_active;
        private System.Windows.Forms.TableLayoutPanel LayoutPan_active;
        private System.Windows.Forms.TextBox TxtBox_right1;
        private System.Windows.Forms.ComboBox ComBox_right1;
        private System.Windows.Forms.TextBox TxtBox_right2;
        private System.Windows.Forms.ComboBox ComBox_right5;
        private System.Windows.Forms.TextBox TxtBox_right4;
        private System.Windows.Forms.TextBox TxtBox_right3;
        private System.Windows.Forms.TextBox TxtBox_right5;
        private System.Windows.Forms.ComboBox ComBox_right4;
        private System.Windows.Forms.ComboBox ComBox_right3;
        private System.Windows.Forms.ComboBox ComBox_right2;
        private System.Windows.Forms.Button Btn_rightAdd;
        private System.Windows.Forms.GroupBox Grp_passive;
        private System.Windows.Forms.TableLayoutPanel LayoutPan_passive;
        private System.Windows.Forms.ComboBox ComBox_left1;
        private System.Windows.Forms.ComboBox ComBox_left5;
        private System.Windows.Forms.TextBox TxtBox_left5;
        private System.Windows.Forms.TextBox TxtBox_left1;
        private System.Windows.Forms.ComboBox ComBox_left2;
        private System.Windows.Forms.TextBox TxtBox_left2;
        private System.Windows.Forms.ComboBox ComBox_left3;
        private System.Windows.Forms.TextBox TxtBox_left4;
        private System.Windows.Forms.ComboBox ComBox_left4;
        private System.Windows.Forms.TextBox TxtBox_left3;
        private System.Windows.Forms.Button Btn_leftAdd;
        private System.Windows.Forms.Label Lbl_SumActive;
        private System.Windows.Forms.Label Lbl_SumPassive;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}