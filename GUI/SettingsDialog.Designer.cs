﻿using System;

namespace Quaestor.GUI
{
    partial class SettingsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsDialog));
            this.Grp_mail = new System.Windows.Forms.GroupBox();
            this.Lbl_sub = new System.Windows.Forms.Label();
            this.TxtBox_Subject = new System.Windows.Forms.TextBox();
            this.Lbl_th = new System.Windows.Forms.Label();
            this.Lbl_pwd = new System.Windows.Forms.Label();
            this.Lbl_pt = new System.Windows.Forms.Label();
            this.Lbl_sd = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Lbl_sv = new System.Windows.Forms.Label();
            this.TxtBox_Threads = new System.Windows.Forms.TextBox();
            this.TxtBox_Sender = new System.Windows.Forms.TextBox();
            this.TxtBox_Password = new System.Windows.Forms.TextBox();
            this.TxtBox_User = new System.Windows.Forms.TextBox();
            this.TxtBox_Port = new System.Windows.Forms.TextBox();
            this.TxtBox_Server = new System.Windows.Forms.TextBox();
            this.Btn_OK = new System.Windows.Forms.Button();
            this.Btn_cancel = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.Grp_mail.SuspendLayout();
            this.SuspendLayout();
            // 
            // Grp_mail
            // 
            this.Grp_mail.Controls.Add(this.Lbl_sub);
            this.Grp_mail.Controls.Add(this.TxtBox_Subject);
            this.Grp_mail.Controls.Add(this.Lbl_th);
            this.Grp_mail.Controls.Add(this.Lbl_pwd);
            this.Grp_mail.Controls.Add(this.Lbl_pt);
            this.Grp_mail.Controls.Add(this.Lbl_sd);
            this.Grp_mail.Controls.Add(this.label1);
            this.Grp_mail.Controls.Add(this.Lbl_sv);
            this.Grp_mail.Controls.Add(this.TxtBox_Threads);
            this.Grp_mail.Controls.Add(this.TxtBox_Sender);
            this.Grp_mail.Controls.Add(this.TxtBox_Password);
            this.Grp_mail.Controls.Add(this.TxtBox_User);
            this.Grp_mail.Controls.Add(this.TxtBox_Port);
            this.Grp_mail.Controls.Add(this.TxtBox_Server);
            this.Grp_mail.Cursor = System.Windows.Forms.Cursors.Default;
            this.Grp_mail.Location = new System.Drawing.Point(12, 12);
            this.Grp_mail.Name = "Grp_mail";
            this.Grp_mail.Size = new System.Drawing.Size(480, 136);
            this.Grp_mail.TabIndex = 0;
            this.Grp_mail.TabStop = false;
            this.Grp_mail.Text = "Email";
            // 
            // Lbl_sub
            // 
            this.Lbl_sub.AutoSize = true;
            this.Lbl_sub.Location = new System.Drawing.Point(39, 100);
            this.Lbl_sub.Name = "Lbl_sub";
            this.Lbl_sub.Size = new System.Drawing.Size(36, 13);
            this.Lbl_sub.TabIndex = 13;
            this.Lbl_sub.Text = "Kasse";
            this.toolTip1.SetToolTip(this.Lbl_sub, "Der Name der Kasse. Wird im Betreff der Emails benutzt");
            // 
            // TxtBox_Subject
            // 
            this.TxtBox_Subject.Location = new System.Drawing.Point(83, 97);
            this.TxtBox_Subject.Name = "TxtBox_Subject";
            this.TxtBox_Subject.Size = new System.Drawing.Size(185, 20);
            this.TxtBox_Subject.TabIndex = 12;
            this.TxtBox_Subject.Text = "Aktivenkasse";
            this.toolTip1.SetToolTip(this.TxtBox_Subject, "Der Name der Kasse. Wird im Betreff der Emails benutzt");
            // 
            // Lbl_th
            // 
            this.Lbl_th.AutoSize = true;
            this.Lbl_th.Location = new System.Drawing.Point(318, 74);
            this.Lbl_th.Name = "Lbl_th";
            this.Lbl_th.Size = new System.Drawing.Size(46, 13);
            this.Lbl_th.TabIndex = 11;
            this.Lbl_th.Text = "Threads";
            this.toolTip1.SetToolTip(this.Lbl_th, "Wird zur Beschleunigung des Sende-Vorgangs benutzt. Legt fest, wie viele Threads " +
        "gleichzeitig Emails senden");
            // 
            // Lbl_pwd
            // 
            this.Lbl_pwd.AutoSize = true;
            this.Lbl_pwd.Location = new System.Drawing.Point(314, 48);
            this.Lbl_pwd.Name = "Lbl_pwd";
            this.Lbl_pwd.Size = new System.Drawing.Size(50, 13);
            this.Lbl_pwd.TabIndex = 10;
            this.Lbl_pwd.Text = "Passwort";
            this.toolTip1.SetToolTip(this.Lbl_pwd, "Das Passwort des Email Kontos");
            // 
            // Lbl_pt
            // 
            this.Lbl_pt.AutoSize = true;
            this.Lbl_pt.Location = new System.Drawing.Point(338, 22);
            this.Lbl_pt.Name = "Lbl_pt";
            this.Lbl_pt.Size = new System.Drawing.Size(26, 13);
            this.Lbl_pt.TabIndex = 9;
            this.Lbl_pt.Text = "Port";
            this.toolTip1.SetToolTip(this.Lbl_pt, "Der Port des SMTP Servers");
            // 
            // Lbl_sd
            // 
            this.Lbl_sd.AutoSize = true;
            this.Lbl_sd.Location = new System.Drawing.Point(25, 74);
            this.Lbl_sd.Name = "Lbl_sd";
            this.Lbl_sd.Size = new System.Drawing.Size(52, 13);
            this.Lbl_sd.TabIndex = 8;
            this.Lbl_sd.Text = "Absender";
            this.toolTip1.SetToolTip(this.Lbl_sd, "Der Absender Name. Manche Server erlauben hier keinen anderen Namen als den Benut" +
        "zernamen");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Benutzername";
            this.toolTip1.SetToolTip(this.label1, "Der Benutzername des Email Kontos");
            // 
            // Lbl_sv
            // 
            this.Lbl_sv.AutoSize = true;
            this.Lbl_sv.Location = new System.Drawing.Point(6, 22);
            this.Lbl_sv.Name = "Lbl_sv";
            this.Lbl_sv.Size = new System.Drawing.Size(71, 13);
            this.Lbl_sv.TabIndex = 6;
            this.Lbl_sv.Text = "SMTP-Server";
            this.toolTip1.SetToolTip(this.Lbl_sv, "Der SMTP Server, über den die Emails versendet werden sollen");
            // 
            // TxtBox_Threads
            // 
            this.TxtBox_Threads.Location = new System.Drawing.Point(374, 71);
            this.TxtBox_Threads.Name = "TxtBox_Threads";
            this.TxtBox_Threads.Size = new System.Drawing.Size(100, 20);
            this.TxtBox_Threads.TabIndex = 5;
            this.TxtBox_Threads.Text = "10";
            this.toolTip1.SetToolTip(this.TxtBox_Threads, "Wird zur Beschleunigung des Sende-Vorgangs benutzt.  Legt fest, wie viele Threads" +
        " gleichzeitig Emails senden");
            // 
            // TxtBox_Sender
            // 
            this.TxtBox_Sender.Location = new System.Drawing.Point(83, 71);
            this.TxtBox_Sender.Name = "TxtBox_Sender";
            this.TxtBox_Sender.Size = new System.Drawing.Size(185, 20);
            this.TxtBox_Sender.TabIndex = 4;
            this.TxtBox_Sender.Text = "max.mustermann@rwth-aachen.de";
            this.toolTip1.SetToolTip(this.TxtBox_Sender, "Der Absender Name. Manche Server erlauben hier keinen anderen Namen als den Benut" +
        "zernamen");
            // 
            // TxtBox_Password
            // 
            this.TxtBox_Password.Location = new System.Drawing.Point(374, 45);
            this.TxtBox_Password.Name = "TxtBox_Password";
            this.TxtBox_Password.Size = new System.Drawing.Size(100, 20);
            this.TxtBox_Password.TabIndex = 3;
            this.TxtBox_Password.Text = "123456";
            this.toolTip1.SetToolTip(this.TxtBox_Password, "Das Passwort des Email Kontos");
            this.TxtBox_Password.Click += new System.EventHandler(this.TxtBox_Password_Click);
            this.TxtBox_Password.TextChanged += new System.EventHandler(this.TxtBox_Password_TextChanged);
            // 
            // TxtBox_User
            // 
            this.TxtBox_User.Location = new System.Drawing.Point(83, 45);
            this.TxtBox_User.Name = "TxtBox_User";
            this.TxtBox_User.Size = new System.Drawing.Size(185, 20);
            this.TxtBox_User.TabIndex = 2;
            this.TxtBox_User.Text = "mm123456@rwth-aachen.de";
            this.toolTip1.SetToolTip(this.TxtBox_User, "Der Benutzername des Email Kontos");
            // 
            // TxtBox_Port
            // 
            this.TxtBox_Port.Location = new System.Drawing.Point(374, 19);
            this.TxtBox_Port.Name = "TxtBox_Port";
            this.TxtBox_Port.Size = new System.Drawing.Size(100, 20);
            this.TxtBox_Port.TabIndex = 1;
            this.TxtBox_Port.Text = "587";
            this.toolTip1.SetToolTip(this.TxtBox_Port, "Der Port des SMTP Servers");
            // 
            // TxtBox_Server
            // 
            this.TxtBox_Server.Location = new System.Drawing.Point(83, 19);
            this.TxtBox_Server.Name = "TxtBox_Server";
            this.TxtBox_Server.Size = new System.Drawing.Size(185, 20);
            this.TxtBox_Server.TabIndex = 0;
            this.TxtBox_Server.Text = "mail.rwth-aachen.de";
            this.toolTip1.SetToolTip(this.TxtBox_Server, "Der SMTP Server, über den die Emails versendet werden sollen");
            // 
            // Btn_OK
            // 
            this.Btn_OK.Location = new System.Drawing.Point(260, 154);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(113, 23);
            this.Btn_OK.TabIndex = 6;
            this.Btn_OK.Text = "OK";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_OK_Click);
            // 
            // Btn_cancel
            // 
            this.Btn_cancel.CausesValidation = false;
            this.Btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Btn_cancel.Location = new System.Drawing.Point(379, 154);
            this.Btn_cancel.Name = "Btn_cancel";
            this.Btn_cancel.Size = new System.Drawing.Size(113, 23);
            this.Btn_cancel.TabIndex = 7;
            this.Btn_cancel.Text = "Abbrechen";
            this.Btn_cancel.UseVisualStyleBackColor = true;
            this.Btn_cancel.Click += new System.EventHandler(this.Btn_cancel_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 8000;
            this.toolTip1.InitialDelay = 500;
            this.toolTip1.ReshowDelay = 100;
            // 
            // SettingsDialog
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuPopup;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Btn_cancel;
            this.ClientSize = new System.Drawing.Size(504, 189);
            this.Controls.Add(this.Btn_cancel);
            this.Controls.Add(this.Btn_OK);
            this.Controls.Add(this.Grp_mail);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SettingsDialog";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Einstellungen";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingsDialog_FormClosing);
            this.Load += new System.EventHandler(this.SettingsDialog_Load);
            this.Grp_mail.ResumeLayout(false);
            this.Grp_mail.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Grp_mail;
        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.Button Btn_cancel;
        private System.Windows.Forms.TextBox TxtBox_Threads;
        private System.Windows.Forms.TextBox TxtBox_Sender;
        private System.Windows.Forms.TextBox TxtBox_Password;
        private System.Windows.Forms.TextBox TxtBox_User;
        private System.Windows.Forms.TextBox TxtBox_Port;
        private System.Windows.Forms.TextBox TxtBox_Server;
        private System.Windows.Forms.Label Lbl_sv;
        private System.Windows.Forms.Label Lbl_th;
        private System.Windows.Forms.Label Lbl_pwd;
        private System.Windows.Forms.Label Lbl_pt;
        private System.Windows.Forms.Label Lbl_sd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Lbl_sub;
        private System.Windows.Forms.TextBox TxtBox_Subject;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}