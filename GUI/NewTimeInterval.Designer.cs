﻿namespace Quaestor.GUI
{
    partial class NewTimeInterval
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewTimeInterval));
            this.Grp_oldTimeInterval = new System.Windows.Forms.GroupBox();
            this.LayoutPan_oldTimeInt = new System.Windows.Forms.TableLayoutPanel();
            this.Rbtn_oldTimeInterval_new = new System.Windows.Forms.RadioButton();
            this.ChkListBox_activeAccounts = new System.Windows.Forms.CheckedListBox();
            this.Grp_activeAccounts = new System.Windows.Forms.GroupBox();
            this.Grp_passiveAccounts = new System.Windows.Forms.GroupBox();
            this.ChkListBox_passiveAccounts = new System.Windows.Forms.CheckedListBox();
            this.ChkBox_planValues = new System.Windows.Forms.CheckBox();
            this.Grp_data = new System.Windows.Forms.GroupBox();
            this.Lbl_timeName = new System.Windows.Forms.Label();
            this.Lbl_adminName = new System.Windows.Forms.Label();
            this.TxtBox_timeName = new System.Windows.Forms.TextBox();
            this.TxtBox_adminName = new System.Windows.Forms.TextBox();
            this.Btn_OK = new System.Windows.Forms.Button();
            this.Btn_cancel = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.Grp_oldTimeInterval.SuspendLayout();
            this.LayoutPan_oldTimeInt.SuspendLayout();
            this.Grp_activeAccounts.SuspendLayout();
            this.Grp_passiveAccounts.SuspendLayout();
            this.Grp_data.SuspendLayout();
            this.SuspendLayout();
            // 
            // Grp_oldTimeInterval
            // 
            this.Grp_oldTimeInterval.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Grp_oldTimeInterval.Controls.Add(this.LayoutPan_oldTimeInt);
            this.Grp_oldTimeInterval.Location = new System.Drawing.Point(12, 12);
            this.Grp_oldTimeInterval.Name = "Grp_oldTimeInterval";
            this.Grp_oldTimeInterval.Size = new System.Drawing.Size(190, 148);
            this.Grp_oldTimeInterval.TabIndex = 0;
            this.Grp_oldTimeInterval.TabStop = false;
            this.Grp_oldTimeInterval.Text = "1. Vorheriges Semester auswählen";
            this.toolTip1.SetToolTip(this.Grp_oldTimeInterval, "Das Semester auswählen, auf dem das neue Semester basieren soll");
            // 
            // LayoutPan_oldTimeInt
            // 
            this.LayoutPan_oldTimeInt.AutoScroll = true;
            this.LayoutPan_oldTimeInt.ColumnCount = 1;
            this.LayoutPan_oldTimeInt.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.LayoutPan_oldTimeInt.Controls.Add(this.Rbtn_oldTimeInterval_new, 0, 0);
            this.LayoutPan_oldTimeInt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutPan_oldTimeInt.Location = new System.Drawing.Point(3, 16);
            this.LayoutPan_oldTimeInt.Name = "LayoutPan_oldTimeInt";
            this.LayoutPan_oldTimeInt.RowCount = 1;
            this.LayoutPan_oldTimeInt.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.LayoutPan_oldTimeInt.Size = new System.Drawing.Size(184, 129);
            this.LayoutPan_oldTimeInt.TabIndex = 4;
            // 
            // Rbtn_oldTimeInterval_new
            // 
            this.Rbtn_oldTimeInterval_new.AutoSize = true;
            this.Rbtn_oldTimeInterval_new.CheckAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.Rbtn_oldTimeInterval_new.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Rbtn_oldTimeInterval_new.Location = new System.Drawing.Point(3, 109);
            this.Rbtn_oldTimeInterval_new.Name = "Rbtn_oldTimeInterval_new";
            this.Rbtn_oldTimeInterval_new.Size = new System.Drawing.Size(178, 17);
            this.Rbtn_oldTimeInterval_new.TabIndex = 1;
            this.Rbtn_oldTimeInterval_new.TabStop = true;
            this.Rbtn_oldTimeInterval_new.Text = "Keins";
            this.Rbtn_oldTimeInterval_new.UseVisualStyleBackColor = true;
            // 
            // ChkListBox_activeAccounts
            // 
            this.ChkListBox_activeAccounts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ChkListBox_activeAccounts.CheckOnClick = true;
            this.ChkListBox_activeAccounts.FormattingEnabled = true;
            this.ChkListBox_activeAccounts.Location = new System.Drawing.Point(6, 19);
            this.ChkListBox_activeAccounts.Name = "ChkListBox_activeAccounts";
            this.ChkListBox_activeAccounts.Size = new System.Drawing.Size(208, 124);
            this.ChkListBox_activeAccounts.TabIndex = 1;
            // 
            // Grp_activeAccounts
            // 
            this.Grp_activeAccounts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Grp_activeAccounts.Controls.Add(this.ChkListBox_activeAccounts);
            this.Grp_activeAccounts.Location = new System.Drawing.Point(208, 12);
            this.Grp_activeAccounts.Name = "Grp_activeAccounts";
            this.Grp_activeAccounts.Size = new System.Drawing.Size(220, 148);
            this.Grp_activeAccounts.TabIndex = 2;
            this.Grp_activeAccounts.TabStop = false;
            this.Grp_activeAccounts.Text = "2. Benutzer/Aktiv-Nullkonten löschen";
            this.toolTip1.SetToolTip(this.Grp_activeAccounts, "Markierte Konten werden aus dem neuen Semester entfernt");
            // 
            // Grp_passiveAccounts
            // 
            this.Grp_passiveAccounts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Grp_passiveAccounts.Controls.Add(this.ChkListBox_passiveAccounts);
            this.Grp_passiveAccounts.Location = new System.Drawing.Point(434, 12);
            this.Grp_passiveAccounts.Name = "Grp_passiveAccounts";
            this.Grp_passiveAccounts.Size = new System.Drawing.Size(220, 148);
            this.Grp_passiveAccounts.TabIndex = 3;
            this.Grp_passiveAccounts.TabStop = false;
            this.Grp_passiveAccounts.Text = "3. Passivkonten löschen";
            this.toolTip1.SetToolTip(this.Grp_passiveAccounts, "Markierte Konten werden aus dem neuen Semester entfernt");
            // 
            // ChkListBox_passiveAccounts
            // 
            this.ChkListBox_passiveAccounts.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ChkListBox_passiveAccounts.CheckOnClick = true;
            this.ChkListBox_passiveAccounts.FormattingEnabled = true;
            this.ChkListBox_passiveAccounts.Location = new System.Drawing.Point(6, 19);
            this.ChkListBox_passiveAccounts.Name = "ChkListBox_passiveAccounts";
            this.ChkListBox_passiveAccounts.Size = new System.Drawing.Size(208, 124);
            this.ChkListBox_passiveAccounts.TabIndex = 1;
            // 
            // ChkBox_planValues
            // 
            this.ChkBox_planValues.AutoSize = true;
            this.ChkBox_planValues.Checked = true;
            this.ChkBox_planValues.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChkBox_planValues.Location = new System.Drawing.Point(9, 71);
            this.ChkBox_planValues.Name = "ChkBox_planValues";
            this.ChkBox_planValues.Size = new System.Drawing.Size(158, 17);
            this.ChkBox_planValues.TabIndex = 4;
            this.ChkBox_planValues.Text = "Planungswerte übernehmen";
            this.toolTip1.SetToolTip(this.ChkBox_planValues, "Sollen die Planwerte für den Haushaltsplan übernommen werden");
            this.ChkBox_planValues.UseVisualStyleBackColor = true;
            // 
            // Grp_data
            // 
            this.Grp_data.Controls.Add(this.Lbl_timeName);
            this.Grp_data.Controls.Add(this.ChkBox_planValues);
            this.Grp_data.Controls.Add(this.Lbl_adminName);
            this.Grp_data.Controls.Add(this.TxtBox_timeName);
            this.Grp_data.Controls.Add(this.TxtBox_adminName);
            this.Grp_data.Location = new System.Drawing.Point(660, 12);
            this.Grp_data.Name = "Grp_data";
            this.Grp_data.Size = new System.Drawing.Size(230, 98);
            this.Grp_data.TabIndex = 4;
            this.Grp_data.TabStop = false;
            this.Grp_data.Text = "4. Semesterdaten";
            // 
            // Lbl_timeName
            // 
            this.Lbl_timeName.AutoSize = true;
            this.Lbl_timeName.Location = new System.Drawing.Point(6, 48);
            this.Lbl_timeName.Name = "Lbl_timeName";
            this.Lbl_timeName.Size = new System.Drawing.Size(82, 13);
            this.Lbl_timeName.TabIndex = 3;
            this.Lbl_timeName.Text = "Semester-Name";
            this.toolTip1.SetToolTip(this.Lbl_timeName, "Der (eindeutige) Name des Semesters. Format SS20/ WS1920");
            // 
            // Lbl_adminName
            // 
            this.Lbl_adminName.AutoSize = true;
            this.Lbl_adminName.Location = new System.Drawing.Point(6, 22);
            this.Lbl_adminName.Name = "Lbl_adminName";
            this.Lbl_adminName.Size = new System.Drawing.Size(81, 13);
            this.Lbl_adminName.TabIndex = 2;
            this.Lbl_adminName.Text = "Quaestor-Name";
            this.toolTip1.SetToolTip(this.Lbl_adminName, "Der Name des Quaestors. Kann später nicht geändert werden. Wird in Emails benutzt" +
        "");
            // 
            // TxtBox_timeName
            // 
            this.TxtBox_timeName.Location = new System.Drawing.Point(92, 45);
            this.TxtBox_timeName.Name = "TxtBox_timeName";
            this.TxtBox_timeName.Size = new System.Drawing.Size(132, 20);
            this.TxtBox_timeName.TabIndex = 1;
            this.TxtBox_timeName.Text = "SS14";
            this.TxtBox_timeName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.TxtBox_timeName, "Der (eindeutige) Name des Semesters. Format SS20/ WS1920");
            this.TxtBox_timeName.Validating += new System.ComponentModel.CancelEventHandler(this.TxtBox_timeName_Validating);
            // 
            // TxtBox_adminName
            // 
            this.TxtBox_adminName.Location = new System.Drawing.Point(92, 18);
            this.TxtBox_adminName.Name = "TxtBox_adminName";
            this.TxtBox_adminName.Size = new System.Drawing.Size(132, 20);
            this.TxtBox_adminName.TabIndex = 0;
            this.TxtBox_adminName.Text = "Tetris";
            this.TxtBox_adminName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.TxtBox_adminName, "Der Name des Quaestors. Kann später nicht geändert werden. Wird in Emails benutzt" +
        "");
            this.TxtBox_adminName.Validating += new System.ComponentModel.CancelEventHandler(this.TxtBox_adminName_Validating);
            // 
            // Btn_OK
            // 
            this.Btn_OK.Location = new System.Drawing.Point(660, 116);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(113, 23);
            this.Btn_OK.TabIndex = 5;
            this.Btn_OK.Text = "OK";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_OK_Click);
            // 
            // Btn_cancel
            // 
            this.Btn_cancel.CausesValidation = false;
            this.Btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Btn_cancel.Location = new System.Drawing.Point(777, 116);
            this.Btn_cancel.Name = "Btn_cancel";
            this.Btn_cancel.Size = new System.Drawing.Size(113, 23);
            this.Btn_cancel.TabIndex = 6;
            this.Btn_cancel.Text = "Abbrechen";
            this.Btn_cancel.UseVisualStyleBackColor = true;
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 8000;
            this.toolTip1.InitialDelay = 500;
            this.toolTip1.ReshowDelay = 100;
            // 
            // NewTimeInterval
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.CancelButton = this.Btn_cancel;
            this.ClientSize = new System.Drawing.Size(904, 181);
            this.Controls.Add(this.Btn_cancel);
            this.Controls.Add(this.Btn_OK);
            this.Controls.Add(this.Grp_data);
            this.Controls.Add(this.Grp_passiveAccounts);
            this.Controls.Add(this.Grp_activeAccounts);
            this.Controls.Add(this.Grp_oldTimeInterval);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(920, 9999999);
            this.MinimumSize = new System.Drawing.Size(223, 200);
            this.Name = "NewTimeInterval";
            this.Text = "Neues Semester";
            this.Load += new System.EventHandler(this.NewTimeInterval_Load);
            this.Grp_oldTimeInterval.ResumeLayout(false);
            this.LayoutPan_oldTimeInt.ResumeLayout(false);
            this.LayoutPan_oldTimeInt.PerformLayout();
            this.Grp_activeAccounts.ResumeLayout(false);
            this.Grp_passiveAccounts.ResumeLayout(false);
            this.Grp_data.ResumeLayout(false);
            this.Grp_data.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Grp_oldTimeInterval;
        private System.Windows.Forms.CheckedListBox ChkListBox_activeAccounts;
        private System.Windows.Forms.GroupBox Grp_activeAccounts;
        private System.Windows.Forms.GroupBox Grp_passiveAccounts;
        private System.Windows.Forms.CheckedListBox ChkListBox_passiveAccounts;
        private System.Windows.Forms.CheckBox ChkBox_planValues;
        private System.Windows.Forms.GroupBox Grp_data;
        private System.Windows.Forms.Label Lbl_timeName;
        private System.Windows.Forms.Label Lbl_adminName;
        private System.Windows.Forms.TextBox TxtBox_timeName;
        private System.Windows.Forms.TextBox TxtBox_adminName;
        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.Button Btn_cancel;
        private System.Windows.Forms.TableLayoutPanel LayoutPan_oldTimeInt;
        private System.Windows.Forms.RadioButton Rbtn_oldTimeInterval_new;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}