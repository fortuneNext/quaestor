﻿namespace Quaestor.GUI
{
    partial class NewAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewAccount));
            this.Btn_OK = new System.Windows.Forms.Button();
            this.Btn_Cancel = new System.Windows.Forms.Button();
            this.TxtBox_name = new System.Windows.Forms.TextBox();
            this.Lbl_name = new System.Windows.Forms.Label();
            this.TxtBox_email = new System.Windows.Forms.TextBox();
            this.Lbl_email = new System.Windows.Forms.Label();
            this.Chkbox_printToOverview = new System.Windows.Forms.CheckBox();
            this.Rbtn_active = new System.Windows.Forms.RadioButton();
            this.Rbtn_passive = new System.Windows.Forms.RadioButton();
            this.Rbtn_user = new System.Windows.Forms.RadioButton();
            this.ChkListBox_subaccountTo = new System.Windows.Forms.CheckedListBox();
            this.ChkListBox_subaccounts = new System.Windows.Forms.CheckedListBox();
            this.ChkBox_structuralAccount = new System.Windows.Forms.CheckBox();
            this.Grp_accountType = new System.Windows.Forms.GroupBox();
            this.Lbl_planValue = new System.Windows.Forms.Label();
            this.TxtBox_planValue = new System.Windows.Forms.TextBox();
            this.SubCon_Subs = new System.Windows.Forms.SplitContainer();
            this.Lbl_subaccountTo = new System.Windows.Forms.Label();
            this.SplitCon_SubAcc = new System.Windows.Forms.SplitContainer();
            this.SplitCon_SubOf = new System.Windows.Forms.SplitContainer();
            this.Lbl_subaccounts = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.Grp_accountType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SubCon_Subs)).BeginInit();
            this.SubCon_Subs.Panel1.SuspendLayout();
            this.SubCon_Subs.Panel2.SuspendLayout();
            this.SubCon_Subs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitCon_SubAcc)).BeginInit();
            this.SplitCon_SubAcc.Panel1.SuspendLayout();
            this.SplitCon_SubAcc.Panel2.SuspendLayout();
            this.SplitCon_SubAcc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitCon_SubOf)).BeginInit();
            this.SplitCon_SubOf.Panel1.SuspendLayout();
            this.SplitCon_SubOf.Panel2.SuspendLayout();
            this.SplitCon_SubOf.SuspendLayout();
            this.SuspendLayout();
            // 
            // Btn_OK
            // 
            this.Btn_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_OK.Location = new System.Drawing.Point(12, 396);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(136, 23);
            this.Btn_OK.TabIndex = 18;
            this.Btn_OK.Text = "OK";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_OK_Click);
            // 
            // Btn_Cancel
            // 
            this.Btn_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Cancel.CausesValidation = false;
            this.Btn_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Btn_Cancel.Location = new System.Drawing.Point(154, 396);
            this.Btn_Cancel.Name = "Btn_Cancel";
            this.Btn_Cancel.Size = new System.Drawing.Size(136, 23);
            this.Btn_Cancel.TabIndex = 19;
            this.Btn_Cancel.Text = "Abbrechen";
            this.Btn_Cancel.UseVisualStyleBackColor = true;
            this.Btn_Cancel.Click += new System.EventHandler(this.Btn_Cancel_Click);
            // 
            // TxtBox_name
            // 
            this.TxtBox_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtBox_name.Location = new System.Drawing.Point(154, 6);
            this.TxtBox_name.Name = "TxtBox_name";
            this.TxtBox_name.Size = new System.Drawing.Size(136, 20);
            this.TxtBox_name.TabIndex = 1;
            this.TxtBox_name.TextChanged += new System.EventHandler(this.TxtBox_name_TextChanged);
            // 
            // Lbl_name
            // 
            this.Lbl_name.AutoSize = true;
            this.Lbl_name.Location = new System.Drawing.Point(12, 9);
            this.Lbl_name.Name = "Lbl_name";
            this.Lbl_name.Size = new System.Drawing.Size(35, 13);
            this.Lbl_name.TabIndex = 0;
            this.Lbl_name.Text = "Name";
            // 
            // TxtBox_email
            // 
            this.TxtBox_email.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtBox_email.Enabled = false;
            this.TxtBox_email.Location = new System.Drawing.Point(154, 111);
            this.TxtBox_email.Name = "TxtBox_email";
            this.TxtBox_email.Size = new System.Drawing.Size(136, 20);
            this.TxtBox_email.TabIndex = 9;
            this.TxtBox_email.Validating += new System.ComponentModel.CancelEventHandler(this.TxtBox_email_Validating);
            // 
            // Lbl_email
            // 
            this.Lbl_email.AutoSize = true;
            this.Lbl_email.Enabled = false;
            this.Lbl_email.Location = new System.Drawing.Point(12, 114);
            this.Lbl_email.Name = "Lbl_email";
            this.Lbl_email.Size = new System.Drawing.Size(32, 13);
            this.Lbl_email.TabIndex = 8;
            this.Lbl_email.Text = "Email";
            // 
            // Chkbox_printToOverview
            // 
            this.Chkbox_printToOverview.AutoSize = true;
            this.Chkbox_printToOverview.Location = new System.Drawing.Point(154, 32);
            this.Chkbox_printToOverview.Name = "Chkbox_printToOverview";
            this.Chkbox_printToOverview.Size = new System.Drawing.Size(136, 17);
            this.Chkbox_printToOverview.TabIndex = 3;
            this.Chkbox_printToOverview.Text = "Auf Übersicht anzeigen";
            this.toolTip1.SetToolTip(this.Chkbox_printToOverview, "Soll das Konto auf der Übersicht angezeigt werden");
            this.Chkbox_printToOverview.UseVisualStyleBackColor = true;
            // 
            // Rbtn_active
            // 
            this.Rbtn_active.AutoSize = true;
            this.Rbtn_active.Location = new System.Drawing.Point(6, 19);
            this.Rbtn_active.Name = "Rbtn_active";
            this.Rbtn_active.Size = new System.Drawing.Size(49, 17);
            this.Rbtn_active.TabIndex = 5;
            this.Rbtn_active.TabStop = true;
            this.Rbtn_active.Text = "Aktiv";
            this.Rbtn_active.UseVisualStyleBackColor = true;
            // 
            // Rbtn_passive
            // 
            this.Rbtn_passive.AutoSize = true;
            this.Rbtn_passive.Location = new System.Drawing.Point(105, 19);
            this.Rbtn_passive.Name = "Rbtn_passive";
            this.Rbtn_passive.Size = new System.Drawing.Size(56, 17);
            this.Rbtn_passive.TabIndex = 6;
            this.Rbtn_passive.TabStop = true;
            this.Rbtn_passive.Text = "Passiv";
            this.Rbtn_passive.UseVisualStyleBackColor = true;
            // 
            // Rbtn_user
            // 
            this.Rbtn_user.AutoSize = true;
            this.Rbtn_user.Location = new System.Drawing.Point(205, 19);
            this.Rbtn_user.Name = "Rbtn_user";
            this.Rbtn_user.Size = new System.Drawing.Size(67, 17);
            this.Rbtn_user.TabIndex = 7;
            this.Rbtn_user.TabStop = true;
            this.Rbtn_user.Text = "Benutzer";
            this.Rbtn_user.UseVisualStyleBackColor = true;
            this.Rbtn_user.CheckedChanged += new System.EventHandler(this.Rbtn_user_CheckedChanged);
            // 
            // ChkListBox_subaccountTo
            // 
            this.ChkListBox_subaccountTo.CheckOnClick = true;
            this.ChkListBox_subaccountTo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChkListBox_subaccountTo.FormattingEnabled = true;
            this.ChkListBox_subaccountTo.Location = new System.Drawing.Point(0, 0);
            this.ChkListBox_subaccountTo.Name = "ChkListBox_subaccountTo";
            this.ChkListBox_subaccountTo.Size = new System.Drawing.Size(137, 115);
            this.ChkListBox_subaccountTo.TabIndex = 15;
            this.toolTip1.SetToolTip(this.ChkListBox_subaccountTo, "Liste der Strukturkonten, zu denen das Konto hinzugefügt werden soll");
            // 
            // ChkListBox_subaccounts
            // 
            this.ChkListBox_subaccounts.CheckOnClick = true;
            this.ChkListBox_subaccounts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChkListBox_subaccounts.Enabled = false;
            this.ChkListBox_subaccounts.FormattingEnabled = true;
            this.ChkListBox_subaccounts.Location = new System.Drawing.Point(0, 0);
            this.ChkListBox_subaccounts.Name = "ChkListBox_subaccounts";
            this.ChkListBox_subaccounts.Size = new System.Drawing.Size(137, 108);
            this.ChkListBox_subaccounts.TabIndex = 17;
            this.toolTip1.SetToolTip(this.ChkListBox_subaccounts, "Liste der Konten, die als Unterkonto hinzugefügt werden sollen");
            // 
            // ChkBox_structuralAccount
            // 
            this.ChkBox_structuralAccount.AutoSize = true;
            this.ChkBox_structuralAccount.Location = new System.Drawing.Point(15, 32);
            this.ChkBox_structuralAccount.Name = "ChkBox_structuralAccount";
            this.ChkBox_structuralAccount.Size = new System.Drawing.Size(90, 17);
            this.ChkBox_structuralAccount.TabIndex = 2;
            this.ChkBox_structuralAccount.Text = "Strukturkonto";
            this.toolTip1.SetToolTip(this.ChkBox_structuralAccount, "Handelt es sich um ein Konto mit Unterkonten, ohne eigene Werte");
            this.ChkBox_structuralAccount.UseVisualStyleBackColor = true;
            this.ChkBox_structuralAccount.CheckedChanged += new System.EventHandler(this.ChkBox_structuralAccount_CheckedChanged);
            // 
            // Grp_accountType
            // 
            this.Grp_accountType.Controls.Add(this.Rbtn_active);
            this.Grp_accountType.Controls.Add(this.Rbtn_passive);
            this.Grp_accountType.Controls.Add(this.Rbtn_user);
            this.Grp_accountType.Location = new System.Drawing.Point(12, 55);
            this.Grp_accountType.Name = "Grp_accountType";
            this.Grp_accountType.Size = new System.Drawing.Size(278, 50);
            this.Grp_accountType.TabIndex = 4;
            this.Grp_accountType.TabStop = false;
            this.Grp_accountType.Text = "Kontotyp";
            this.toolTip1.SetToolTip(this.Grp_accountType, "Art des Kontos");
            // 
            // Lbl_planValue
            // 
            this.Lbl_planValue.AutoSize = true;
            this.Lbl_planValue.Location = new System.Drawing.Point(12, 140);
            this.Lbl_planValue.Name = "Lbl_planValue";
            this.Lbl_planValue.Size = new System.Drawing.Size(74, 13);
            this.Lbl_planValue.TabIndex = 12;
            this.Lbl_planValue.Text = "Haushaltsplan";
            this.toolTip1.SetToolTip(this.Lbl_planValue, "Der prognostizierte Endwert im Semester");
            // 
            // TxtBox_planValue
            // 
            this.TxtBox_planValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtBox_planValue.Location = new System.Drawing.Point(154, 137);
            this.TxtBox_planValue.Name = "TxtBox_planValue";
            this.TxtBox_planValue.Size = new System.Drawing.Size(136, 20);
            this.TxtBox_planValue.TabIndex = 13;
            this.TxtBox_planValue.Text = "0";
            this.toolTip1.SetToolTip(this.TxtBox_planValue, "Der prognostizierte Endwert im Semester");
            this.TxtBox_planValue.Validating += new System.ComponentModel.CancelEventHandler(this.TxtBox_planValue_Validating);
            // 
            // SubCon_Subs
            // 
            this.SubCon_Subs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SubCon_Subs.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.SubCon_Subs.IsSplitterFixed = true;
            this.SubCon_Subs.Location = new System.Drawing.Point(0, 0);
            this.SubCon_Subs.Name = "SubCon_Subs";
            // 
            // SubCon_Subs.Panel1
            // 
            this.SubCon_Subs.Panel1.Controls.Add(this.Lbl_subaccountTo);
            this.toolTip1.SetToolTip(this.SubCon_Subs.Panel1, "Zu welchen Strukturkonten das Konto hinzugefügt werden soll");
            // 
            // SubCon_Subs.Panel2
            // 
            this.SubCon_Subs.Panel2.Controls.Add(this.ChkListBox_subaccountTo);
            this.SubCon_Subs.Size = new System.Drawing.Size(278, 115);
            this.SubCon_Subs.SplitterDistance = 137;
            this.SubCon_Subs.TabIndex = 20;
            this.toolTip1.SetToolTip(this.SubCon_Subs, "Zu welchen Strukturkonten das Konto hinzugefügt werden soll");
            // 
            // Lbl_subaccountTo
            // 
            this.Lbl_subaccountTo.AutoSize = true;
            this.Lbl_subaccountTo.Dock = System.Windows.Forms.DockStyle.Top;
            this.Lbl_subaccountTo.Location = new System.Drawing.Point(0, 0);
            this.Lbl_subaccountTo.Name = "Lbl_subaccountTo";
            this.Lbl_subaccountTo.Size = new System.Drawing.Size(80, 26);
            this.Lbl_subaccountTo.TabIndex = 17;
            this.Lbl_subaccountTo.Text = "Als Unterkonto \r\nhinzufügen zu";
            this.toolTip1.SetToolTip(this.Lbl_subaccountTo, "Liste der Strukturkonten, zu denen das Konto hinzugefügt werden soll");
            // 
            // SplitCon_SubAcc
            // 
            this.SplitCon_SubAcc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SplitCon_SubAcc.IsSplitterFixed = true;
            this.SplitCon_SubAcc.Location = new System.Drawing.Point(12, 163);
            this.SplitCon_SubAcc.Name = "SplitCon_SubAcc";
            this.SplitCon_SubAcc.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // SplitCon_SubAcc.Panel1
            // 
            this.SplitCon_SubAcc.Panel1.Controls.Add(this.SplitCon_SubOf);
            // 
            // SplitCon_SubAcc.Panel2
            // 
            this.SplitCon_SubAcc.Panel2.Controls.Add(this.SubCon_Subs);
            this.SplitCon_SubAcc.Size = new System.Drawing.Size(278, 227);
            this.SplitCon_SubAcc.SplitterDistance = 108;
            this.SplitCon_SubAcc.TabIndex = 15;
            // 
            // SplitCon_SubOf
            // 
            this.SplitCon_SubOf.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitCon_SubOf.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.SplitCon_SubOf.IsSplitterFixed = true;
            this.SplitCon_SubOf.Location = new System.Drawing.Point(0, 0);
            this.SplitCon_SubOf.Name = "SplitCon_SubOf";
            // 
            // SplitCon_SubOf.Panel1
            // 
            this.SplitCon_SubOf.Panel1.Controls.Add(this.Lbl_subaccounts);
            // 
            // SplitCon_SubOf.Panel2
            // 
            this.SplitCon_SubOf.Panel2.Controls.Add(this.ChkListBox_subaccounts);
            this.SplitCon_SubOf.Size = new System.Drawing.Size(278, 108);
            this.SplitCon_SubOf.SplitterDistance = 137;
            this.SplitCon_SubOf.TabIndex = 0;
            this.toolTip1.SetToolTip(this.SplitCon_SubOf, "Die Unterkonten, wenn es sich um ein Strukturkonto handelt");
            // 
            // Lbl_subaccounts
            // 
            this.Lbl_subaccounts.AutoSize = true;
            this.Lbl_subaccounts.Dock = System.Windows.Forms.DockStyle.Top;
            this.Lbl_subaccounts.Enabled = false;
            this.Lbl_subaccounts.Location = new System.Drawing.Point(0, 0);
            this.Lbl_subaccounts.Name = "Lbl_subaccounts";
            this.Lbl_subaccounts.Size = new System.Drawing.Size(66, 13);
            this.Lbl_subaccounts.TabIndex = 17;
            this.Lbl_subaccounts.Text = "Unterkonten";
            this.toolTip1.SetToolTip(this.Lbl_subaccounts, "Liste der Konten, die als Unterkonto hinzugefügt werden sollen");
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 8000;
            this.toolTip1.InitialDelay = 500;
            this.toolTip1.ReshowDelay = 100;
            // 
            // NewAccount
            // 
            this.AcceptButton = this.Btn_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Btn_Cancel;
            this.ClientSize = new System.Drawing.Size(304, 431);
            this.Controls.Add(this.SplitCon_SubAcc);
            this.Controls.Add(this.TxtBox_planValue);
            this.Controls.Add(this.Lbl_planValue);
            this.Controls.Add(this.Grp_accountType);
            this.Controls.Add(this.ChkBox_structuralAccount);
            this.Controls.Add(this.Chkbox_printToOverview);
            this.Controls.Add(this.Lbl_email);
            this.Controls.Add(this.TxtBox_email);
            this.Controls.Add(this.Lbl_name);
            this.Controls.Add(this.TxtBox_name);
            this.Controls.Add(this.Btn_Cancel);
            this.Controls.Add(this.Btn_OK);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(320, 360);
            this.Name = "NewAccount";
            this.Text = "Neues Konto";
            this.Load += new System.EventHandler(this.NewAccount_Load);
            this.Grp_accountType.ResumeLayout(false);
            this.Grp_accountType.PerformLayout();
            this.SubCon_Subs.Panel1.ResumeLayout(false);
            this.SubCon_Subs.Panel1.PerformLayout();
            this.SubCon_Subs.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SubCon_Subs)).EndInit();
            this.SubCon_Subs.ResumeLayout(false);
            this.SplitCon_SubAcc.Panel1.ResumeLayout(false);
            this.SplitCon_SubAcc.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitCon_SubAcc)).EndInit();
            this.SplitCon_SubAcc.ResumeLayout(false);
            this.SplitCon_SubOf.Panel1.ResumeLayout(false);
            this.SplitCon_SubOf.Panel1.PerformLayout();
            this.SplitCon_SubOf.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitCon_SubOf)).EndInit();
            this.SplitCon_SubOf.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.Button Btn_Cancel;
        private System.Windows.Forms.TextBox TxtBox_name;
        private System.Windows.Forms.Label Lbl_name;
        private System.Windows.Forms.TextBox TxtBox_email;
        private System.Windows.Forms.Label Lbl_email;
        private System.Windows.Forms.CheckBox Chkbox_printToOverview;
        private System.Windows.Forms.RadioButton Rbtn_active;
        private System.Windows.Forms.RadioButton Rbtn_passive;
        private System.Windows.Forms.RadioButton Rbtn_user;
        private System.Windows.Forms.CheckedListBox ChkListBox_subaccountTo;
        private System.Windows.Forms.CheckedListBox ChkListBox_subaccounts;
        private System.Windows.Forms.CheckBox ChkBox_structuralAccount;
        private System.Windows.Forms.GroupBox Grp_accountType;
        private System.Windows.Forms.Label Lbl_planValue;
        private System.Windows.Forms.TextBox TxtBox_planValue;
        private System.Windows.Forms.SplitContainer SubCon_Subs;
        private System.Windows.Forms.SplitContainer SplitCon_SubAcc;
        private System.Windows.Forms.SplitContainer SplitCon_SubOf;
        private System.Windows.Forms.Label Lbl_subaccountTo;
        private System.Windows.Forms.Label Lbl_subaccounts;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}