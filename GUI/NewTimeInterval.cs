﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Windows.Forms;
using Quaestor.Logic;
using Quaestor.Properties;

namespace Quaestor.GUI
{
    public partial class NewTimeInterval : Form
    {
        private readonly List<RadioButton> _rbtns;
        private readonly Dictionary<RadioButton, int> _rbtnDict = new Dictionary<RadioButton, int>();
        /// <summary>
        /// The resulting logic of the new time interval
        /// </summary>
        public Logic.Logic ResultLogic { get; private set; }

        /// <summary>
        /// The Transaction in which the start values of the new interval are saved
        /// </summary>
        public Transaction LegacyTransaction;

        public NewTimeInterval()
        {
            InitializeComponent();
            _rbtns = new List<RadioButton> {Rbtn_oldTimeInterval_new};
            LayoutPan_oldTimeInt.Padding = new Padding(0, 0, SystemInformation.VerticalScrollBarWidth, 0);
        }

        private void NewTimeInterval_Load(object sender, EventArgs e)
        {
            DialogResult confirmDialog;
            List<Tuple<int, string, string>> reversedData = Logic.Logic.GetArchiveDataNames();

            //find out if current Semester is arcived
            if (!reversedData.Exists( s => s.Item2 == Program.Logic.TimeName)) 
            {
                //if not, ask if it should be archived
                confirmDialog = MessageBox.Show(strings.NewTimeIntervalArchive.Replace(@"%id%", Logic.Logic.GetNewArchiveId().ToString()).Replace(@"\n", Environment.NewLine),
                                                strings.ArchiveConfirmationTitle, MessageBoxButtons.YesNo);
                if (confirmDialog == DialogResult.Yes)
                {
                    if (Program.Logic.ArchiveState())
                    {
                        //if Arcived worked, reload archive
                        reversedData = Logic.Logic.GetArchiveDataNames();
                        MessageBox.Show(strings.ArchiveSuccessful);
                    } else
                        MessageBox.Show(strings.ArchiveError);
                }
            }
            //reverse archive, so first box is newest Semester
            reversedData.Reverse();

            foreach (var historyData in reversedData)
            {
                //Generate new Radiobutton
                var rbtnOldTimeInterval = new RadioButton
                {
                    Name = "rbtn_oldTimeInterval_" + historyData.Item1,
                    Text = historyData.Item1 + @": " + historyData.Item2 + @" (" + historyData.Item3 + @")",
                    Dock = DockStyle.Bottom,
                    AutoSize = true
                };
                //Put Button in Layoutpanel in right position
                LayoutPan_oldTimeInt.Controls.Add(rbtnOldTimeInterval, 0, LayoutPan_oldTimeInt.Controls.IndexOf(Rbtn_oldTimeInterval_new));
                LayoutPan_oldTimeInt.Controls.Add(Rbtn_oldTimeInterval_new, 0, LayoutPan_oldTimeInt.Controls.Count);
                rbtnOldTimeInterval.CheckedChanged += RefreshAll;
                _rbtnDict.Add(rbtnOldTimeInterval, historyData.Item1);
                _rbtns.Add(rbtnOldTimeInterval);
            }
            //check newest Semester, if one is archived
            if(_rbtns.Count > 1)
                _rbtns[1].Checked = true;

            TxtBox_adminName.Text = Program.Logic.AdminName;
            TxtBox_timeName.Text = Program.Logic.TimeName;
        }

        private int GetCheckedId()
        {
            if (Rbtn_oldTimeInterval_new.Checked)
                return -1;
            foreach (var rbtn in _rbtns.Where(rbtn => rbtn.Checked))
                return _rbtnDict[rbtn];
            MessageBox.Show(strings.ErrorNoCheckedIdButtonFound);
            return -2;
        }

        /// <summary>
        ///     Creates a Transaction with all Accounts and their current value and books them to a transfer account that replaces Start Values
        /// </summary>
        /// <param name="ProgramState">The Logic state, i.e. the accounts from which the transaction is generated</param>
        /// <returns>true if Legacy Transaction could be created, else returns false</returns>
        public bool CreateLegacyTransaction(Logic.Logic ProgramState)
        {

            var accounts = new List<ValueAccount>();
            ValueAccount newLegacyAccount;
            var values = new List<int>();
            int endValue = 0;

            /*
             * Loading from a file generates a LegacyTransaction, so you don't need to test if there is one
             */

            //add all accounts exept for LegacyAccount
            foreach (ValueAccount account in ProgramState.GetAccounts.OfType<ValueAccount>()
                     .Where(a => a.Name != Settings.Default.LegacyAccount && a.AccountType != AccountTypes.Passive
                     && a.GetValue() != 0))
            {
                    accounts.Add(account);
                    values.Add(account.GetValue());
            }

            //add together current value of all Accounts
            foreach (int value in values)
                endValue += value;

            //book endValue to LegacyAccount, if it exists; else create it first
            newLegacyAccount = (ValueAccount)ProgramState.StrToAccount(Settings.Default.LegacyAccount);
            if(newLegacyAccount == null) 
                ProgramState.NewValueAccount(0, 0, Settings.Default.LegacyAccount, AccountTypes.Passive, false);
            accounts.Add((ValueAccount)ProgramState.StrToAccount(Settings.Default.LegacyAccount));
            values.Add(endValue);

            //LegacyTransaction always has ID 0, as it is the first one
            int tempId = 0;

            LegacyTransaction = new Transaction(accounts, values, tempId,
                new DateTime(), 0, strings.NewTimeIntervalTransactionType, strings.NewTimeIntervalTransactionComment);

            if (LegacyTransaction.CheckValidity())
            {
                return true;
            }
            MessageBox.Show(strings.InvalidTransaction);
            return false;
        }

        private void RefreshAll(object sender, EventArgs e)
        {
            if (!((RadioButton) sender).Checked)
                return;
            ChkListBox_activeAccounts.Items.Clear();
            ChkListBox_activeAccounts.Enabled = sender != Rbtn_oldTimeInterval_new;
            ChkListBox_passiveAccounts.Items.Clear();
            ChkListBox_passiveAccounts.Enabled = sender != Rbtn_oldTimeInterval_new;
            if (sender == Rbtn_oldTimeInterval_new)
                return;
            var id = _rbtnDict[(RadioButton) sender];
            var archiveLogic = new Logic.Logic();
            archiveLogic.LoadStateFromArchive(id);
            foreach (var account in archiveLogic.GetAccounts.Where(account => account.AccountType != AccountTypes.Passive && account is ValueAccount).Where(account => account.GetValue() == 0))
                ChkListBox_activeAccounts.Items.Add(account.Name);
            foreach (var account in archiveLogic.GetAccounts.Where(account => account.AccountType == AccountTypes.Passive))
                ChkListBox_passiveAccounts.Items.Add(account.Name);
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            //if Semester name exists already in archive
            if (Logic.Logic.GetArchiveDataNames().Exists(s => s.Item2 == TxtBox_timeName.Text))
            {
                MessageBox.Show(strings.NewTimeIntervalNameExisting);
                return;
            }
            int id = GetCheckedId();
            ResultLogic = new Logic.Logic();
            if (id < 0)
                ResultLogic.LoadDefaultState();
            else
                ResultLogic.LoadStateFromArchive(id);
            
            ResultLogic.DeleteAllTransactions();
            ResultLogic.OverviewColumns.Clear();
            foreach (var item in ChkListBox_activeAccounts.CheckedItems)
                ResultLogic.DeleteAccount(item.ToString());
            foreach (var item in ChkListBox_passiveAccounts.CheckedItems)
                ResultLogic.DeleteAccount(item.ToString());


            //Generates new Legacy Transaction, which replaces StartValues, from selected Semester
            if (!CreateLegacyTransaction(ResultLogic))
            {
                MessageBox.Show(strings.ErrorCouldNotGenerateLegacyTransaction);
                return;
            }

            //TODO NEW_ERA--
            /**
             * Die Accounts müssen mit StartValue = 0 erzeugt werden
             */

            // Set startvalues to 0 and maybe planvalues to 0
            for (int i = 0; i < ResultLogic.GetAccounts.Count; i++)
                if (ResultLogic.GetAccounts[i] is UserAccount)
                    ResultLogic.ReplaceWithUserAccount(
                        ResultLogic.GetAccounts[i].Name,
                        (ResultLogic.GetAccounts[i].AccountType == AccountTypes.Passive) ? (ChkBox_planValues.Checked ? ResultLogic.GetAccounts[i].GetPlanValue() : 0) : ResultLogic.GetAccounts[i].GetValue(),
                        ResultLogic.GetAccounts[i].Name, ResultLogic.GetAccounts[i].PrintToOverview, ((UserAccount)ResultLogic.GetAccounts[i]).Email);
                else if (ResultLogic.GetAccounts[i] is ValueAccount)
                    ResultLogic.ReplaceWithValueAccount(
                        ResultLogic.GetAccounts[i].Name,
                        (ResultLogic.GetAccounts[i].AccountType == AccountTypes.Passive) ? (ChkBox_planValues.Checked ? ResultLogic.GetAccounts[i].GetPlanValue() : 0) : ResultLogic.GetAccounts[i].GetValue(),
                        ResultLogic.GetAccounts[i].Name, ResultLogic.GetAccounts[i].AccountType, ResultLogic.GetAccounts[i].PrintToOverview);

            /*
             * The LegacyTransaction has to be created again with the new accounts,
             * as the accounts were overritten here and are therefore different to the ones in the transaction
             */

            //List of all account names in LegacyTransaction
            List<string> LegacyNames = new List<string>();
            LegacyTransaction.Accounts.ForEach(a => LegacyNames.Add(a.Name));

            var accounts = new List<ValueAccount>();
            foreach (ValueAccount account in ResultLogic.GetAccounts.OfType<ValueAccount>()
                     .Where(a => LegacyNames.Contains(a.Name) && a.Name != Settings.Default.LegacyAccount))
                accounts.Add(account);

            accounts.Add((ValueAccount)ResultLogic.StrToAccount(Settings.Default.LegacyAccount));

            //dateTime without hours, seconds; it looks better as the interval start
            DateTime time = new DateTime(System.DateTime.Now.Year, System.DateTime.Now.Month, System.DateTime.Now.Day);

            LegacyTransaction = new Transaction(accounts, LegacyTransaction.Values, LegacyTransaction.Id,
                time, 0, strings.NewTimeIntervalTransactionType, strings.NewTimeIntervalTransactionComment);


            ResultLogic.AdminName = TxtBox_adminName.Text;
            ResultLogic.TimeName = TxtBox_timeName.Text;

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void TxtBox_adminName_Validating(object sender, CancelEventArgs e)
        {
            if (TxtBox_adminName.Text == "")
                e.Cancel = true;
        }

        private void TxtBox_timeName_Validating(object sender, CancelEventArgs e)
        {
            if (TxtBox_timeName.Text == "")
                e.Cancel = true;
        }

    }
}