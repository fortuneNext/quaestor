﻿using Quaestor.Logic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

/*
 Komplett unnützer Code, mit dessen Hilfe ich Visual Studio gelernt habe. Muss auch mal sein.
 Falls du diesen Code findest, kannst du ihn löschen. Oder nicht, spielt keine Rolle
 */

namespace Quaestor.GUI
{
    public partial class LearningBasics : Form
    {
        public LearningBasics()
        {
            InitializeComponent();
            LayoutPan_passive.Padding = new Padding(0, 0, SystemInformation.VerticalScrollBarWidth, 0);
        }

        int counter = 6;

        private void SettingsDialog_Load(object sender, EventArgs e)
        {
            PopulateListBoxWithFonts();
        }

        private void PopulateListBoxWithFonts()
        {
            foreach (FontFamily oneFontFamily in FontFamily.Families)
            {
                //listBox1.Font = new Font(oneFontFamily, 14);
                listBox1.Items.Add(oneFontFamily.Name);
            }
            listBox1.SelectedIndex = 0;
        }

        private void showButton_Click(object sender, EventArgs e)
        {
            //load picture to picture box
            if (openFileDialog1.ShowDialog() == DialogResult.OK) {
                pictureBox1.Load(openFileDialog1.FileName);
            }
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            //clear picture
            pictureBox1.Image = null;
        }

        private void backgroundButton_Click(object sender, EventArgs e)
        {
            //change background color
            if (colorDialog1.ShowDialog() == DialogResult.OK) {
                pictureBox1.BackColor = colorDialog1.Color;
            }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            //close the window
            //this.Close();
            //PopulateListBoxWithFonts();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            else
                pictureBox1.SizeMode = PictureBoxSizeMode.Normal;
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ComboBoxRight_Add()
        {
            // Init new Combobox
            var newComBox = new ComboBox
            {
                //Size = ComBox_right1.Size,
                Dock = DockStyle.Fill,
                //Location = new Point(ComBox_right1.Location.X, Btn_rightAdd.Location.Y),
                DropDownStyle = ComboBoxStyle.DropDownList,
                Name = "ComBox_right" + counter
            };
            foreach (object item in ComBox_left1.Items)
                newComBox.Items.Add(item);
            LayoutPan_passive.Controls.Add(newComBox, 0,counter);

            // Init new Textbox
            var newTxtBox = new TextBox
            {
                //Size = TxtBox_right1.Size,
                Dock = DockStyle.Fill,
                //Location = new Point(TxtBox_right1.Location.X, Btn_rightAdd.Location.Y),
                Text = @"0",
                Name = "TxtBox_right" + counter
            }; 
            LayoutPan_passive.Controls.Add(newTxtBox, 1,counter);
            newTxtBox.Validating += txtBox_value_Validator;
            counter++;
            
        }


        private void txtBox_value_Validator(object sender, CancelEventArgs e)
        {
            ((TextBox)sender).Text = MainForm.CleanMoneyInput(((TextBox)sender).Text);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox1.Font = new Font(listBox1.GetItemText(listBox1.SelectedItem), 14);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ComboBoxRight_Add();
        }

        private void But_Test_Click(object sender, EventArgs e)
        {
            var dialog = new DateDialog();
            dialog.ShowDialog();
            var stopWatch = new Stopwatch();
            if (dialog.DialogResult == DialogResult.OK)
            {
                listBox1.Items.Clear();
                foreach (ValueAccount account in Program.Logic.GetAccounts.OfType<ValueAccount>())
                {
                    stopWatch.Start();
                    listBox1.Items.Add(account.GetValueAtDate(dialog.time));
                    listBox1.Items.Add("  " + stopWatch.Elapsed);
                    stopWatch.Restart();
                    listBox1.Items.Add(account.GetValue(dialog.time));
                    listBox1.Items.Add("  " + stopWatch.Elapsed);
                    stopWatch.Reset();
                    listBox1.Items.Add(" ");
                }
            }
        }
    }
}
