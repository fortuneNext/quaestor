﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Quaestor.Logic;
using Quaestor.Properties;
#if DEBUG
using System.Diagnostics;

#endif

namespace Quaestor.GUI
{
    public partial class NewTransaction : Form
    {
        private readonly List<ComboBox> _leftComBoxes;
        private readonly List<TextBox> _leftTxtBoxes;
        private readonly List<ComboBox> _rightComBoxes;
        private readonly List<TextBox> _rightTxtBoxes;
        private readonly Transaction _transaction;
        public Transaction ResultTransaction; //New generated Transaction, added in MainForm

        public NewTransaction()
        {
            InitializeComponent();
            _leftComBoxes = new List<ComboBox> {ComBox_left1, ComBox_left2, ComBox_left3, ComBox_left4, ComBox_left5};
            _rightComBoxes = new List<ComboBox> {ComBox_right1, ComBox_right2, ComBox_right3, ComBox_right4, ComBox_right5};

            _leftTxtBoxes = new List<TextBox> {TxtBox_left1, TxtBox_left2, TxtBox_left3, TxtBox_left4, TxtBox_left5};
            _rightTxtBoxes = new List<TextBox> {TxtBox_right1, TxtBox_right2, TxtBox_right3, TxtBox_right4, TxtBox_right5};

            foreach (TextBox txtBox in _leftTxtBoxes)
                txtBox.Validating += txtBox_value_Validator;
            foreach (TextBox txtBox in _rightTxtBoxes)
                txtBox.Validating += txtBox_value_Validator;

            //so scrollbars are the correct way in the panels
            LayoutPan_active.Padding = new Padding(0, 0, SystemInformation.VerticalScrollBarWidth, 0);
            LayoutPan_passive.Padding = new Padding(0, 0, SystemInformation.VerticalScrollBarWidth, 0);

            //Starts new Transaction with no values preselected
            _transaction = null;
        }

        public NewTransaction(Transaction transaction): this()
        {
            _transaction = null;
        }

        private void NewTransaction_Load(object sender, EventArgs e)
        {
            // Fülle Comboboxes mit Accounts, erstes leerer String
            foreach (ComboBox comBox in _leftComBoxes) {
                comBox.Items.Add("");
            }
            foreach (ComboBox comBox in _rightComBoxes)
            {
                comBox.Items.Add("");
            }
            // Iteriere durch alle Accounts
            foreach (ValueAccount account in Program.Logic.GetAccounts.OfType<ValueAccount>())
            {
                //LegacyAccount, which replaces Start Values cannot be modified by the user
                if (account.Name == Settings.Default.LegacyAccount)
                    continue;

                // Unterscheide nach Passiv/Aktiv
                if (account.AccountType == AccountTypes.Passive)
                {
                    // Fülle jede Combobox (in Listen)
                    foreach (ComboBox comBox in _leftComBoxes)
                        comBox.Items.Add(account.Name);
                }
                else
                {
                    foreach (ComboBox comBox in _rightComBoxes)
                        comBox.Items.Add(account.Name);
                }
            }

            // Suche existente Kategorien
            TxtBox_category.DataSource = Program.Logic.GetExistingCategories();

            //if new transaction
            if (_transaction == null)
            {
                // Zeige neue Id an
                Lbl_showID.Text = Program.Logic.GetNewTransactionId().ToString(CultureInfo.InvariantCulture);
            }


            //if existing transaction, add all values into the form
            else {
                //set top values
                Lbl_showID.Text = _transaction.Id.ToString();
                TxtBox_comment.Text = _transaction.Comment;
                TxtBox_receiptNr.Text = _transaction.ReceiptNr.ToString();
                TxtBox_category.Text = _transaction.Category;
                Dtp_timeStamp.Value = _transaction.TimeStamp;

                //add passive accounts in left list, active accounts in right list
#if DEBUG
                Console.WriteLine(@"Adding Accounts to left / right list...");
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                try
                {
#endif
                    int leftIndex = 0;
                    int rightIndex = 0;
                    for (int i = 0; i < _transaction.Accounts.Count; i++)
                    {
                        // Unterscheide nach Passiv/Aktiv
                        if (_transaction.Accounts[i].AccountType == AccountTypes.Passive)
                        {
                            //add new Combobox, if more accounts than Boxes
                            if (leftIndex > _leftComBoxes.Count-1)
                            {
                                ComboBoxLeft_Add();
                            }
                            _leftComBoxes[leftIndex].Text = _transaction.Accounts[i].Name;
                            _leftTxtBoxes[leftIndex].Text = ((double)_transaction.Values[i] / 100).ToString();
                            leftIndex++;
                        }
                        else
                        {
                            //add new Combobox, if more accounts than Boxes
                            if (rightIndex > _rightComBoxes.Count-1)
                            {
                                ComboBoxRight_Add();
                            }
                            _rightComBoxes[rightIndex].Text = _transaction.Accounts[i].Name;
                            _rightTxtBoxes[rightIndex].Text = ((double)_transaction.Values[i] / 100).ToString();
                            rightIndex++;
                        }
                    }
#if DEBUG
                }
                catch {
                    Console.WriteLine(@"Couldn't add all Accounts to list");
                }
                finally
                {
                    stopWatch.Stop();
                    Console.WriteLine(@"Adding Accounts" + @" took " + stopWatch.Elapsed);
                }
#endif
            }


        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            int receiptNr;
            Int32.TryParse(TxtBox_receiptNr.Text, out receiptNr);
            var accounts = new List<ValueAccount>();
            var values = new List<int>();

            // Für jede Combox links
            for (int i = 0; i < _leftComBoxes.Count; i++)
            {
                // Falls Combox nicht leer
                if (_leftComBoxes[i].Text == "" || _leftTxtBoxes[i].Text == "0" || _leftTxtBoxes[i].Text == "0.00") continue;
                // Korrigiere Zahlen
                _leftTxtBoxes[i].Text = MainForm.CleanMoneyInput(_leftTxtBoxes[i].Text);
                // Füge Eintrag hinzu
                accounts.Add((ValueAccount) Program.Logic.StrToAccount(_leftComBoxes[i].Text));
                values.Add(Convert.ToInt32(MainForm.MoneyToInt(_leftTxtBoxes[i].Text)));
            }

            // Rechts analog
            for (int i = 0; i < _rightComBoxes.Count; i++)
            {
                if (_rightComBoxes[i].Text == "" || _rightTxtBoxes[i].Text == "0" || _rightTxtBoxes[i].Text == "0.00") continue;
                _rightTxtBoxes[i].Text = MainForm.CleanMoneyInput(_rightTxtBoxes[i].Text);
                accounts.Add((ValueAccount) Program.Logic.StrToAccount(_rightComBoxes[i].Text));
                values.Add(Convert.ToInt32(MainForm.MoneyToInt(_rightTxtBoxes[i].Text)));
            }

            // Aktualisiere Id noch einmal
            if (_transaction == null)
            {
                Lbl_showID.Text = Program.Logic.GetNewTransactionId().ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                Lbl_showID.Text = _transaction.Id.ToString();
            }
            int tempId;
            int.TryParse(Lbl_showID.Text, out tempId);
            // Prüfe Validität
            ResultTransaction = new Transaction(accounts, values, tempId,
                Dtp_timeStamp.Value, receiptNr, TxtBox_category.Text, TxtBox_comment.Text);
            if (ResultTransaction.CheckValidity())
            {
                DialogResult = DialogResult.OK;
                Close();
            }
            else
                MessageBox.Show(strings.InvalidTransaction);
        }

        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ComboBoxLeft_Add() {
            // Init new Combobox
            var newComBox = new ComboBox
            {
                Dock = DockStyle.Fill,
                DropDownStyle = ComboBoxStyle.DropDownList,
                Name = "ComBox_left" + (_leftComBoxes.Count + 1)
            };
            foreach (object item in ComBox_left1.Items)
                newComBox.Items.Add(item);
            LayoutPan_passive.Controls.Add(newComBox, 0, _leftComBoxes.Count + 1);
            _leftComBoxes.Add(newComBox);

            // Init new Textbox
            var newTxtBox = new TextBox
            {
                Dock = DockStyle.Fill,
                Text = @"0",
                Name = "TxtBox_left" + (_leftTxtBoxes.Count + 1)
            };
            LayoutPan_passive.Controls.Add(newTxtBox, 1, _leftTxtBoxes.Count + 1);
            _leftTxtBoxes.Add(newTxtBox);
            newTxtBox.Validating += txtBox_value_Validator;
        }

        private void ComboBoxRight_Add() {
            // Init new Combobox
            var newComBox = new ComboBox
            {
                Dock = DockStyle.Fill,
                DropDownStyle = ComboBoxStyle.DropDownList,
                Name = "ComBox_right" + (_rightComBoxes.Count + 1)
            };
            foreach (object item in ComBox_right1.Items)
                newComBox.Items.Add(item);
            LayoutPan_active.Controls.Add(newComBox, 0, _rightComBoxes.Count + 1);
            _rightComBoxes.Add(newComBox);

            // Init new Textbox
            var newTxtBox = new TextBox
            {
                Dock = DockStyle.Fill,
                Text = @"0",
                Name = "TxtBox_right" + (_rightTxtBoxes.Count + 1)
            };
            LayoutPan_active.Controls.Add(newTxtBox, 1, _rightTxtBoxes.Count + 1);
            _rightTxtBoxes.Add(newTxtBox);
            newTxtBox.Validating += txtBox_value_Validator;

        }

        private void Btn_leftAdd_Click(object sender, EventArgs e)
        {
            ComboBoxLeft_Add();
        }

        private void Btn_rightAdd_Click(object sender, EventArgs e)
        {
            ComboBoxRight_Add();
        }

        private void txtBox_receiptNr_Validating(object sender, CancelEventArgs e)
        {
            int receiptNr;
            if (!(Int32.TryParse(TxtBox_receiptNr.Text, out receiptNr)))
                e.Cancel = true;
        }

        private void txtBox_value_Validator(object sender, CancelEventArgs e)
        {
            ((TextBox)sender).Text = MainForm.CleanMoneyInput(((TextBox)sender).Text);
            Lbl_SumPassive.Text = _leftTxtBoxes.Sum(TxtBox => Convert.ToInt32(MainForm.MoneyToInt(TxtBox.Text))/100.0).ToString("C");
            Lbl_SumActive.Text = _rightTxtBoxes.Sum(TxtBox => Convert.ToInt32(MainForm.MoneyToInt(TxtBox.Text))/100.0).ToString("C");
        }

        private bool once = false;
        private void TxtBox_comment_TextChanged(object sender, EventArgs e)
        {
            //if (TxtBox_comment.Text.Contains(@"bierjunge"))
            if (TxtBox_comment.Text.IndexOf(@"bierjunge", StringComparison.OrdinalIgnoreCase) >= 0 && !once)
            {
                MessageBox.Show(@"Die werden also mittlerweile auch von der Activitas bezahlt");
                once = true;
            }
        }
    }
}