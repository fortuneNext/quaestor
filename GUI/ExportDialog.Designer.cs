﻿namespace Quaestor.GUI
{
    partial class ExportDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ChkListBox_StructAccounts = new System.Windows.Forms.CheckedListBox();
            this.Btn_Cancel = new System.Windows.Forms.Button();
            this.Btn_OK = new System.Windows.Forms.Button();
            this.labHeader = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // ChkListBox_StructAccounts
            // 
            this.ChkListBox_StructAccounts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ChkListBox_StructAccounts.CheckOnClick = true;
            this.ChkListBox_StructAccounts.FormattingEnabled = true;
            this.ChkListBox_StructAccounts.Location = new System.Drawing.Point(12, 41);
            this.ChkListBox_StructAccounts.Name = "ChkListBox_StructAccounts";
            this.ChkListBox_StructAccounts.Size = new System.Drawing.Size(310, 109);
            this.ChkListBox_StructAccounts.TabIndex = 0;
            this.toolTip1.SetToolTip(this.ChkListBox_StructAccounts, "Liste der Strukturkonten, von denen die Unterkonten exportiert werden sollen\r\nEin" +
        "zelne Konten zu exportieren ist nicht möglich");
            // 
            // Btn_Cancel
            // 
            this.Btn_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Cancel.CausesValidation = false;
            this.Btn_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Btn_Cancel.Location = new System.Drawing.Point(228, 156);
            this.Btn_Cancel.Name = "Btn_Cancel";
            this.Btn_Cancel.Size = new System.Drawing.Size(94, 23);
            this.Btn_Cancel.TabIndex = 4;
            this.Btn_Cancel.Text = "Abbrechen";
            this.Btn_Cancel.UseVisualStyleBackColor = true;
            // 
            // Btn_OK
            // 
            this.Btn_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Btn_OK.Location = new System.Drawing.Point(121, 156);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(101, 23);
            this.Btn_OK.TabIndex = 3;
            this.Btn_OK.Text = "OK";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_OK_Click);
            // 
            // labHeader
            // 
            this.labHeader.AutoSize = true;
            this.labHeader.Location = new System.Drawing.Point(13, 13);
            this.labHeader.Name = "labHeader";
            this.labHeader.Size = new System.Drawing.Size(203, 13);
            this.labHeader.TabIndex = 5;
            this.labHeader.Text = "Zu exportierende Nutzerkonten markieren";
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 8000;
            this.toolTip1.InitialDelay = 500;
            this.toolTip1.ReshowDelay = 100;
            // 
            // ExportDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 191);
            this.Controls.Add(this.labHeader);
            this.Controls.Add(this.Btn_Cancel);
            this.Controls.Add(this.Btn_OK);
            this.Controls.Add(this.ChkListBox_StructAccounts);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(350, 230);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(350, 220);
            this.Name = "ExportDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Export-Eigenschaften";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.ExportDialog_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox ChkListBox_StructAccounts;
        private System.Windows.Forms.Button Btn_Cancel;
        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.Label labHeader;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}