﻿using System;
using System.Windows.Forms;

namespace Quaestor.GUI
{
    public partial class DateDialog : Form
    {
        public DateTime time { get; private set; }

        public DateDialog()
        {
            InitializeComponent();
        }

        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            time = Dtp_date.Value;
            //Program.Logic.OverviewColumns.Add(Dtp_date.Value);
            Close();
        }

        private void DateDialog_Load(object sender, EventArgs e)
        {

        }
    }
}