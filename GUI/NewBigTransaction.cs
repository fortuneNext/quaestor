﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Quaestor.Logic;
using Quaestor.Properties;

namespace Quaestor.GUI
{
    public partial class NewBigTransaction : Form
    {

        private readonly List<ComboBox> _leftComBoxes;
        private readonly List<TextBox> _leftTxtBoxes;

        public NewBigTransaction()
        {
            InitializeComponent();
            _leftComBoxes = new List<ComboBox> { ComBox_left1, ComBox_left2, ComBox_left3, ComBox_left4, ComBox_left5 };

            _leftTxtBoxes = new List<TextBox> { TxtBox_left1, TxtBox_left2, TxtBox_left3, TxtBox_left4, TxtBox_left5 };

            foreach (TextBox txtBox in _leftTxtBoxes)
                txtBox.Validating += txtBox_value_Validator;

            //so scrollbars are the correct way in the panels
            LayoutPan_passive.Padding = new Padding(0, 0, SystemInformation.VerticalScrollBarWidth, 0);
        }

        private void ActualizeSum()
        {
            int sum;
            try
            {
                sum = ChkListBox_right1.CheckedItems.Count*MainForm.MoneyToInt(TxtBox_right1.Text);
            }
            catch
            {
                return;
            }

            TxtBox_left1.Text = ((double)sum / 100.0).ToString(CultureInfo.InvariantCulture);
            TxtBox_left1.Text = MainForm.CleanMoneyInput(TxtBox_left1.Text);
            //actuallize sum
            Lbl_SumPassive.Text = _leftTxtBoxes.Sum(TxtBox => Convert.ToInt32(MainForm.MoneyToInt(TxtBox.Text)) / 100.0).ToString("C");
        }

        private void NewBigTransaction_Load(object sender, EventArgs e)
        {
            // Fülle Comboboxes mit Accounts, erstes leerer String
            foreach (ComboBox comBox in _leftComBoxes)
            {
                comBox.Items.Add("");
            }
            // Iteriere durch alle Accounts
            foreach (ValueAccount account in Program.Logic.GetAccounts.OfType<ValueAccount>())
            {
                //LegacyAccount, which replaces Start Values cannot be modified by the user
                //left side, add passive accounts that are not Structural accounts
                if (account.AccountType == AccountTypes.Passive && account.Name != Settings.Default.LegacyAccount)
                {
                    // Fülle jede Combobox (in Listen)
                    foreach (ComboBox comBox in _leftComBoxes)
                        comBox.Items.Add(account.Name);
                }
            }

            //Right side, add User Accounts sorted by Structural Accounts
            //for each structural User account
            foreach (StructuralAccount account in Program.Logic.GetAccounts.OfType<StructuralAccount>().Where(account => (account.AccountType == AccountTypes.User)))
            {
                //Add headers to devide the  users by structual User Account
                ChkListBox_right1.Items.Add(@"-----" + account.Name + @"-----");
                ChkListBox_right1.SetDivider(ChkListBox_right1.Items.Count - 1, true);
                foreach (UserAccount sub in account.Subaccounts.Where(
                        sub => (sub is UserAccount)))
                    ChkListBox_right1.Items.Add(sub.Name);
            }
            Lbl_showID.Text = Program.Logic.GetNewTransactionId().ToString(CultureInfo.InvariantCulture);
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            int receiptNr;
            Int32.TryParse(TxtBox_receiptNr.Text, out receiptNr);

            var accounts = new List<ValueAccount>();
            var values = new List<int>();

            for (int i = 0; i < _leftComBoxes.Count; i++)
            {
                // Falls Combox nicht leer
                if (_leftComBoxes[i].Text == "" || _leftTxtBoxes[i].Text == "0" || _leftTxtBoxes[i].Text == "0.00") continue;
                // Korrigiere Zahlen
                _leftTxtBoxes[i].Text = MainForm.CleanMoneyInput(_leftTxtBoxes[i].Text);
                // Füge Eintrag hinzu
                accounts.Add((ValueAccount)Program.Logic.StrToAccount(_leftComBoxes[i].Text));
                values.Add(Convert.ToInt32(MainForm.MoneyToInt(_leftTxtBoxes[i].Text)));
            }

            foreach (object account in ChkListBox_right1.CheckedItems)
            {
                accounts.Add((ValueAccount) Program.Logic.StrToAccount(account.ToString()));
                values.Add(MainForm.MoneyToInt(TxtBox_right1.Text));
            }

            if (Program.Logic.NewTransaction(accounts, values, Dtp_timeStamp.Value, receiptNr, TxtBox_category.Text,
                TxtBox_comment.Text))
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
                MessageBox.Show(strings.InvalidTransaction);
        }

        private void ComboBoxLeft_Add()
        {
            // Init new Combobox
            var newComBox = new ComboBox
            {
                Dock = DockStyle.Fill,
                DropDownStyle = ComboBoxStyle.DropDownList,
                Name = "ComBox_left" + (_leftComBoxes.Count + 1)
            };
            foreach (object item in ComBox_left1.Items)
                newComBox.Items.Add(item);
            LayoutPan_passive.Controls.Add(newComBox, 0, _leftComBoxes.Count + 1);
            _leftComBoxes.Add(newComBox);

            // Init new Textbox
            var newTxtBox = new TextBox
            {
                Dock = DockStyle.Fill,
                Text = @"0",
                Name = "TxtBox_left" + (_leftTxtBoxes.Count + 1)
            };
            LayoutPan_passive.Controls.Add(newTxtBox, 1, _leftTxtBoxes.Count + 1);
            _leftTxtBoxes.Add(newTxtBox);
            newTxtBox.Validating += txtBox_value_Validator;
        }

        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void TxtBox_right1_TextChanged(object sender, EventArgs e)
        {
            ActualizeSum();
        }

        private void TxtBox_receiptNr_Validating(object sender, CancelEventArgs e)
        {
            int receiptNr;
            if (!(Int32.TryParse(TxtBox_receiptNr.Text, out receiptNr)))
                e.Cancel = true;
        }

        private void txtBox_value_Validator(object sender, CancelEventArgs e)
        {
            ((TextBox)sender).Text = MainForm.CleanMoneyInput(((TextBox)sender).Text);
            Lbl_SumPassive.Text = _leftTxtBoxes.Sum(TxtBox => Convert.ToInt32(MainForm.MoneyToInt(TxtBox.Text)) / 100.0).ToString("C");
        }

        private void Btn_LeftAdd_Click(object sender, EventArgs e)
        {
            ComboBoxLeft_Add();
        }

        private void ChkListBox_right1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            //ItemCheck is invoked before the item is checked; BeginInvoke starts Method after item is checked
            this.BeginInvoke((MethodInvoker)delegate { ActualizeSum(); });
        }
    }
}