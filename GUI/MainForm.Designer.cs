﻿using System.Windows.Forms;

namespace Quaestor.GUI
{
    partial class MainForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MenuStrip = new System.Windows.Forms.MenuStrip();
            this.verwaltungToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ladenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.öffnenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.speichernToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.speichernUnterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.semesterArchivierenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.neuesSemesterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.eMailsVersendenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportierenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transaktionenExportierenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.schuldnerlisteExportierenExelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.schuldnerlisteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.einstellungenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BeendenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bildAnzeigeprogrammToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.übersichtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.neueSpalteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kontenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.neuesKontoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vorgängeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.neuerVorgangToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aktivenbeitragToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zinsenBerechnenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hilfeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TabCtrl_main = new System.Windows.Forms.TabControl();
            this.tabPage_main = new System.Windows.Forms.TabPage();
            this.Btn_newColumn = new System.Windows.Forms.Button();
            this.Btn_overviewReset = new System.Windows.Forms.Button();
            this.NUD_overviewFontSize = new System.Windows.Forms.NumericUpDown();
            this.DataGrid_overview = new System.Windows.Forms.DataGridView();
            this.column_accounts = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_plan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Menu_ColumnContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.spalteLoeschenStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage_users = new System.Windows.Forms.TabPage();
            this.Btn_userAccountsReset = new System.Windows.Forms.Button();
            this.ChkBox_showNullUserAccounts = new System.Windows.Forms.CheckBox();
            this.NUD_userAccountsFontSize = new System.Windows.Forms.NumericUpDown();
            this.DataGrid_userAccounts = new System.Windows.Forms.DataGridView();
            this.column_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_startValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage_accounts = new System.Windows.Forms.TabPage();
            this.Pan_TopRight = new System.Windows.Forms.Panel();
            this.Grp_Filter = new System.Windows.Forms.GroupBox();
            this.ChkBox_SortByStructural = new System.Windows.Forms.CheckBox();
            this.ChkBox_FilterStructural = new System.Windows.Forms.CheckBox();
            this.ComBox_FilterValue = new System.Windows.Forms.ComboBox();
            this.TxtBox_FilterValue = new System.Windows.Forms.TextBox();
            this.Lbl_FilterValue = new System.Windows.Forms.Label();
            this.Lbl_FilterName = new System.Windows.Forms.Label();
            this.TxtBox_FilterName = new System.Windows.Forms.TextBox();
            this.ChkBox_FilterUser = new System.Windows.Forms.CheckBox();
            this.ChkBox_FilterPassive = new System.Windows.Forms.CheckBox();
            this.ChkBox_FilterActive = new System.Windows.Forms.CheckBox();
            this.Grp_AccountInfo = new System.Windows.Forms.GroupBox();
            this.Btn_editAccount = new System.Windows.Forms.Button();
            this.Btn_deleteAccount = new System.Windows.Forms.Button();
            this.Lbl_value = new System.Windows.Forms.Label();
            this.Lbl_showValue = new System.Windows.Forms.Label();
            this.Lbl_startValue = new System.Windows.Forms.Label();
            this.Lbl_showPlanValue = new System.Windows.Forms.Label();
            this.Lbl_showStartValue = new System.Windows.Forms.Label();
            this.Lbl_planValue = new System.Windows.Forms.Label();
            this.Lbl_accountType = new System.Windows.Forms.Label();
            this.Lbl_showAccountType = new System.Windows.Forms.Label();
            this.TxtBox_history = new System.Windows.Forms.RichTextBox();
            this.ListBox_accounts = new System.Windows.Forms.ListBox();
            this.tabPage_transactions = new System.Windows.Forms.TabPage();
            this.Btn_OpenTransacReceipt = new System.Windows.Forms.Button();
            this.Btn_deleteTransaction = new System.Windows.Forms.Button();
            this.Txtbox_transactionInfo = new System.Windows.Forms.RichTextBox();
            this.DataGrid_transactions = new System.Windows.Forms.DataGridView();
            this.column_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_timeStamp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_receiptNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_category = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.column_comment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolTip_MainForm = new System.Windows.Forms.ToolTip(this.components);
            this.MenuStrip.SuspendLayout();
            this.TabCtrl_main.SuspendLayout();
            this.tabPage_main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_overviewFontSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_overview)).BeginInit();
            this.Menu_ColumnContext.SuspendLayout();
            this.tabPage_users.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUD_userAccountsFontSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_userAccounts)).BeginInit();
            this.tabPage_accounts.SuspendLayout();
            this.Pan_TopRight.SuspendLayout();
            this.Grp_Filter.SuspendLayout();
            this.Grp_AccountInfo.SuspendLayout();
            this.tabPage_transactions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_transactions)).BeginInit();
            this.SuspendLayout();
            // 
            // MenuStrip
            // 
            this.MenuStrip.ImageScalingSize = new System.Drawing.Size(28, 28);
            this.MenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verwaltungToolStripMenuItem,
            this.übersichtToolStripMenuItem,
            this.kontenToolStripMenuItem,
            this.vorgängeToolStripMenuItem,
            this.hilfeToolStripMenuItem});
            this.MenuStrip.Location = new System.Drawing.Point(0, 0);
            this.MenuStrip.Name = "MenuStrip";
            this.MenuStrip.Size = new System.Drawing.Size(1139, 24);
            this.MenuStrip.TabIndex = 0;
            this.MenuStrip.Text = "menuStrip1";
            // 
            // verwaltungToolStripMenuItem
            // 
            this.verwaltungToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ladenToolStripMenuItem,
            this.öffnenToolStripMenuItem,
            this.speichernToolStripMenuItem,
            this.speichernUnterToolStripMenuItem,
            this.toolStripSeparator1,
            this.semesterArchivierenToolStripMenuItem,
            this.neuesSemesterToolStripMenuItem,
            this.toolStripSeparator2,
            this.eMailsVersendenToolStripMenuItem,
            this.exportierenToolStripMenuItem,
            this.toolStripSeparator3,
            this.einstellungenToolStripMenuItem,
            this.BeendenToolStripMenuItem,
            this.bildAnzeigeprogrammToolStripMenuItem});
            this.verwaltungToolStripMenuItem.Name = "verwaltungToolStripMenuItem";
            this.verwaltungToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.verwaltungToolStripMenuItem.Text = "Datei";
            // 
            // ladenToolStripMenuItem
            // 
            this.ladenToolStripMenuItem.Name = "ladenToolStripMenuItem";
            this.ladenToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.ladenToolStripMenuItem.Text = "Neu laden";
            this.ladenToolStripMenuItem.ToolTipText = "Den letzten gespeicherten Stand des Programms laden\r\nAlle Änderungen werden verwo" +
    "rfen";
            this.ladenToolStripMenuItem.Click += new System.EventHandler(this.LadenToolStripMenuItem_Click);
            // 
            // öffnenToolStripMenuItem
            // 
            this.öffnenToolStripMenuItem.Name = "öffnenToolStripMenuItem";
            this.öffnenToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.öffnenToolStripMenuItem.Text = "Öffnen";
            this.öffnenToolStripMenuItem.ToolTipText = "Einen Speicherstand aus dem Backup- oder History-Ordner laden";
            this.öffnenToolStripMenuItem.Click += new System.EventHandler(this.ladenAusDateiToolStripMenuItem_Click);
            // 
            // speichernToolStripMenuItem
            // 
            this.speichernToolStripMenuItem.Name = "speichernToolStripMenuItem";
            this.speichernToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.speichernToolStripMenuItem.Text = "Speichern";
            this.speichernToolStripMenuItem.ToolTipText = "Den aktuellen Programm Stand speichern";
            this.speichernToolStripMenuItem.Click += new System.EventHandler(this.SpeichernToolStripMenuItem_Click);
            // 
            // speichernUnterToolStripMenuItem
            // 
            this.speichernUnterToolStripMenuItem.Name = "speichernUnterToolStripMenuItem";
            this.speichernUnterToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.speichernUnterToolStripMenuItem.Text = "Speichern unter";
            this.speichernUnterToolStripMenuItem.ToolTipText = "Den aktuellen Programm Stand in anderem Ordner speichern";
            this.speichernUnterToolStripMenuItem.Click += new System.EventHandler(this.speichernUnterToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(195, 6);
            // 
            // semesterArchivierenToolStripMenuItem
            // 
            this.semesterArchivierenToolStripMenuItem.Name = "semesterArchivierenToolStripMenuItem";
            this.semesterArchivierenToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.semesterArchivierenToolStripMenuItem.Text = "Semester archivieren";
            this.semesterArchivierenToolStripMenuItem.ToolTipText = "Archiviert das aktuelle Semester im History-Ordner";
            this.semesterArchivierenToolStripMenuItem.Click += new System.EventHandler(this.SemesterArchivierenToolStripMenuItem_Click);
            // 
            // neuesSemesterToolStripMenuItem
            // 
            this.neuesSemesterToolStripMenuItem.Name = "neuesSemesterToolStripMenuItem";
            this.neuesSemesterToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.neuesSemesterToolStripMenuItem.Text = "Neues Semester";
            this.neuesSemesterToolStripMenuItem.ToolTipText = "Erstellt ein neues Semester und lädt es in das System";
            this.neuesSemesterToolStripMenuItem.Click += new System.EventHandler(this.neuesSemesterToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(195, 6);
            // 
            // eMailsVersendenToolStripMenuItem
            // 
            this.eMailsVersendenToolStripMenuItem.Name = "eMailsVersendenToolStripMenuItem";
            this.eMailsVersendenToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.eMailsVersendenToolStripMenuItem.Text = "E-Mails versenden";
            this.eMailsVersendenToolStripMenuItem.ToolTipText = "Sendet Emails mit den aktuellen Schuldenständen";
            this.eMailsVersendenToolStripMenuItem.Click += new System.EventHandler(this.EMailsVersendenToolStripMenuItem_Click);
            // 
            // exportierenToolStripMenuItem
            // 
            this.exportierenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.transaktionenExportierenToolStripMenuItem,
            this.schuldnerlisteExportierenExelToolStripMenuItem,
            this.schuldnerlisteToolStripMenuItem});
            this.exportierenToolStripMenuItem.Name = "exportierenToolStripMenuItem";
            this.exportierenToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.exportierenToolStripMenuItem.Text = "Exportieren";
            // 
            // transaktionenExportierenToolStripMenuItem
            // 
            this.transaktionenExportierenToolStripMenuItem.Name = "transaktionenExportierenToolStripMenuItem";
            this.transaktionenExportierenToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.transaktionenExportierenToolStripMenuItem.Text = "Transaktionen exportieren";
            this.transaktionenExportierenToolStripMenuItem.ToolTipText = "Exportiert die Transaktionen in eine Textdatei\r\nKann für die Kassenprüfung genutz" +
    "t werden";
            this.transaktionenExportierenToolStripMenuItem.Click += new System.EventHandler(this.transaktionenExportierenToolStripMenuItem_Click);
            // 
            // schuldnerlisteExportierenExelToolStripMenuItem
            // 
            this.schuldnerlisteExportierenExelToolStripMenuItem.Name = "schuldnerlisteExportierenExelToolStripMenuItem";
            this.schuldnerlisteExportierenExelToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.schuldnerlisteExportierenExelToolStripMenuItem.Text = "Gesamtschuldnerliste (Excel)";
            this.schuldnerlisteExportierenExelToolStripMenuItem.ToolTipText = "Exportiert Konten mit ihren Schulden in ein Excel Dokument,\r\nBietet eine Gesamtüb" +
    "ersicht, in die (fast) automatisch andere Kassen eingebaut werden\r\nWenn du kein " +
    "Excel hast, sucks to be you";
            this.schuldnerlisteExportierenExelToolStripMenuItem.Click += new System.EventHandler(this.schuldnerlisteExportierenExelToolStripMenuItem_Click);
            // 
            // schuldnerlisteToolStripMenuItem
            // 
            this.schuldnerlisteToolStripMenuItem.Name = "schuldnerlisteToolStripMenuItem";
            this.schuldnerlisteToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.schuldnerlisteToolStripMenuItem.Text = "Schuldnerliste (Excel / txt)";
            this.schuldnerlisteToolStripMenuItem.ToolTipText = "Exportiert Konten mit ihren Schulden in ein Excel oder Text Dokument";
            this.schuldnerlisteToolStripMenuItem.Click += new System.EventHandler(this.schuldnerlisteToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(195, 6);
            // 
            // einstellungenToolStripMenuItem
            // 
            this.einstellungenToolStripMenuItem.Name = "einstellungenToolStripMenuItem";
            this.einstellungenToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.einstellungenToolStripMenuItem.Text = "Einstellungen";
            this.einstellungenToolStripMenuItem.ToolTipText = "Einstellungen für das Exportieren von Emails";
            this.einstellungenToolStripMenuItem.Click += new System.EventHandler(this.einstellungenToolStripMenuItem_Click);
            // 
            // BeendenToolStripMenuItem
            // 
            this.BeendenToolStripMenuItem.Name = "BeendenToolStripMenuItem";
            this.BeendenToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.BeendenToolStripMenuItem.Text = "Beenden";
            this.BeendenToolStripMenuItem.Click += new System.EventHandler(this.BeendenToolStripMenuItem_Click);
            // 
            // bildAnzeigeprogrammToolStripMenuItem
            // 
            this.bildAnzeigeprogrammToolStripMenuItem.Name = "bildAnzeigeprogrammToolStripMenuItem";
            this.bildAnzeigeprogrammToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.bildAnzeigeprogrammToolStripMenuItem.Text = "Bild-Anzeigeprogramm";
            this.bildAnzeigeprogrammToolStripMenuItem.ToolTipText = "Das sollte nicht hier sein";
            this.bildAnzeigeprogrammToolStripMenuItem.Visible = false;
            this.bildAnzeigeprogrammToolStripMenuItem.Click += new System.EventHandler(this.bildAnzeigeprogrammToolStripMenuItem_Click);
            // 
            // übersichtToolStripMenuItem
            // 
            this.übersichtToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.neueSpalteToolStripMenuItem});
            this.übersichtToolStripMenuItem.Name = "übersichtToolStripMenuItem";
            this.übersichtToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.übersichtToolStripMenuItem.Text = "Übersicht";
            // 
            // neueSpalteToolStripMenuItem
            // 
            this.neueSpalteToolStripMenuItem.Name = "neueSpalteToolStripMenuItem";
            this.neueSpalteToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.neueSpalteToolStripMenuItem.Text = "Neue Spalte";
            this.neueSpalteToolStripMenuItem.ToolTipText = "Fügt der Übersicht eine neue Spalte hinzu";
            this.neueSpalteToolStripMenuItem.Click += new System.EventHandler(this.NeueSpalteToolStripMenuItem_Click);
            // 
            // kontenToolStripMenuItem
            // 
            this.kontenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.neuesKontoToolStripMenuItem});
            this.kontenToolStripMenuItem.Name = "kontenToolStripMenuItem";
            this.kontenToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.kontenToolStripMenuItem.Text = "Konten";
            // 
            // neuesKontoToolStripMenuItem
            // 
            this.neuesKontoToolStripMenuItem.Name = "neuesKontoToolStripMenuItem";
            this.neuesKontoToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.neuesKontoToolStripMenuItem.Text = "Neues Konto";
            this.neuesKontoToolStripMenuItem.Click += new System.EventHandler(this.NeuesKontoToolStripMenuItem_Click);
            // 
            // vorgängeToolStripMenuItem
            // 
            this.vorgängeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.neuerVorgangToolStripMenuItem,
            this.aktivenbeitragToolStripMenuItem,
            this.zinsenBerechnenToolStripMenuItem});
            this.vorgängeToolStripMenuItem.Name = "vorgängeToolStripMenuItem";
            this.vorgängeToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.vorgängeToolStripMenuItem.Text = "Vorgänge";
            // 
            // neuerVorgangToolStripMenuItem
            // 
            this.neuerVorgangToolStripMenuItem.Name = "neuerVorgangToolStripMenuItem";
            this.neuerVorgangToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.neuerVorgangToolStripMenuItem.Text = "Neuer Vorgang";
            this.neuerVorgangToolStripMenuItem.Click += new System.EventHandler(this.NeuerVorgangToolStripMenuItem_Click);
            // 
            // aktivenbeitragToolStripMenuItem
            // 
            this.aktivenbeitragToolStripMenuItem.Name = "aktivenbeitragToolStripMenuItem";
            this.aktivenbeitragToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.aktivenbeitragToolStripMenuItem.Text = "Aktivenbeitrag / Großvorgang";
            this.aktivenbeitragToolStripMenuItem.Click += new System.EventHandler(this.AktivenbeitragToolStripMenuItem_Click);
            // 
            // zinsenBerechnenToolStripMenuItem
            // 
            this.zinsenBerechnenToolStripMenuItem.Name = "zinsenBerechnenToolStripMenuItem";
            this.zinsenBerechnenToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.zinsenBerechnenToolStripMenuItem.Text = "Zinsen berechnen";
            this.zinsenBerechnenToolStripMenuItem.Click += new System.EventHandler(this.ZinsenBerechnenToolStripMenuItem_Click);
            // 
            // hilfeToolStripMenuItem
            // 
            this.hilfeToolStripMenuItem.Name = "hilfeToolStripMenuItem";
            this.hilfeToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.hilfeToolStripMenuItem.Text = "Hilfe";
            this.hilfeToolStripMenuItem.Click += new System.EventHandler(this.hilfeToolStripMenuItem_Click);
            // 
            // TabCtrl_main
            // 
            this.TabCtrl_main.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabCtrl_main.Controls.Add(this.tabPage_main);
            this.TabCtrl_main.Controls.Add(this.tabPage_users);
            this.TabCtrl_main.Controls.Add(this.tabPage_accounts);
            this.TabCtrl_main.Controls.Add(this.tabPage_transactions);
            this.TabCtrl_main.Location = new System.Drawing.Point(0, 27);
            this.TabCtrl_main.Name = "TabCtrl_main";
            this.TabCtrl_main.SelectedIndex = 0;
            this.TabCtrl_main.Size = new System.Drawing.Size(1139, 585);
            this.TabCtrl_main.TabIndex = 1;
            this.TabCtrl_main.SelectedIndexChanged += new System.EventHandler(this.TabCtrl_Main_SelectedIndexChanged);
            // 
            // tabPage_main
            // 
            this.tabPage_main.Controls.Add(this.Btn_newColumn);
            this.tabPage_main.Controls.Add(this.Btn_overviewReset);
            this.tabPage_main.Controls.Add(this.NUD_overviewFontSize);
            this.tabPage_main.Controls.Add(this.DataGrid_overview);
            this.tabPage_main.Location = new System.Drawing.Point(4, 22);
            this.tabPage_main.Name = "tabPage_main";
            this.tabPage_main.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_main.Size = new System.Drawing.Size(1131, 559);
            this.tabPage_main.TabIndex = 0;
            this.tabPage_main.Text = "Übersicht";
            this.tabPage_main.UseVisualStyleBackColor = true;
            // 
            // Btn_newColumn
            // 
            this.Btn_newColumn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_newColumn.Location = new System.Drawing.Point(1049, 73);
            this.Btn_newColumn.Name = "Btn_newColumn";
            this.Btn_newColumn.Size = new System.Drawing.Size(75, 23);
            this.Btn_newColumn.TabIndex = 6;
            this.Btn_newColumn.Text = "Neue Spalte";
            this.toolTip_MainForm.SetToolTip(this.Btn_newColumn, "Fügt der Übersicht eine neue Spalte hinzu");
            this.Btn_newColumn.UseVisualStyleBackColor = true;
            this.Btn_newColumn.Click += new System.EventHandler(this.Btn_newColumn_Click);
            // 
            // Btn_overviewReset
            // 
            this.Btn_overviewReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_overviewReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Btn_overviewReset.Location = new System.Drawing.Point(1044, 32);
            this.Btn_overviewReset.Name = "Btn_overviewReset";
            this.Btn_overviewReset.Size = new System.Drawing.Size(84, 35);
            this.Btn_overviewReset.TabIndex = 5;
            this.Btn_overviewReset.Text = "Ansicht zurücksetzen";
            this.toolTip_MainForm.SetToolTip(this.Btn_overviewReset, "Setzt die Übersicht auf den Standard zurück");
            this.Btn_overviewReset.UseVisualStyleBackColor = true;
            this.Btn_overviewReset.Click += new System.EventHandler(this.Btn_overviewReset_Click);
            // 
            // NUD_overviewFontSize
            // 
            this.NUD_overviewFontSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NUD_overviewFontSize.Location = new System.Drawing.Point(1060, 6);
            this.NUD_overviewFontSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NUD_overviewFontSize.Name = "NUD_overviewFontSize";
            this.NUD_overviewFontSize.Size = new System.Drawing.Size(49, 20);
            this.NUD_overviewFontSize.TabIndex = 1;
            this.toolTip_MainForm.SetToolTip(this.NUD_overviewFontSize, "Ändert die Schriftgröße");
            this.NUD_overviewFontSize.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.NUD_overviewFontSize.ValueChanged += new System.EventHandler(this.NUD_OverviewFontSize_ValueChanged);
            // 
            // DataGrid_overview
            // 
            this.DataGrid_overview.AllowUserToAddRows = false;
            this.DataGrid_overview.AllowUserToDeleteRows = false;
            this.DataGrid_overview.AllowUserToResizeRows = false;
            this.DataGrid_overview.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DataGrid_overview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGrid_overview.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.column_accounts,
            this.column_plan});
            this.DataGrid_overview.ContextMenuStrip = this.Menu_ColumnContext;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGrid_overview.DefaultCellStyle = dataGridViewCellStyle1;
            this.DataGrid_overview.Location = new System.Drawing.Point(3, 6);
            this.DataGrid_overview.Name = "DataGrid_overview";
            this.DataGrid_overview.ReadOnly = true;
            this.DataGrid_overview.RowHeadersVisible = false;
            this.DataGrid_overview.RowHeadersWidth = 72;
            this.DataGrid_overview.Size = new System.Drawing.Size(1040, 550);
            this.DataGrid_overview.TabIndex = 0;
            // 
            // column_accounts
            // 
            this.column_accounts.FillWeight = 250F;
            this.column_accounts.HeaderText = "Konten";
            this.column_accounts.MinimumWidth = 9;
            this.column_accounts.Name = "column_accounts";
            this.column_accounts.ReadOnly = true;
            this.column_accounts.Width = 250;
            // 
            // column_plan
            // 
            this.column_plan.HeaderText = "Anfangsstand/ Haushaltsplan";
            this.column_plan.MinimumWidth = 9;
            this.column_plan.Name = "column_plan";
            this.column_plan.ReadOnly = true;
            this.column_plan.Width = 175;
            // 
            // Menu_ColumnContext
            // 
            this.Menu_ColumnContext.ImageScalingSize = new System.Drawing.Size(28, 28);
            this.Menu_ColumnContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.spalteLoeschenStripMenuItem});
            this.Menu_ColumnContext.Name = "contextMenu_Columns";
            this.Menu_ColumnContext.Size = new System.Drawing.Size(119, 26);
            this.Menu_ColumnContext.Opening += new System.ComponentModel.CancelEventHandler(this.Menu_ColumnContext_Opening);
            // 
            // spalteLoeschenStripMenuItem
            // 
            this.spalteLoeschenStripMenuItem.Name = "spalteLoeschenStripMenuItem";
            this.spalteLoeschenStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.spalteLoeschenStripMenuItem.Text = "Löschen";
            this.spalteLoeschenStripMenuItem.Click += new System.EventHandler(this.deleteColumnStripMenuItem_Click);
            // 
            // tabPage_users
            // 
            this.tabPage_users.Controls.Add(this.Btn_userAccountsReset);
            this.tabPage_users.Controls.Add(this.ChkBox_showNullUserAccounts);
            this.tabPage_users.Controls.Add(this.NUD_userAccountsFontSize);
            this.tabPage_users.Controls.Add(this.DataGrid_userAccounts);
            this.tabPage_users.Location = new System.Drawing.Point(4, 22);
            this.tabPage_users.Name = "tabPage_users";
            this.tabPage_users.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_users.Size = new System.Drawing.Size(1131, 559);
            this.tabPage_users.TabIndex = 1;
            this.tabPage_users.Text = "Schuldnerliste";
            this.tabPage_users.UseVisualStyleBackColor = true;
            // 
            // Btn_userAccountsReset
            // 
            this.Btn_userAccountsReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_userAccountsReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Btn_userAccountsReset.Location = new System.Drawing.Point(1044, 32);
            this.Btn_userAccountsReset.Name = "Btn_userAccountsReset";
            this.Btn_userAccountsReset.Size = new System.Drawing.Size(84, 35);
            this.Btn_userAccountsReset.TabIndex = 4;
            this.Btn_userAccountsReset.Text = "Ansicht zurücksetzen";
            this.toolTip_MainForm.SetToolTip(this.Btn_userAccountsReset, "Setzt die Übersicht auf den Standard zurück");
            this.Btn_userAccountsReset.UseVisualStyleBackColor = true;
            this.Btn_userAccountsReset.Click += new System.EventHandler(this.Btn_userAccountsReset_Click);
            // 
            // ChkBox_showNullUserAccounts
            // 
            this.ChkBox_showNullUserAccounts.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ChkBox_showNullUserAccounts.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ChkBox_showNullUserAccounts.Location = new System.Drawing.Point(1046, 73);
            this.ChkBox_showNullUserAccounts.Name = "ChkBox_showNullUserAccounts";
            this.ChkBox_showNullUserAccounts.Size = new System.Drawing.Size(82, 57);
            this.ChkBox_showNullUserAccounts.TabIndex = 5;
            this.ChkBox_showNullUserAccounts.Text = "Ausgeglichene Schuldner anzeigen";
            this.ChkBox_showNullUserAccounts.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip_MainForm.SetToolTip(this.ChkBox_showNullUserAccounts, "Sollen Konten mit keinen Schulden / Guthaben angezeigt werden");
            this.ChkBox_showNullUserAccounts.UseVisualStyleBackColor = true;
            this.ChkBox_showNullUserAccounts.CheckedChanged += new System.EventHandler(this.ChkBox_showNullUserAccounts_CheckedChanged);
            // 
            // NUD_userAccountsFontSize
            // 
            this.NUD_userAccountsFontSize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.NUD_userAccountsFontSize.Location = new System.Drawing.Point(1060, 6);
            this.NUD_userAccountsFontSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NUD_userAccountsFontSize.Name = "NUD_userAccountsFontSize";
            this.NUD_userAccountsFontSize.Size = new System.Drawing.Size(49, 20);
            this.NUD_userAccountsFontSize.TabIndex = 2;
            this.toolTip_MainForm.SetToolTip(this.NUD_userAccountsFontSize, "Ändert die Schriftgröße");
            this.NUD_userAccountsFontSize.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.NUD_userAccountsFontSize.ValueChanged += new System.EventHandler(this.NUD_userAccounts_ValueChanged);
            // 
            // DataGrid_userAccounts
            // 
            this.DataGrid_userAccounts.AllowUserToAddRows = false;
            this.DataGrid_userAccounts.AllowUserToDeleteRows = false;
            this.DataGrid_userAccounts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DataGrid_userAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGrid_userAccounts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.column_name,
            this.column_startValue,
            this.column_value});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGrid_userAccounts.DefaultCellStyle = dataGridViewCellStyle4;
            this.DataGrid_userAccounts.Location = new System.Drawing.Point(3, 6);
            this.DataGrid_userAccounts.Name = "DataGrid_userAccounts";
            this.DataGrid_userAccounts.ReadOnly = true;
            this.DataGrid_userAccounts.RowHeadersVisible = false;
            this.DataGrid_userAccounts.RowHeadersWidth = 72;
            this.DataGrid_userAccounts.Size = new System.Drawing.Size(1040, 550);
            this.DataGrid_userAccounts.TabIndex = 0;
            // 
            // column_name
            // 
            this.column_name.FillWeight = 200F;
            this.column_name.HeaderText = "Name";
            this.column_name.MinimumWidth = 9;
            this.column_name.Name = "column_name";
            this.column_name.ReadOnly = true;
            this.column_name.Width = 200;
            // 
            // column_startValue
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.column_startValue.DefaultCellStyle = dataGridViewCellStyle2;
            this.column_startValue.HeaderText = "Semesterbeginn";
            this.column_startValue.MinimumWidth = 9;
            this.column_startValue.Name = "column_startValue";
            this.column_startValue.ReadOnly = true;
            this.column_startValue.Width = 175;
            // 
            // column_value
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.column_value.DefaultCellStyle = dataGridViewCellStyle3;
            this.column_value.HeaderText = "Gesamtkontostand";
            this.column_value.MinimumWidth = 9;
            this.column_value.Name = "column_value";
            this.column_value.ReadOnly = true;
            this.column_value.Width = 175;
            // 
            // tabPage_accounts
            // 
            this.tabPage_accounts.Controls.Add(this.Pan_TopRight);
            this.tabPage_accounts.Controls.Add(this.TxtBox_history);
            this.tabPage_accounts.Controls.Add(this.ListBox_accounts);
            this.tabPage_accounts.Location = new System.Drawing.Point(4, 22);
            this.tabPage_accounts.Name = "tabPage_accounts";
            this.tabPage_accounts.Size = new System.Drawing.Size(1131, 559);
            this.tabPage_accounts.TabIndex = 2;
            this.tabPage_accounts.Text = "Konten";
            this.tabPage_accounts.UseVisualStyleBackColor = true;
            // 
            // Pan_TopRight
            // 
            this.Pan_TopRight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Pan_TopRight.Controls.Add(this.Grp_Filter);
            this.Pan_TopRight.Controls.Add(this.Grp_AccountInfo);
            this.Pan_TopRight.Location = new System.Drawing.Point(677, 3);
            this.Pan_TopRight.Name = "Pan_TopRight";
            this.Pan_TopRight.Size = new System.Drawing.Size(446, 114);
            this.Pan_TopRight.TabIndex = 9;
            // 
            // Grp_Filter
            // 
            this.Grp_Filter.Controls.Add(this.ChkBox_SortByStructural);
            this.Grp_Filter.Controls.Add(this.ChkBox_FilterStructural);
            this.Grp_Filter.Controls.Add(this.ComBox_FilterValue);
            this.Grp_Filter.Controls.Add(this.TxtBox_FilterValue);
            this.Grp_Filter.Controls.Add(this.Lbl_FilterValue);
            this.Grp_Filter.Controls.Add(this.Lbl_FilterName);
            this.Grp_Filter.Controls.Add(this.TxtBox_FilterName);
            this.Grp_Filter.Controls.Add(this.ChkBox_FilterUser);
            this.Grp_Filter.Controls.Add(this.ChkBox_FilterPassive);
            this.Grp_Filter.Controls.Add(this.ChkBox_FilterActive);
            this.Grp_Filter.Dock = System.Windows.Forms.DockStyle.Left;
            this.Grp_Filter.Location = new System.Drawing.Point(0, 0);
            this.Grp_Filter.Name = "Grp_Filter";
            this.Grp_Filter.Size = new System.Drawing.Size(200, 114);
            this.Grp_Filter.TabIndex = 13;
            this.Grp_Filter.TabStop = false;
            this.Grp_Filter.Text = "Filter";
            this.toolTip_MainForm.SetToolTip(this.Grp_Filter, "Die Filter entfernen Konten aus der Übersicht, die nicht den Voraussetzungen ents" +
        "prechen");
            // 
            // ChkBox_SortByStructural
            // 
            this.ChkBox_SortByStructural.AutoSize = true;
            this.ChkBox_SortByStructural.Location = new System.Drawing.Point(94, 88);
            this.ChkBox_SortByStructural.Name = "ChkBox_SortByStructural";
            this.ChkBox_SortByStructural.Size = new System.Drawing.Size(90, 17);
            this.ChkBox_SortByStructural.TabIndex = 9;
            this.ChkBox_SortByStructural.Text = "Konten-Baum";
            this.toolTip_MainForm.SetToolTip(this.ChkBox_SortByStructural, "Die Konten werden in einer Baumstruktur, sortiert nach Unterkonten, angezeigt");
            this.ChkBox_SortByStructural.UseVisualStyleBackColor = true;
            this.ChkBox_SortByStructural.CheckedChanged += new System.EventHandler(this.ChkBox_SortByStructural_CheckedChanged);
            // 
            // ChkBox_FilterStructural
            // 
            this.ChkBox_FilterStructural.AutoSize = true;
            this.ChkBox_FilterStructural.Checked = true;
            this.ChkBox_FilterStructural.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChkBox_FilterStructural.Location = new System.Drawing.Point(6, 88);
            this.ChkBox_FilterStructural.Name = "ChkBox_FilterStructural";
            this.ChkBox_FilterStructural.Size = new System.Drawing.Size(63, 17);
            this.ChkBox_FilterStructural.TabIndex = 3;
            this.ChkBox_FilterStructural.Text = "Struktur";
            this.ChkBox_FilterStructural.UseVisualStyleBackColor = true;
            this.ChkBox_FilterStructural.CheckedChanged += new System.EventHandler(this.ChkBox_FilterStructural_CheckedChanged);
            // 
            // ComBox_FilterValue
            // 
            this.ComBox_FilterValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComBox_FilterValue.FormattingEnabled = true;
            this.ComBox_FilterValue.Items.AddRange(new object[] {
            ">=",
            ">",
            "=",
            "<",
            "<="});
            this.ComBox_FilterValue.Location = new System.Drawing.Point(94, 64);
            this.ComBox_FilterValue.Name = "ComBox_FilterValue";
            this.ComBox_FilterValue.Size = new System.Drawing.Size(39, 21);
            this.ComBox_FilterValue.TabIndex = 6;
            this.ComBox_FilterValue.SelectedIndexChanged += new System.EventHandler(this.ComBox_FilterValue_SelectedIndexChanged);
            // 
            // TxtBox_FilterValue
            // 
            this.TxtBox_FilterValue.Location = new System.Drawing.Point(139, 65);
            this.TxtBox_FilterValue.Name = "TxtBox_FilterValue";
            this.TxtBox_FilterValue.Size = new System.Drawing.Size(55, 20);
            this.TxtBox_FilterValue.TabIndex = 7;
            this.TxtBox_FilterValue.Validating += new System.ComponentModel.CancelEventHandler(this.TxtBox_FilterValue_Validating);
            // 
            // Lbl_FilterValue
            // 
            this.Lbl_FilterValue.AutoSize = true;
            this.Lbl_FilterValue.Location = new System.Drawing.Point(91, 48);
            this.Lbl_FilterValue.Name = "Lbl_FilterValue";
            this.Lbl_FilterValue.Size = new System.Drawing.Size(30, 13);
            this.Lbl_FilterValue.TabIndex = 5;
            this.Lbl_FilterValue.Text = "Wert";
            // 
            // Lbl_FilterName
            // 
            this.Lbl_FilterName.AutoSize = true;
            this.Lbl_FilterName.Location = new System.Drawing.Point(91, 9);
            this.Lbl_FilterName.Name = "Lbl_FilterName";
            this.Lbl_FilterName.Size = new System.Drawing.Size(35, 13);
            this.Lbl_FilterName.TabIndex = 3;
            this.Lbl_FilterName.Text = "Name";
            // 
            // TxtBox_FilterName
            // 
            this.TxtBox_FilterName.Location = new System.Drawing.Point(94, 25);
            this.TxtBox_FilterName.Name = "TxtBox_FilterName";
            this.TxtBox_FilterName.Size = new System.Drawing.Size(100, 20);
            this.TxtBox_FilterName.TabIndex = 4;
            this.TxtBox_FilterName.TextChanged += new System.EventHandler(this.TxtBox_FilterName_TextChanged);
            // 
            // ChkBox_FilterUser
            // 
            this.ChkBox_FilterUser.AutoSize = true;
            this.ChkBox_FilterUser.Checked = true;
            this.ChkBox_FilterUser.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChkBox_FilterUser.Location = new System.Drawing.Point(6, 65);
            this.ChkBox_FilterUser.Name = "ChkBox_FilterUser";
            this.ChkBox_FilterUser.Size = new System.Drawing.Size(68, 17);
            this.ChkBox_FilterUser.TabIndex = 2;
            this.ChkBox_FilterUser.Text = "Benutzer";
            this.ChkBox_FilterUser.UseVisualStyleBackColor = true;
            this.ChkBox_FilterUser.CheckedChanged += new System.EventHandler(this.ChkBox_FilterUser_CheckedChanged);
            // 
            // ChkBox_FilterPassive
            // 
            this.ChkBox_FilterPassive.AutoSize = true;
            this.ChkBox_FilterPassive.Checked = true;
            this.ChkBox_FilterPassive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChkBox_FilterPassive.Location = new System.Drawing.Point(6, 42);
            this.ChkBox_FilterPassive.Name = "ChkBox_FilterPassive";
            this.ChkBox_FilterPassive.Size = new System.Drawing.Size(57, 17);
            this.ChkBox_FilterPassive.TabIndex = 1;
            this.ChkBox_FilterPassive.Text = "Passiv";
            this.ChkBox_FilterPassive.UseVisualStyleBackColor = true;
            this.ChkBox_FilterPassive.CheckedChanged += new System.EventHandler(this.ChkBox_FilterPassive_CheckedChanged);
            // 
            // ChkBox_FilterActive
            // 
            this.ChkBox_FilterActive.AutoSize = true;
            this.ChkBox_FilterActive.Checked = true;
            this.ChkBox_FilterActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ChkBox_FilterActive.Location = new System.Drawing.Point(6, 19);
            this.ChkBox_FilterActive.Name = "ChkBox_FilterActive";
            this.ChkBox_FilterActive.Size = new System.Drawing.Size(50, 17);
            this.ChkBox_FilterActive.TabIndex = 0;
            this.ChkBox_FilterActive.Text = "Aktiv";
            this.ChkBox_FilterActive.UseVisualStyleBackColor = true;
            this.ChkBox_FilterActive.CheckedChanged += new System.EventHandler(this.ChkBox_FilterActive_CheckedChanged);
            // 
            // Grp_AccountInfo
            // 
            this.Grp_AccountInfo.Controls.Add(this.Btn_editAccount);
            this.Grp_AccountInfo.Controls.Add(this.Btn_deleteAccount);
            this.Grp_AccountInfo.Controls.Add(this.Lbl_value);
            this.Grp_AccountInfo.Controls.Add(this.Lbl_showValue);
            this.Grp_AccountInfo.Controls.Add(this.Lbl_startValue);
            this.Grp_AccountInfo.Controls.Add(this.Lbl_showPlanValue);
            this.Grp_AccountInfo.Controls.Add(this.Lbl_showStartValue);
            this.Grp_AccountInfo.Controls.Add(this.Lbl_planValue);
            this.Grp_AccountInfo.Controls.Add(this.Lbl_accountType);
            this.Grp_AccountInfo.Controls.Add(this.Lbl_showAccountType);
            this.Grp_AccountInfo.Dock = System.Windows.Forms.DockStyle.Right;
            this.Grp_AccountInfo.Location = new System.Drawing.Point(206, 0);
            this.Grp_AccountInfo.Name = "Grp_AccountInfo";
            this.Grp_AccountInfo.Size = new System.Drawing.Size(240, 114);
            this.Grp_AccountInfo.TabIndex = 12;
            this.Grp_AccountInfo.TabStop = false;
            this.Grp_AccountInfo.Text = "Kontoinformationen";
            // 
            // Btn_editAccount
            // 
            this.Btn_editAccount.Location = new System.Drawing.Point(8, 82);
            this.Btn_editAccount.Name = "Btn_editAccount";
            this.Btn_editAccount.Size = new System.Drawing.Size(110, 23);
            this.Btn_editAccount.TabIndex = 9;
            this.Btn_editAccount.Text = "Bearbeiten";
            this.Btn_editAccount.UseVisualStyleBackColor = true;
            this.Btn_editAccount.Click += new System.EventHandler(this.Btn_editAccount_Click);
            // 
            // Btn_deleteAccount
            // 
            this.Btn_deleteAccount.Location = new System.Drawing.Point(124, 82);
            this.Btn_deleteAccount.Name = "Btn_deleteAccount";
            this.Btn_deleteAccount.Size = new System.Drawing.Size(110, 23);
            this.Btn_deleteAccount.TabIndex = 11;
            this.Btn_deleteAccount.Text = "Löschen";
            this.Btn_deleteAccount.UseVisualStyleBackColor = true;
            this.Btn_deleteAccount.Click += new System.EventHandler(this.Btn_deleteAccount_Click);
            // 
            // Lbl_value
            // 
            this.Lbl_value.AutoSize = true;
            this.Lbl_value.Location = new System.Drawing.Point(3, 19);
            this.Lbl_value.Name = "Lbl_value";
            this.Lbl_value.Size = new System.Drawing.Size(30, 13);
            this.Lbl_value.TabIndex = 1;
            this.Lbl_value.Text = "Wert";
            // 
            // Lbl_showValue
            // 
            this.Lbl_showValue.AutoSize = true;
            this.Lbl_showValue.Location = new System.Drawing.Point(85, 19);
            this.Lbl_showValue.Name = "Lbl_showValue";
            this.Lbl_showValue.Size = new System.Drawing.Size(13, 13);
            this.Lbl_showValue.TabIndex = 2;
            this.Lbl_showValue.Text = "0";
            // 
            // Lbl_startValue
            // 
            this.Lbl_startValue.AutoSize = true;
            this.Lbl_startValue.Location = new System.Drawing.Point(3, 32);
            this.Lbl_startValue.Name = "Lbl_startValue";
            this.Lbl_startValue.Size = new System.Drawing.Size(49, 13);
            this.Lbl_startValue.TabIndex = 3;
            this.Lbl_startValue.Text = "Startwert";
            // 
            // Lbl_showPlanValue
            // 
            this.Lbl_showPlanValue.AutoSize = true;
            this.Lbl_showPlanValue.Location = new System.Drawing.Point(85, 45);
            this.Lbl_showPlanValue.Name = "Lbl_showPlanValue";
            this.Lbl_showPlanValue.Size = new System.Drawing.Size(13, 13);
            this.Lbl_showPlanValue.TabIndex = 8;
            this.Lbl_showPlanValue.Text = "0";
            // 
            // Lbl_showStartValue
            // 
            this.Lbl_showStartValue.AutoSize = true;
            this.Lbl_showStartValue.Location = new System.Drawing.Point(85, 32);
            this.Lbl_showStartValue.Name = "Lbl_showStartValue";
            this.Lbl_showStartValue.Size = new System.Drawing.Size(13, 13);
            this.Lbl_showStartValue.TabIndex = 4;
            this.Lbl_showStartValue.Text = "0";
            // 
            // Lbl_planValue
            // 
            this.Lbl_planValue.AutoSize = true;
            this.Lbl_planValue.Location = new System.Drawing.Point(3, 45);
            this.Lbl_planValue.Name = "Lbl_planValue";
            this.Lbl_planValue.Size = new System.Drawing.Size(71, 13);
            this.Lbl_planValue.TabIndex = 7;
            this.Lbl_planValue.Text = "Planungswert";
            // 
            // Lbl_accountType
            // 
            this.Lbl_accountType.AutoSize = true;
            this.Lbl_accountType.Location = new System.Drawing.Point(3, 58);
            this.Lbl_accountType.Name = "Lbl_accountType";
            this.Lbl_accountType.Size = new System.Drawing.Size(49, 13);
            this.Lbl_accountType.TabIndex = 5;
            this.Lbl_accountType.Text = "Kontotyp";
            // 
            // Lbl_showAccountType
            // 
            this.Lbl_showAccountType.AutoSize = true;
            this.Lbl_showAccountType.Location = new System.Drawing.Point(85, 58);
            this.Lbl_showAccountType.Name = "Lbl_showAccountType";
            this.Lbl_showAccountType.Size = new System.Drawing.Size(31, 13);
            this.Lbl_showAccountType.TabIndex = 6;
            this.Lbl_showAccountType.Text = "Aktiv";
            // 
            // TxtBox_history
            // 
            this.TxtBox_history.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtBox_history.Location = new System.Drawing.Point(677, 123);
            this.TxtBox_history.Name = "TxtBox_history";
            this.TxtBox_history.ReadOnly = true;
            this.TxtBox_history.Size = new System.Drawing.Size(446, 424);
            this.TxtBox_history.TabIndex = 10;
            this.TxtBox_history.TabStop = false;
            this.TxtBox_history.Text = "";
            // 
            // ListBox_accounts
            // 
            this.ListBox_accounts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ListBox_accounts.FormattingEnabled = true;
            this.ListBox_accounts.Location = new System.Drawing.Point(0, 10);
            this.ListBox_accounts.MinimumSize = new System.Drawing.Size(120, 4);
            this.ListBox_accounts.Name = "ListBox_accounts";
            this.ListBox_accounts.Size = new System.Drawing.Size(671, 537);
            this.ListBox_accounts.TabIndex = 0;
            this.ListBox_accounts.SelectedIndexChanged += new System.EventHandler(this.ListBox_accounts_SelectedIndexChanged);
            this.ListBox_accounts.DoubleClick += new System.EventHandler(this.ListBox_accounts_DoubleClick);
            this.ListBox_accounts.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ListBox_accounts_KeyDown);
            // 
            // tabPage_transactions
            // 
            this.tabPage_transactions.Controls.Add(this.Btn_OpenTransacReceipt);
            this.tabPage_transactions.Controls.Add(this.Btn_deleteTransaction);
            this.tabPage_transactions.Controls.Add(this.Txtbox_transactionInfo);
            this.tabPage_transactions.Controls.Add(this.DataGrid_transactions);
            this.tabPage_transactions.Location = new System.Drawing.Point(4, 22);
            this.tabPage_transactions.Name = "tabPage_transactions";
            this.tabPage_transactions.Size = new System.Drawing.Size(1131, 559);
            this.tabPage_transactions.TabIndex = 3;
            this.tabPage_transactions.Text = "Vorgänge";
            this.tabPage_transactions.UseVisualStyleBackColor = true;
            // 
            // Btn_OpenTransacReceipt
            // 
            this.Btn_OpenTransacReceipt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_OpenTransacReceipt.Location = new System.Drawing.Point(1018, 6);
            this.Btn_OpenTransacReceipt.Name = "Btn_OpenTransacReceipt";
            this.Btn_OpenTransacReceipt.Size = new System.Drawing.Size(75, 23);
            this.Btn_OpenTransacReceipt.TabIndex = 14;
            this.Btn_OpenTransacReceipt.Text = "Beleg öffnen";
            this.toolTip_MainForm.SetToolTip(this.Btn_OpenTransacReceipt, "Öffnet den Beleg einer Transaktion, wenn einer existiert");
            this.Btn_OpenTransacReceipt.UseVisualStyleBackColor = true;
            this.Btn_OpenTransacReceipt.Click += new System.EventHandler(this.Btn_OpenTransacReceipt_Click);
            // 
            // Btn_deleteTransaction
            // 
            this.Btn_deleteTransaction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_deleteTransaction.Location = new System.Drawing.Point(788, 6);
            this.Btn_deleteTransaction.Name = "Btn_deleteTransaction";
            this.Btn_deleteTransaction.Size = new System.Drawing.Size(79, 24);
            this.Btn_deleteTransaction.TabIndex = 12;
            this.Btn_deleteTransaction.Text = "Löschen";
            this.Btn_deleteTransaction.UseVisualStyleBackColor = true;
            this.Btn_deleteTransaction.Click += new System.EventHandler(this.Btn_deleteTransaction_Click);
            // 
            // Txtbox_transactionInfo
            // 
            this.Txtbox_transactionInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Txtbox_transactionInfo.Location = new System.Drawing.Point(788, 33);
            this.Txtbox_transactionInfo.MinimumSize = new System.Drawing.Size(220, 4);
            this.Txtbox_transactionInfo.Name = "Txtbox_transactionInfo";
            this.Txtbox_transactionInfo.ReadOnly = true;
            this.Txtbox_transactionInfo.Size = new System.Drawing.Size(342, 528);
            this.Txtbox_transactionInfo.TabIndex = 11;
            this.Txtbox_transactionInfo.TabStop = false;
            this.Txtbox_transactionInfo.Text = "";
            // 
            // DataGrid_transactions
            // 
            this.DataGrid_transactions.AllowUserToAddRows = false;
            this.DataGrid_transactions.AllowUserToDeleteRows = false;
            this.DataGrid_transactions.AllowUserToResizeRows = false;
            this.DataGrid_transactions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DataGrid_transactions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGrid_transactions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.column_ID,
            this.column_timeStamp,
            this.column_receiptNr,
            this.column_category,
            this.column_comment});
            this.DataGrid_transactions.Location = new System.Drawing.Point(3, 6);
            this.DataGrid_transactions.MultiSelect = false;
            this.DataGrid_transactions.Name = "DataGrid_transactions";
            this.DataGrid_transactions.ReadOnly = true;
            this.DataGrid_transactions.RowHeadersVisible = false;
            this.DataGrid_transactions.RowHeadersWidth = 72;
            this.DataGrid_transactions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGrid_transactions.Size = new System.Drawing.Size(783, 556);
            this.DataGrid_transactions.StandardTab = true;
            this.DataGrid_transactions.TabIndex = 0;
            this.DataGrid_transactions.SelectionChanged += new System.EventHandler(this.DataGrid_transactions_SelectionChanged);
            // 
            // column_ID
            // 
            this.column_ID.HeaderText = "Id";
            this.column_ID.MinimumWidth = 9;
            this.column_ID.Name = "column_ID";
            this.column_ID.ReadOnly = true;
            this.column_ID.Width = 40;
            // 
            // column_timeStamp
            // 
            this.column_timeStamp.HeaderText = "Zeit";
            this.column_timeStamp.MinimumWidth = 9;
            this.column_timeStamp.Name = "column_timeStamp";
            this.column_timeStamp.ReadOnly = true;
            this.column_timeStamp.Width = 140;
            // 
            // column_receiptNr
            // 
            this.column_receiptNr.HeaderText = "Beleg Nr.";
            this.column_receiptNr.MinimumWidth = 9;
            this.column_receiptNr.Name = "column_receiptNr";
            this.column_receiptNr.ReadOnly = true;
            this.column_receiptNr.Width = 75;
            // 
            // column_category
            // 
            this.column_category.HeaderText = "Kategorie";
            this.column_category.MinimumWidth = 9;
            this.column_category.Name = "column_category";
            this.column_category.ReadOnly = true;
            this.column_category.Width = 125;
            // 
            // column_comment
            // 
            this.column_comment.HeaderText = "Kommentar";
            this.column_comment.MinimumWidth = 9;
            this.column_comment.Name = "column_comment";
            this.column_comment.ReadOnly = true;
            this.column_comment.Width = 400;
            // 
            // toolTip_MainForm
            // 
            this.toolTip_MainForm.AutoPopDelay = 8000;
            this.toolTip_MainForm.InitialDelay = 500;
            this.toolTip_MainForm.ReshowDelay = 100;
            this.toolTip_MainForm.ShowAlways = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1139, 611);
            this.Controls.Add(this.TabCtrl_main);
            this.Controls.Add(this.MenuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.MenuStrip;
            this.MinimumSize = new System.Drawing.Size(610, 95);
            this.Name = "MainForm";
            this.Text = "Aktivenkasse";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.MenuStrip.ResumeLayout(false);
            this.MenuStrip.PerformLayout();
            this.TabCtrl_main.ResumeLayout(false);
            this.tabPage_main.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NUD_overviewFontSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_overview)).EndInit();
            this.Menu_ColumnContext.ResumeLayout(false);
            this.tabPage_users.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NUD_userAccountsFontSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_userAccounts)).EndInit();
            this.tabPage_accounts.ResumeLayout(false);
            this.Pan_TopRight.ResumeLayout(false);
            this.Grp_Filter.ResumeLayout(false);
            this.Grp_Filter.PerformLayout();
            this.Grp_AccountInfo.ResumeLayout(false);
            this.Grp_AccountInfo.PerformLayout();
            this.tabPage_transactions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_transactions)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MenuStrip;
        private System.Windows.Forms.ToolStripMenuItem vorgängeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem neuerVorgangToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kontenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem neuesKontoToolStripMenuItem;
        private System.Windows.Forms.TabControl TabCtrl_main;
        private System.Windows.Forms.TabPage tabPage_main;
        private System.Windows.Forms.TabPage tabPage_users;
        private System.Windows.Forms.TabPage tabPage_accounts;
        private System.Windows.Forms.TabPage tabPage_transactions;
        private System.Windows.Forms.DataGridView DataGrid_transactions;
        private System.Windows.Forms.ListBox ListBox_accounts;
        private System.Windows.Forms.Label Lbl_value;
        private System.Windows.Forms.Label Lbl_showValue;
        private System.Windows.Forms.ToolStripMenuItem verwaltungToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem speichernToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ladenToolStripMenuItem;
        private System.Windows.Forms.DataGridView DataGrid_overview;
        private System.Windows.Forms.ToolStripMenuItem übersichtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem neueSpalteToolStripMenuItem;
        private System.Windows.Forms.DataGridView DataGrid_userAccounts;
        private System.Windows.Forms.Label Lbl_showStartValue;
        private System.Windows.Forms.Label Lbl_startValue;
        private System.Windows.Forms.Label Lbl_accountType;
        private System.Windows.Forms.Label Lbl_showAccountType;
        private System.Windows.Forms.Label Lbl_showPlanValue;
        private System.Windows.Forms.Label Lbl_planValue;
        private System.Windows.Forms.Button Btn_editAccount;
        private System.Windows.Forms.ToolStripMenuItem aktivenbeitragToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zinsenBerechnenToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_startValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn column_value;
        private System.Windows.Forms.RichTextBox TxtBox_history;
        private System.Windows.Forms.ToolStripMenuItem eMailsVersendenToolStripMenuItem;
        private System.Windows.Forms.RichTextBox Txtbox_transactionInfo;
        private System.Windows.Forms.Button Btn_deleteTransaction;
        private System.Windows.Forms.ToolStripMenuItem semesterArchivierenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem neuesSemesterToolStripMenuItem;
        private System.Windows.Forms.Button Btn_deleteAccount;
        private System.Windows.Forms.ToolStripMenuItem exportierenToolStripMenuItem;
        private System.Windows.Forms.NumericUpDown NUD_overviewFontSize;
        private System.Windows.Forms.NumericUpDown NUD_userAccountsFontSize;
        private System.Windows.Forms.ContextMenuStrip Menu_ColumnContext;
        private System.Windows.Forms.ToolStripMenuItem spalteLoeschenStripMenuItem;
        private System.Windows.Forms.CheckBox ChkBox_showNullUserAccounts;
        private System.Windows.Forms.GroupBox Grp_AccountInfo;
        private System.Windows.Forms.GroupBox Grp_Filter;
        private System.Windows.Forms.CheckBox ChkBox_FilterActive;
        private System.Windows.Forms.CheckBox ChkBox_FilterPassive;
        private System.Windows.Forms.CheckBox ChkBox_FilterUser;
        private System.Windows.Forms.TextBox TxtBox_FilterName;
        private System.Windows.Forms.Label Lbl_FilterName;
        private System.Windows.Forms.Label Lbl_FilterValue;
        private System.Windows.Forms.TextBox TxtBox_FilterValue;
        private System.Windows.Forms.ComboBox ComBox_FilterValue;
        private System.Windows.Forms.CheckBox ChkBox_FilterStructural;
        private System.Windows.Forms.ToolStripMenuItem einstellungenToolStripMenuItem;
        private ToolTip toolTip_MainForm;
        private Panel Pan_TopRight;
        private ToolStripMenuItem bildAnzeigeprogrammToolStripMenuItem;
        private DataGridViewTextBoxColumn column_ID;
        private DataGridViewTextBoxColumn column_timeStamp;
        private DataGridViewTextBoxColumn column_receiptNr;
        private DataGridViewTextBoxColumn column_category;
        private DataGridViewTextBoxColumn column_comment;
        private Button Btn_overviewReset;
        private Button Btn_userAccountsReset;
        private DataGridViewTextBoxColumn column_accounts;
        private DataGridViewTextBoxColumn column_plan;
        private Button Btn_OpenTransacReceipt;
        private CheckBox ChkBox_SortByStructural;
        private ToolStripMenuItem hilfeToolStripMenuItem;
        private ToolStripMenuItem transaktionenExportierenToolStripMenuItem;
        private ToolStripMenuItem schuldnerlisteExportierenExelToolStripMenuItem;
        private ToolStripMenuItem öffnenToolStripMenuItem;
        private ToolStripMenuItem speichernUnterToolStripMenuItem;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripMenuItem BeendenToolStripMenuItem;
        private ToolStripMenuItem schuldnerlisteToolStripMenuItem;
        private Button Btn_newColumn;
    }
}

