﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Quaestor.Logic;

namespace Quaestor.GUI
{
    public partial class NewAccount : Form
    {
        //public Account CreatedAccount;

        public NewAccount()
        {
            InitializeComponent();
        }

        private void RefreshEnabled()
        {
            TxtBox_email.Enabled = (Rbtn_user.Checked && !ChkBox_structuralAccount.Checked);
            Lbl_email.Enabled = TxtBox_email.Enabled;
            TxtBox_planValue.Enabled = !ChkBox_structuralAccount.Checked;
            Lbl_planValue.Enabled = !ChkBox_structuralAccount.Checked;
            ChkListBox_subaccounts.Enabled = ChkBox_structuralAccount.Checked;
            Lbl_subaccounts.Enabled = ChkListBox_subaccounts.Enabled;
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            if (ChkBox_structuralAccount.Checked)
            {
                List<Account> accounts =
                    (from object account in ChkListBox_subaccounts.CheckedItems
                        select Program.Logic.StrToAccount(account.ToString())).ToList();
                if (Rbtn_active.Checked)
                {
                    if (
                        !Program.Logic.NewStructuralAccount(TxtBox_name.Text.Trim(), accounts, AccountTypes.Active,
                            Chkbox_printToOverview.Checked))
                    {
                        MessageBox.Show(strings.NameAlreadyInUse);
                        return;
                    }
                }
                else if (Rbtn_passive.Checked)
                {
                    if (
                        !Program.Logic.NewStructuralAccount(TxtBox_name.Text.Trim(), accounts, AccountTypes.Passive,
                            Chkbox_printToOverview.Checked))
                    {
                        MessageBox.Show(strings.NameAlreadyInUse);
                        return;
                    }
                }
                else if (Rbtn_user.Checked)
                {
                    if (
                        !Program.Logic.NewStructuralAccount(TxtBox_name.Text.Trim(), accounts, AccountTypes.User,
                            Chkbox_printToOverview.Checked))
                    {
                        MessageBox.Show(strings.NameAlreadyInUse);
                        return;
                    }
                }
            }
            else
            {
                if (Rbtn_active.Checked)
                {
                    if (
                        !Program.Logic.NewValueAccount(0,
                            MainForm.MoneyToInt(TxtBox_planValue.Text), TxtBox_name.Text.Trim(), AccountTypes.Active,
                            Chkbox_printToOverview.Checked))
                    {
                        MessageBox.Show(strings.NameAlreadyInUse);
                        return;
                    }
                }
                else if (Rbtn_passive.Checked)
                {
                    if (
                        !Program.Logic.NewValueAccount(0,
                            MainForm.MoneyToInt(TxtBox_planValue.Text), TxtBox_name.Text.Trim(), AccountTypes.Passive,
                            Chkbox_printToOverview.Checked))
                    {
                        MessageBox.Show(strings.NameAlreadyInUse);
                        return;
                    }
                }
                else if (Rbtn_user.Checked)
                {
                    if (
                        !Program.Logic.NewUserAccount(0,
                            MainForm.MoneyToInt(TxtBox_planValue.Text), TxtBox_name.Text.Trim(), Chkbox_printToOverview.Checked,
                            TxtBox_email.Text))
                    {
                        MessageBox.Show(strings.NameAlreadyInUse);
                        return;
                    }
                }
            }
            foreach (object account in ChkListBox_subaccountTo.CheckedItems)
                ((StructuralAccount) Program.Logic.StrToAccount(account.ToString())).AddAccount(
                    Program.Logic.StrToAccount(TxtBox_name.Text));

            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void NewAccount_Load(object sender, EventArgs e)
        {
            Rbtn_active.Checked = true;
            foreach (Account account in Program.Logic.GetAccounts)
            {
                ChkListBox_subaccounts.Items.Add(account.Name);
                if (account is StructuralAccount)
                    ChkListBox_subaccountTo.Items.Add(account.Name);
            }
            RefreshEnabled();
        }

        private void ChkBox_structuralAccount_CheckedChanged(object sender, EventArgs e)
        {
            RefreshEnabled();
        }

        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Rbtn_user_CheckedChanged(object sender, EventArgs e)
        {
            RefreshEnabled();
        }

        private void TxtBox_email_Validating(object sender, CancelEventArgs e)
        {
            if (!(Logic.Logic.IsValidEmail(TxtBox_email.Text) || TxtBox_email.Text == ""))
                e.Cancel = true;
        }

        private void TxtBox_planValue_Validating(object sender, CancelEventArgs e)
        {
            TxtBox_planValue.Text = MainForm.CleanMoneyInput(TxtBox_planValue.Text);
        }

        private void TxtBox_name_TextChanged(object sender, EventArgs e)
        {
            if (TxtBox_name.Text.Equals(@"phritte"))
                MessageBox.Show(@"Selber Phritte");
        }
    }
}