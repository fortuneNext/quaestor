﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;//used to write exel document
using Excel = Microsoft.Office.Interop.Excel;//used to write exel document
using Quaestor.Properties;
using Microsoft.Office.Interop.Excel;
#if DEBUG
using System.Diagnostics; 
#endif
using Quaestor.Logic;

namespace Quaestor.GUI
{
    public partial class MainForm : Form
    {
        private DataGridView.HitTestInfo ColumnContextMenuCell { get; set; }

        public MainForm()
        {
            InitializeComponent();
            DataGrid_userAccounts.SortCompare += MoneySortCompare;
            ComBox_FilterValue.SelectedIndex = 0;

        }

        private void RefreshAll()
        {
#if DEBUG
            Console.WriteLine(@"Calling " + MethodBase.GetCurrentMethod().Name + @"...");
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            try
            {
#endif
            RefreshOverview();
            RefreshUserAccounts();
            RefreshAccounts();
            RefreshTransactions();
            Version version = Assembly.GetExecutingAssembly().GetName().Version;
            Text = @"Quaestor " + version + @" - " + Program.Logic.AdminName + @" - " + Program.Logic.TimeName;
#if DEBUG
            }
            finally
            {
                stopWatch.Stop();
                Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" took " + stopWatch.Elapsed);
            }
#endif
        }

        private void RefreshOverview()
        {
#if DEBUG
            Console.WriteLine(@"Calling " + MethodBase.GetCurrentMethod().Name + @"...");
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            try
            {
#endif
            DataGrid_overview.Rows.Clear();
            while (DataGrid_overview.Columns.Count > 2)
                DataGrid_overview.Columns.Remove(DataGrid_overview.Columns[2]);
            DataGrid_overview.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", Convert.ToSingle(NUD_overviewFontSize.Value), GraphicsUnit.Point);
            Program.Logic.OverviewColumns.Sort();
            foreach (DateTime date in Program.Logic.OverviewColumns)
            {
                DataGrid_overview.Columns.Add(@"column_" + date.ToString(CultureInfo.InvariantCulture), date.ToString(Thread.CurrentThread.CurrentCulture));
            }
            //Last collumn shows difference to Haushaltspalan
            DataGrid_overview.Columns.Add(@"column_Difference", @"Differenz zu Haushaltsplan");
            DataGridViewColumn lastColumn = DataGrid_overview.Columns[DataGrid_overview.Columns.Count - 1];
            lastColumn.DefaultCellStyle.Font = new System.Drawing.Font("Microsofr Sans Serif", Convert.ToSingle(NUD_overviewFontSize.Value), FontStyle.Italic);
            lastColumn.DefaultCellStyle.BackColor = Color.LightGray;
            //lastColumn.DividerWidth += 10;

            DataGrid_overview.Rows.Add("Aktiva");
            foreach (
                Account account in
                    Program.Logic.GetAccounts.Where(
                        account => account.PrintToOverview && account.AccountType == AccountTypes.Active))
                PrintAccount(account, 0);
            DataGrid_overview.Rows.Add(string.Empty);
            DataGrid_overview.Rows.Add("Passiva");
            foreach (
                var account in
                    Program.Logic.GetAccounts.Where(
                        account => account.PrintToOverview && account.AccountType == AccountTypes.Passive))
                PrintAccount(account, 0);
            DataGrid_overview.AutoResizeColumns();
            DataGrid_overview.AutoResizeRows();
#if DEBUG
            }
            finally
            {
                stopWatch.Stop();
                Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" took " + stopWatch.Elapsed);
            }
#endif
        }

        private void PrintAccount(Account account, int order)
        {
            List<DateTime> columns = Program.Logic.OverviewColumns;
            if (account.AccountType == AccountTypes.User)
                return;
            // Space Level
            string res = "";
            for (int i = 0; i < order; i++)
                res += "   ";
            // Row init; +2 for "Konten"/ "Haushaltsplan", +1 for "Differenz"
            var row = new object[columns.Count + 2 + 1];
            row[0] = res + account.Name;
            row[1] = (((double)account.GetPlanValue()) / 100).ToString("C");
            // For every column get value
            for (int i = 2; i < columns.Count + 2; i++)
                row[i] = (((double)(account.GetValue(columns[i - 2]))) / 100).ToString("C");
            //Get difference between Haushaltsplan and last collumn, if there are columns
            row[columns.Count + 2] = (columns.Count > 0) ?
                ((double)(account.GetPlanValue() - Math.Abs(account.GetValue(columns[columns.Count - 1]))) / -100).ToString("C") : 0.00.ToString("C");
            //if not negative, add whitespace in front to format correctly
            if (!row[columns.Count + 2].ToString().Contains(@"-"))
                row[columns.Count + 2] = @" " + row[columns.Count + 2].ToString();
            // Write
            DataGrid_overview.Rows.Add(row);

            // Eventually print subaccounts
            var structuralAccount = account as StructuralAccount;
            if (structuralAccount == null) return;
            foreach (Account subaccount in structuralAccount.Subaccounts)
                PrintAccount(subaccount, order + 1);
        }

        public static string CleanMoneyInput(string moneyString)
        {
            return CleanMoneyInput(moneyString, false);
        }

        public static string CleanMoneyInput(string moneyString, bool allowEmpty)
        {
            // Leerer Input
            if (moneyString == "")
            {
                return allowEmpty ? moneyString : "0";
            }
            // Nur Zahlen und Kommas/Punkte und Minus
            moneyString = Regex.Replace(moneyString, "[^0-9.,-]", "");
            // Ersetze Kommas durch Punkte
            moneyString = moneyString.Replace(',', '.');
            // Fall "123" -> "123.00"
            if (moneyString.LastIndexOf('.') < 0)
            {
                moneyString += ".00";
                return moneyString;
            }
            // Fall "123." -> "123.00"
            if (moneyString.LastIndexOf('.') == moneyString.Length - 1)
                moneyString += "00";
            // Fall "123.4" -> "123.40"
            else if (moneyString.LastIndexOf('.') == moneyString.Length - 2)
                moneyString += "0";
            // Fall "123.45" -> "123.45" implizit
            // Fall "123.456" -> "123.45"
            else if (moneyString.LastIndexOf('.') < moneyString.Length - 3)
                moneyString = moneyString.Substring(0, moneyString.LastIndexOf('.') + 3);
            // Lösche alle Punkte
            moneyString = moneyString.Replace(".", string.Empty);
            // Füge korrekten Punkt ein
            moneyString = moneyString.Insert(moneyString.Length - 2, ".");
            return moneyString;
        }

        public static int MoneyToInt(string moneyString)
        {
            moneyString = CleanMoneyInput(moneyString);
            return Int32.Parse(moneyString.Replace(".", String.Empty));
        }

        private void RefreshUserAccounts()
        {
#if DEBUG
            Console.WriteLine(@"Calling " + MethodBase.GetCurrentMethod().Name + @"...");
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            try
            {
#endif
            DataGrid_userAccounts.Rows.Clear();
            DataGrid_userAccounts.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", Convert.ToSingle(NUD_userAccountsFontSize.Value), GraphicsUnit.Point);
            foreach (
                Account account in
                    Program.Logic.GetAccounts.Where(
                        account => account.PrintToOverview && account.AccountType == AccountTypes.User))
            {
                PrintUserAccount(account, 0);
            }

            DataGrid_userAccounts.AutoResizeColumns();
            DataGrid_userAccounts.AutoResizeRows();
#if DEBUG
            }
            finally
            {
                stopWatch.Stop();
                Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" took " + stopWatch.Elapsed);
            }
#endif
        }

        private static void MoneySortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            if (!((e.Column.Index == 1) || (e.Column.Index == 2))) return;
            int a = MoneyToInt(e.CellValue1.ToString()), b = MoneyToInt(e.CellValue2.ToString());
            e.SortResult = a.CompareTo(b);
            e.Handled = true;
        }

        private void PrintUserAccount(Account account, int order)
        {
            if ((!(account is StructuralAccount)) && account.GetValue() == 0 && !ChkBox_showNullUserAccounts.Checked)
                return;
            // Space Level
            string res = "";
            for (int i = 0; i < order; i++)
                res += "  ";
            // Write
            DataGrid_userAccounts.Rows.Add(new object[]
            {
                res + account.Name, (((double) (account.GetStartValue()))/100).ToString("C"),
                (((double) (account.GetValue()))/100).ToString("C")
            });

            // Eventually print subaccounts
            var structuralAccount = account as StructuralAccount;
            if (structuralAccount == null) return;
            foreach (Account subaccount in structuralAccount.Subaccounts)
                PrintUserAccount(subaccount, order + 1);
        }

        private void RefreshAccounts()
        {
#if DEBUG
            Console.WriteLine(@"Calling " + MethodBase.GetCurrentMethod().Name + @"...");
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            try
            {
#endif
                ListBox_accounts.Items.Clear();
                ListBox_accounts.BeginUpdate();

                

                if (ChkBox_SortByStructural.Checked)
                {
                    //if Checked, recursively create Tree structure sorted by structural accounts

                    //first create list with Structural Accounts satisfying Filters
                    List<StructuralAccount> TreeList = new List<StructuralAccount>(Program.Logic.GetAccounts.OfType<StructuralAccount>().Where(account =>
                                                                               (account.AccountType != AccountTypes.Active || ChkBox_FilterActive.Checked)
                                                                            && (account.AccountType != AccountTypes.Passive || ChkBox_FilterPassive.Checked)
                                                                            && (account.AccountType != AccountTypes.User || ChkBox_FilterUser.Checked)
                                                                            && (account.Name.IndexOf(TxtBox_FilterName.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

                    
                    List<StructuralAccount> TreeListComplete = new List<StructuralAccount>(TreeList);

                    //then remove all Accounts which are themselves Subaccounts; else Accounts are shown multiple times
                    //has to be done with for, because foreach doesn't allow changing the list it loops over
                    for (int index = TreeList.Count - 1; index >= 0; index--)
                    {
                        //index is decreased because TreeList shrinks and index will be smaller than original count
                        for (int indexSub = 0; indexSub < TreeListComplete.Count; indexSub++)
                        {
                            if (indexSub == index)
                                continue;

                            if (TreeListComplete[indexSub].Subaccounts.Contains(TreeList[index]))
                            {
                                TreeList.RemoveAt(index);
                                break;
                            }
                        }
                    }

                    //now recursively build the tree with accounts that are no Subaccounts as root
                    foreach (StructuralAccount account in TreeList)
                    {
                        ListBox_accounts.Items.Add(account.Name);
                        RefreshAccountsRecursive(account, "");
                    }
                }
                else 
                {
                    //if not checked, just show normal list
                    foreach (var account in from account in Program.Logic.GetAccounts
                                            where account.AccountType != AccountTypes.Active || ChkBox_FilterActive.Checked
                                            where account.AccountType != AccountTypes.Passive || ChkBox_FilterPassive.Checked
                                            where account.AccountType != AccountTypes.User || ChkBox_FilterUser.Checked
                                            where !(account is StructuralAccount) || ChkBox_FilterStructural.Checked
                                            where account.Name.IndexOf(TxtBox_FilterName.Text, StringComparison.OrdinalIgnoreCase) >= 0
                                            select account)
                    {
                        if (TxtBox_FilterValue.Text != "")
                            switch (ComBox_FilterValue.SelectedIndex)
                            {
                                case 0:
                                    if (!(account.GetValue() >= MoneyToInt(TxtBox_FilterValue.Text)))
                                        continue;
                                    break;
                                case 1:
                                    if (!(account.GetValue() > MoneyToInt(TxtBox_FilterValue.Text)))
                                        continue;
                                    break;
                                case 2:
                                    if (account.GetValue() != MoneyToInt(TxtBox_FilterValue.Text))
                                        continue;
                                    break;
                                case 3:
                                    if (!(account.GetValue() < MoneyToInt(TxtBox_FilterValue.Text)))
                                        continue;
                                    break;
                                case 4:
                                    if (!(account.GetValue() <= MoneyToInt(TxtBox_FilterValue.Text)))
                                        continue;
                                    break;
                                default:
                                    MessageBox.Show(strings.ErrorInvalidComparisonType);
                                    break;
                            }
                        ListBox_accounts.Items.Add(account.Name);
                    }
                }
                
            ListBox_accounts.EndUpdate();
#if DEBUG
            }
            finally
            {
                stopWatch.Stop();
                Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" took " + stopWatch.Elapsed);
            }
#endif
        }

        private void RefreshAccountsRecursive(StructuralAccount account, string einzug) 
        {
            //so indent gets bigger for subs of a structural account
            einzug += "   ";
            foreach (Account sub in  from sub in account.Subaccounts
                                        where sub.AccountType != AccountTypes.Active || ChkBox_FilterActive.Checked
                                        where sub.AccountType != AccountTypes.Passive || ChkBox_FilterPassive.Checked
                                        where sub.AccountType != AccountTypes.User || ChkBox_FilterUser.Checked
                                        select sub)
            {
                ListBox_accounts.Items.Add(einzug + sub.Name);
                //create subtree
                if (sub as StructuralAccount != null)
                    RefreshAccountsRecursive((StructuralAccount)sub, einzug);
            }
        }

        private void RefreshTransactions()
        {
#if DEBUG
            Console.WriteLine(@"Calling " + MethodBase.GetCurrentMethod().Name + @"...");
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            try
            {
#endif
            DataGrid_transactions.Rows.Clear();
            foreach (Transaction transaction in Program.Logic.GetTransactions)
            {
                var row = new object[]
                {
                    transaction.Id, transaction.TimeStamp,
                    transaction.ReceiptNr, transaction.Category, transaction.Comment
                };
                DataGrid_transactions.Rows.Add(row);
                if (!transaction.ReceiptExists())
                    DataGrid_transactions.Rows[DataGrid_transactions.Rows.Count - 1].DefaultCellStyle.BackColor =
                        Color.Red;
            }
                //Sort the list by index
                DataGrid_transactions.Sort(DataGrid_transactions.Columns[0], 0);
#if DEBUG
            }
            finally
            {
                stopWatch.Stop();
                Console.WriteLine(MethodBase.GetCurrentMethod().Name + @" took " + stopWatch.Elapsed);
            }
#endif
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            new Task(RefreshAll).RunSynchronously();
#if DEBUG
            bildAnzeigeprogrammToolStripMenuItem.Visible = true;
#endif
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if no changes were made, dont ask the user
            if (!Program.Logic.CheckForChanges())
                return;

            //ask if changes should be safed
            DialogResult confirmClosing = MessageBox.Show(strings.ExitConfirm, @"Close the Program", MessageBoxButtons.YesNoCancel);

            switch (confirmClosing) {
                //Safe before exiting
                case DialogResult.Yes:
                    Program.Logic.SaveState(); ; break;

                //don't safe and exit
                case DialogResult.No: ; break;

                //abort the exit
                case DialogResult.Cancel: 
                    e.Cancel = true; break;
            }
        }

        private void NeuerVorgangToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new NewTransaction();
            dialog.ShowDialog();
            if (dialog.DialogResult == DialogResult.OK)
                Program.Logic.NewTransaction(dialog.ResultTransaction);
            this.RefreshTransactions();
        }

        private void NeuesKontoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //safe currently selected item to re-select it after dialog is closed
            object lastSelected = ListBox_accounts.SelectedItem;

            var dialog = new NewAccount();
            dialog.ShowDialog();

            //refresh account list and currently selected item
            RefreshAccounts();
            ListBox_accounts.SelectedItem = lastSelected;
        }

        private void AktivenbeitragToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new NewBigTransaction();
            dialog.ShowDialog();
            this.RefreshTransactions();
        }

        private void ZinsenBerechnenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new NewRelativeTransaction();
            dialog.ShowDialog();
            this.RefreshTransactions();
        }

        private void TabCtrl_Main_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (TabCtrl_main.SelectedIndex)
            {
                case 0:
                    RefreshOverview();
                    break;
                case 1:
                    RefreshUserAccounts();
                    break;
                case 2:
                    RefreshAccounts();
                    break;
                case 3:
                    RefreshTransactions();
                    break;
                default:
                    MessageBox.Show(strings.ErrorInvalidTab);
                    break;
            }
        }

        private void ListBox_accounts_SelectedIndexChanged(object sender, EventArgs e)
        {
            Account selectedAccount = Program.Logic.StrToAccount(ListBox_accounts.GetItemText(ListBox_accounts.SelectedItem).Trim());
            if (selectedAccount == null)
                return;
            Lbl_showValue.Text = (((double) (selectedAccount.GetValue()))/100).ToString("C");
            Lbl_showStartValue.Text = (((double) (selectedAccount.GetStartValue()))/100).ToString("C");
            Lbl_showPlanValue.Text = (((double) (selectedAccount.GetPlanValue()))/100).ToString("C");
            switch (selectedAccount.AccountType)
            {
                case AccountTypes.Active:
                    Lbl_showAccountType.Text = strings.Active;
                    break;
                case AccountTypes.Passive:
                    Lbl_showAccountType.Text = strings.Passive;
                    break;
                case AccountTypes.User:
                    Lbl_showAccountType.Text = strings.User;
                    break;
                default:
                    Lbl_showAccountType.Text = strings.ErrorInvalidAccountType;
                    break;
            }
            if (selectedAccount as StructuralAccount != null)
                Lbl_showAccountType.Text += @" / Strukturkonto";
            Tuple<List<DateTime>, List<string>, List<string>, List<int>> history = Program.Logic.GetAccountHistory(selectedAccount.Name);
            TxtBox_history.Clear();
            for (int i = 0; i < history.Item3.Count; i++)
            {
                TxtBox_history.AppendText(history.Item1[i] + @" " + history.Item2[i] + @": " + (((double) (history.Item4[i]))/100).ToString("C") +
                                          @" (" + history.Item3[i] + @")" + Environment.NewLine);
            }
            TxtBox_history.AppendText(strings.LatestValue +
                                      (((double) (Program.Logic.StrToAccount(selectedAccount.Name).GetValue()))/100)
                                          .ToString("C"));
        }

        private void ListBox_accounts_DoubleClick(object sender, EventArgs e) 
        {
            //safe currently selected item to re-select it after dialog is closed
            Account selectedAccount = Program.Logic.StrToAccount(ListBox_accounts.GetItemText(ListBox_accounts.SelectedItem).Trim());
            if (selectedAccount == null)
                return;

            //LegacyAccount, which replaces Start Values cannot be edited
            if (ListBox_accounts.GetItemText(ListBox_accounts.SelectedItem).Trim() == Settings.Default.LegacyAccount)
                return;

            //start dialog window
            var dialog =
            new EditAccount(selectedAccount);
            dialog.ShowDialog();

            //refresh account list and currently selected item
            RefreshAccounts();
            ListBox_accounts.SelectedItem = selectedAccount;
        }

        private void SpeichernToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.Logic.SaveState();
        }

        private void LadenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.Logic.LoadState();
            RefreshAll();
        }

        private void NeueSpalteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new DateDialog();
            dialog.ShowDialog();
            if(dialog.DialogResult == DialogResult.OK)
                Program.Logic.OverviewColumns.Add(dialog.time);
            RefreshOverview();
        }

        private void Btn_editAccount_Click(object sender, EventArgs e)
        {
            //to prevent crash, check if an account is selected
            if (ListBox_accounts.SelectedItem != null) {

                //LegacyAccount, which replaces Start Values cannot be edited
                if (ListBox_accounts.GetItemText(ListBox_accounts.SelectedItem).Trim() == Settings.Default.LegacyAccount)
                    return;

                //safe currently selected item to re-select it after dialog is closed
                object lastSelected = ListBox_accounts.SelectedItem;

                //start dialog window
                var dialog =
                new EditAccount(Program.Logic.StrToAccount(ListBox_accounts.GetItemText(ListBox_accounts.SelectedItem).Trim()));
                dialog.ShowDialog();

                //refresh account list and currently selected item
                RefreshAccounts();
                ListBox_accounts.SelectedItem = lastSelected;

            } else
                MessageBox.Show(strings.ErrorNoAccountSelected);
            
        }

        private void Btn_deleteAccount_Click(object sender, EventArgs e)
        {
            //to prevent crash, check if an account is selected
            if (ListBox_accounts.SelectedItem != null) {

                //safe currently selected item to re-select it after dialog is closed
                object lastSelected = ListBox_accounts.SelectedItem;
                //is lastSelected the last Item in the list?
                int nextOrPrev = ListBox_accounts.Items.IndexOf(ListBox_accounts.SelectedItem) >= ListBox_accounts.Items.Count - 1 ? -1 : 1;

                //Ask if deletion is wanted
                if (MessageBox.Show(strings.DeleteConfirm, "", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                    if (Program.Logic.DeleteAccount(ListBox_accounts.GetItemText(ListBox_accounts.SelectedItem).Trim()))
                    {
                        //set lastSelected to next Item in the list or if at end of the list, the previous
                        lastSelected = ListBox_accounts.Items[ListBox_accounts.Items.IndexOf(lastSelected) + nextOrPrev];

                        //refresh account list and currently selected item
                        RefreshAccounts();
                        ListBox_accounts.SelectedItem = lastSelected;
                    }
                    else
                        MessageBox.Show(strings.DeleteAccountFailed);
                }
                
            } else
                MessageBox.Show(strings.ErrorNoAccountSelected);
        }

        private void EMailsVersendenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new SendEmails();
            dialog.ShowDialog();
        }

        private void showTransactionInTxtBox(Transaction transaction)
        {
            if (transaction == null)
                return;
            Txtbox_transactionInfo.Clear();

            for (int i = 0; i < transaction.Accounts.Count; i++)
            {
                Txtbox_transactionInfo.Text += transaction.Accounts[i].Name + @": " +
                                               (((double)transaction.Values[i]) / 100).ToString("C") +
                                               Environment.NewLine;
            }
        }

        private void DataGrid_transactions_SelectionChanged(object sender, EventArgs e)
        {
            if (DataGrid_transactions.CurrentRow == null)
                return;

            Transaction transaction = Program.Logic.GetTransaction((int) DataGrid_transactions.CurrentRow.Cells[0].Value);
            showTransactionInTxtBox(transaction);
        }

        private void Btn_deleteTransaction_Click(object sender, EventArgs e)
        {
            if (DataGrid_transactions.CurrentRow == null)
            {
                MessageBox.Show(strings.NoTransactionSelected);
                return;
            }

            //TODO NEW_ERA--
            //LegacyTransaction, which replaces Start Values, cannot be deleted; it always has ID 0
            if ((int)DataGrid_transactions.CurrentRow.Cells[0].Value == 0)
                return;
            

            //Ask if deletion is wanted; if not, do nothing
            if (MessageBox.Show(strings.DeleteConfirm, "", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                int rowSafe = DataGrid_transactions.CurrentRow.Index;
                int colSafe = DataGrid_transactions.CurrentCell.ColumnIndex;
                Program.Logic.DeleteTransaction((int)DataGrid_transactions.CurrentRow.Cells[0].Value);
                RefreshTransactions();
                DataGrid_transactions.CurrentCell = DataGrid_transactions[colSafe, Math.Min(rowSafe, DataGrid_transactions.RowCount - 1)];

                Transaction transaction = Program.Logic.GetTransaction((int)DataGrid_transactions.CurrentRow.Cells[0].Value);
                showTransactionInTxtBox(transaction);
            }
        }

        private void Btn_OpenTransacReceipt_Click(object sender, EventArgs e)
        {
            if (DataGrid_transactions.CurrentRow == null)
            {
                MessageBox.Show(strings.NoTransactionSelected);
                return;
            }
            int receiptNr = (int)DataGrid_transactions.CurrentRow.Cells[2].Value;
            if (receiptNr != 0)
            {
                IEnumerable<string> reciepts = Directory.EnumerateFiles(strings.RecipeDir, receiptNr + "_*");
                foreach(string path in reciepts)
                    System.Diagnostics.Process.Start(path);
            }
        }

        private void Btn_EditTransaction_Click(object sender, EventArgs e)
        {
            if (DataGrid_transactions.CurrentRow == null)
            {
                MessageBox.Show(strings.NoTransactionSelected);
                return;
            }

            //TODO NEW_ERA--
            //LegacyTransaction, which replaces Start Values, cannot be changed; it always has ID 0
            if ((int)DataGrid_transactions.CurrentRow.Cells[0].Value == 0)
                return;
            

            int rowSafe = DataGrid_transactions.CurrentRow.Index;
            int colSafe = DataGrid_transactions.CurrentCell.ColumnIndex;

            var dialog = new NewTransaction(Program.Logic.GetTransaction((int)DataGrid_transactions.CurrentRow.Cells[0].Value));
            dialog.ShowDialog();

            //If all ok, delete old transaction and add new one
            if (dialog.DialogResult == DialogResult.OK) {
                Program.Logic.DeleteTransaction((int)DataGrid_transactions.CurrentRow.Cells[0].Value);
                Program.Logic.NewTransaction(dialog.ResultTransaction);


                RefreshTransactions();
                DataGrid_transactions.CurrentCell = DataGrid_transactions[colSafe, Math.Min(rowSafe, DataGrid_transactions.RowCount - 1)];

                Transaction transaction = Program.Logic.GetTransaction((int)DataGrid_transactions.CurrentRow.Cells[0].Value);
                showTransactionInTxtBox(transaction);
            }
        }

        private void SemesterArchivierenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult confirmDialog = MessageBox.Show(strings.ArchiveConfirmation.Replace(@"%id%",
                            Logic.Logic.GetNewArchiveId().ToString(CultureInfo.InvariantCulture)), strings.ArchiveConfirmationTitle,
                            MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
            if (confirmDialog != DialogResult.OK) return;
            if (!Program.Logic.ArchiveState())
            {
                MessageBox.Show(strings.ArchiveError);
                return;
            }
            MessageBox.Show(strings.ArchiveSuccessful);
        }

        private void neuesSemesterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new NewTimeInterval();
            dialog.ShowDialog();
            if (dialog.DialogResult == DialogResult.OK)
            {
                Program.Logic = dialog.ResultLogic;
                //TODO NEW_ERA--
                Program.Logic.NewTransaction(dialog.LegacyTransaction);
            }
            RefreshAll();
        }

        private void ladenAusDateiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog {InitialDirectory = Environment.CurrentDirectory, Filter = @"xml files (*.xml)|*.xml|All files (*.*)|*.*"};
            if (dialog.ShowDialog() != DialogResult.OK) return;
            if (!(Program.Logic.LoadStateFromFile(dialog.FileName)))
            MessageBox.Show(strings.LoadFromFileFailed);
            RefreshAll();
        }

        private void NUD_OverviewFontSize_ValueChanged(object sender, EventArgs e)
        {
            RefreshOverview();
        }

        private void NUD_userAccounts_ValueChanged(object sender, EventArgs e)
        {
            RefreshUserAccounts();
        }

        private void deleteColumnStripMenuItem_Click(object sender, EventArgs e)
        {
            var columnToRemove = ColumnContextMenuCell.ColumnIndex - 2;
            if (columnToRemove >= 0)
                Program.Logic.OverviewColumns.RemoveAt(columnToRemove);
            RefreshOverview();
        }

        private void Menu_ColumnContext_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var hitPoint = DataGrid_overview.PointToClient(Menu_ColumnContext.Location);
            ColumnContextMenuCell = DataGrid_overview.HitTest(hitPoint.X, hitPoint.Y);
            spalteLoeschenStripMenuItem.Enabled = ColumnContextMenuCell.ColumnIndex >= 2 && ColumnContextMenuCell.ColumnIndex < DataGrid_overview.ColumnCount-1;
            RefreshOverview();
        }

        private void ChkBox_showNullUserAccounts_CheckedChanged(object sender, EventArgs e)
        {
            RefreshUserAccounts();
        }

        private void ChkBox_FilterActive_CheckedChanged(object sender, EventArgs e)
        {
            RefreshAccounts();
        }

        private void ChkBox_FilterPassive_CheckedChanged(object sender, EventArgs e)
        {
            RefreshAccounts();
        }

        private void ChkBox_FilterUser_CheckedChanged(object sender, EventArgs e)
        {
            RefreshAccounts();
        }

        private void ChkBox_FilterStructural_CheckedChanged(object sender, EventArgs e)
        {
            RefreshAccounts();
        }

        private void TxtBox_FilterName_TextChanged(object sender, EventArgs e)
        {
            RefreshAccounts();
        }

        private void TxtBox_FilterValue_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Console.WriteLine(sender.GetType());
            ((System.Windows.Forms.TextBox)sender).Text = CleanMoneyInput(((System.Windows.Forms.TextBox)sender).Text, true);
            RefreshAccounts();
        }

        private void ComBox_FilterValue_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshAccounts();
        }

        private void einstellungenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new SettingsDialog();
            dialog.ShowDialog();
        }

        private void bildAnzeigeprogrammToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new LearningBasics();
            dialog.ShowDialog();
        }

        private void Btn_overviewReset_Click(object sender, EventArgs e)
        {
            RefreshOverview();
        }

        private void Btn_userAccountsReset_Click(object sender, EventArgs e)
        {
            RefreshUserAccounts();
        }

        private void speichernUnterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new SaveFileDialog { InitialDirectory = Environment.CurrentDirectory, Filter = @"xml files (*.xml)|*.xml" };
            if (dialog.ShowDialog() != DialogResult.OK) return;
            Program.Logic.SaveStateToFile(dialog.FileName);
        }

        private void ChkBox_SortByStructural_CheckedChanged(object sender, EventArgs e)
        {
            //Deactivate activated Structural checkbox, because structural accounts have to be shown here
            //And Filter by Value, because that is not a logical criterium in a structural accounts tree
            if (ChkBox_SortByStructural.Checked)
            {
                ChkBox_FilterStructural.Checked = true;
                ChkBox_FilterStructural.Enabled = false;
                TxtBox_FilterValue.Enabled = false;
            }
            else
            {
                ChkBox_FilterStructural.Enabled = true;
                TxtBox_FilterValue.Enabled = true;
            }

            RefreshAccounts();
        }

        private void hilfeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //TODO
            MessageBox.Show("Hier wird eventuell irgendwann mal ein Tutorial oder ähnliches stehen");
        }

        private void BeendenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ListBox_accounts_KeyDown(object sender, KeyEventArgs e)
        {
            //safe currently selected item to re-select it after dialog is closed
            object lastSelected = ListBox_accounts.SelectedItem;

            if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Clear || e.KeyCode == Keys.Back)
            {
                //Ask if deletion is wanted
                if (MessageBox.Show(strings.DeleteConfirm, "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    //is lastSelected the last Item in the list?
                    int nextOrPrev = ListBox_accounts.Items.IndexOf(lastSelected) >= ListBox_accounts.Items.Count-1 ? -1 : 1;

                    if (!Program.Logic.DeleteAccount(ListBox_accounts.GetItemText(ListBox_accounts.SelectedItem).Trim()))
                        MessageBox.Show(strings.DeleteAccountFailed);
                    //set lastSelected to next Item in the list or if at end of the list, the previous
                    else
                        lastSelected = ListBox_accounts.Items[ListBox_accounts.Items.IndexOf(lastSelected)+nextOrPrev];
                        
                }
            }
            else if(e.KeyCode == Keys.Enter || e.KeyCode == Keys.Space)
            {

                //start dialog window
                var dialog =
                    new EditAccount(Program.Logic.StrToAccount(ListBox_accounts.GetItemText(ListBox_accounts.SelectedItem).Trim()));
                dialog.ShowDialog();
            }
            else if(e.KeyCode == Keys.Insert)
            {

                var dialog = new NewAccount();
                dialog.ShowDialog();

            }

            //refresh account list and currently selected item
            RefreshAccounts();
            ListBox_accounts.SelectedItem = lastSelected;
        }

        private void transaktionenExportierenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dialog = new SaveFileDialog { InitialDirectory = Environment.CurrentDirectory, Filter = @"txt files (*.txt)|*.txt" };
            if (dialog.ShowDialog() != DialogResult.OK) return;
            File.WriteAllText(dialog.FileName, Program.Logic.GetTransactionsPrint());
        }

        private void Btn_newColumn_Click(object sender, EventArgs e)
        {

            var dialog = new DateDialog();
            dialog.ShowDialog();
            if (dialog.DialogResult == DialogResult.OK)
                Program.Logic.OverviewColumns.Add(dialog.time);
            RefreshOverview();
        }

        private void schuldnerlisteExportierenExelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Excel.Application XLApp;
            Excel._Workbook XLWorkBook;
            Excel._Worksheet XLWorkSheet;
            Excel.Range XLRange;//used to define ranges of cells to manipulate at once
            List<Account> structAccounts;//the accounts to be exported (by structural parent)

            //Start Excel and get Application object.
            XLApp = new Excel.Application();
            if(XLApp == null)
            {
                MessageBox.Show(strings.ErrorExcelNotFound);
                return;
            }

            //which accounts should be exported
            var dialog = new ExportDialog();
            if (dialog.ShowDialog() != DialogResult.OK)
                return;
            else
                structAccounts = dialog.StructAccounts;

            //Get a new workbook.
            XLWorkBook = (Excel._Workbook) XLApp.Workbooks.Add(Missing.Value);
            XLWorkSheet = (Excel._Worksheet) XLWorkBook.ActiveSheet;
            XLWorkSheet.Name = "Gesamtschuldnerliste";

            //Add table headers going cell by cell.
            XLWorkSheet.Cells[1, 2] = "Aktivenkasse";
            XLWorkSheet.Cells[1, 3] = "Hauskasse";
            XLWorkSheet.Cells[1, 4] = "Fuxenkasse";
            XLWorkSheet.Cells[1, 5] = "Bierkasse";
            XLWorkSheet.Cells[1, 6] = "Summe";
            XLWorkSheet.Cells[2, 7] = "Passwort";
            XLWorkSheet.Cells[2, 1] = "Stand";
            XLWorkSheet.Cells[2, 2] = DateTime.Today;
            XLWorkSheet.Cells[2, 3] = "??.??.????";//don't know the last update date
            XLWorkSheet.Cells[2, 4] = "??.??.????";
            XLWorkSheet.Cells[2, 5] = "??.??.????";
            XLWorkSheet.Cells[3, 7] = "bergland";

            //Format Headers
            XLWorkSheet.get_Range("A1", "F1").Font.Bold = true;
            XLWorkSheet.get_Range("A2", "F2").Font.Bold = true;

            //Add Names and their value
            int rowCounter = 2;//how many rows has the table
            foreach (Account account in Program.Logic.GetAccounts.Where(
                        account => !(account is StructuralAccount) && account.AccountType == AccountTypes.User))
            {
                foreach(StructuralAccount structAcc in structAccounts)
                {
                    if(structAcc.Subaccounts.Contains(account))
                    {
                        rowCounter++;
                        XLWorkSheet.Cells[rowCounter, 1] = account.Name;
                        XLWorkSheet.Cells[rowCounter, 2] = (double)account.GetValue() / 100;
                        break;
                    }
                }
            }

            //change collumn width to fit for all names
            XLRange = XLWorkSheet.get_Range("A1", "B1");
            XLRange.EntireColumn.AutoFit();

            //add formulas for the sum collumn and format it
            XLRange = XLWorkSheet.get_Range("F3", "F" + rowCounter);
            XLRange.Formula = "=SUMIF(B3:E3,\">0\")";
            XLRange.Borders.get_Item(Excel.XlBordersIndex.xlEdgeLeft).Weight = Excel.XlBorderWeight.xlThin;

            //add formulas for the sum row on the bottom of the list and format it
            XLRange = XLWorkSheet.get_Range("B" + (rowCounter + 2), "F" + (rowCounter + 2));
            XLRange.Formula = "=SUMIF(B3:B" + rowCounter + ",\">0\")";
            XLRange.Borders.get_Item(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlDouble;

            //Format  Header and Collumns with names
            XLRange = XLWorkSheet.get_Range("A1", "F2");
            XLRange.Interior.Color = XlRgbColor.rgbYellow;
            XLRange.Borders.Weight = Excel.XlBorderWeight.xlThin;
            XLRange = XLWorkSheet.get_Range("A1", "A" + rowCounter);
            XLRange.Interior.Color = XlRgbColor.rgbYellow;

            //Format Collumns with numbers
            XLRange = XLWorkSheet.get_Range("B3", "F" + (rowCounter + 2));
            XLRange.NumberFormat = "0.00€";

            XLRange = XLRange.get_Resize(rowCounter - 2, Missing.Value);//last row isn't colored
            XLRange.Borders.get_Item(Excel.XlBordersIndex.xlInsideHorizontal).Weight = Excel.XlBorderWeight.xlThin;

            //add rules to change color depending on values
            FormatCondition format1 = XLRange.FormatConditions.Add(XlFormatConditionType.xlCellValue,
                                                   XlFormatConditionOperator.xlGreater, "=" + Settings.Default.OverviewDeptBorder);
            format1.Font.Color = XlRgbColor.rgbDarkRed;//Red if over a specific amount of dept
            format1.Interior.Color = XlRgbColor.rgbLightPink;
            FormatCondition format2 = XLRange.FormatConditions.Add(XlFormatConditionType.xlCellValue,
                                                   XlFormatConditionOperator.xlLess, "=-1");
            format2.Font.Color = XlRgbColor.rgbDarkGreen;//Green if under -1 Euro
            format2.Interior.Color = XlRgbColor.rgbLightGreen;

            FormatCondition format3 = XLRange.FormatConditions.Add(XlFormatConditionType.xlCellValue,
                                                   XlFormatConditionOperator.xlBetween, "=-1", "=-0.01");
            format3.Font.Color = XlRgbColor.rgbBrown;//Yellow(ish) if between 0 and -1 Euro
            format3.Interior.Color = XlRgbColor.rgbPaleGoldenrod;

            //Auto populate the table with the dates from the other sheets
            XLRange = XLRange.get_Resize(Missing.Value, 4);

            //lock the sheet, then unprotect all parts that should be editable
            XLRange.Locked = false;
            XLRange = XLWorkSheet.get_Range("B2", "E2");//the dates
            XLRange.Locked = false;
            XLWorkSheet.Protect("bergland");

            //freeze first row
            XLWorkSheet.Application.ActiveWindow.SplitRow = 1;
            XLWorkSheet.Application.ActiveWindow.FreezePanes = true;

            //Create a new sheet for each cashbox and link it to the right column
            XLRange = XLWorkSheet.get_Range("C3", "C" + rowCounter);
            addAutoMatch(XLWorkBook, XLRange, Convert.ToString(XLRange.EntireColumn.Cells[1].Value2));
            XLRange = XLWorkSheet.get_Range("D3", "D" + rowCounter);
            addAutoMatch(XLWorkBook, XLRange, Convert.ToString(XLRange.EntireColumn.Cells[1].Value2));
            XLRange = XLWorkSheet.get_Range("E3", "E" + rowCounter);
            addAutoMatch(XLWorkBook, XLRange, Convert.ToString(XLRange.EntireColumn.Cells[1].Value2));



            var Savedialog = new SaveFileDialog { InitialDirectory = Environment.CurrentDirectory, Filter = @"Exel-Arbeitsmappe (*.xlsx)|*.xlsx" };
            if (Savedialog.ShowDialog() == DialogResult.OK)
                XLWorkBook.SaveAs(Savedialog.FileName);

            //Make sure Excel is visible and give the user control of Microsoft Excel's lifetime.
            XLWorkSheet.Activate();
            XLApp.Visible = true;
            XLApp.UserControl = true;
        }

        private void addAutoMatch(Excel._Workbook XLWorkBook, Excel.Range XLRange, string name)
        {
            //Create new sheet and link column with it
            createNewWorksheet(XLWorkBook, name);
            XLRange.Formula = "=INDEX(" + name + "!B$3:B$200,MATCH($A3," + name + "!A$3:A$200,0))";
        }

        private void createNewWorksheet(Excel._Workbook XLWorkBook, string name)
        {
            //create a new sheet where user data can be added
            XLWorkBook.Sheets.Add(After: XLWorkBook.Sheets[XLWorkBook.Sheets.Count]);
            Excel._Worksheet XLWorkSheet = XLWorkBook.Sheets[XLWorkBook.Sheets.Count];

            XLWorkSheet.Name = name;
            XLWorkSheet.Cells[1, 1] = name;
            XLWorkSheet.Cells[1, 1].Font.Size = 16;
            XLWorkSheet.Cells[1, 1].Font.Bold = true;
            XLWorkSheet.Rows[1].AutoFit();

            XLWorkSheet.Cells[2, 1] = "Name";
            XLWorkSheet.Cells[2, 2] = "Schulden/Guthaben";

            XLWorkSheet.Columns[1].ColumnWidth = 20;
            XLWorkSheet.Columns[2].ColumnWidth = 20;

        }

        private void schuldnerlisteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<Account> structAccounts;

            //which accounts should be exported (by structural parent)
            var dialog = new ExportDialog();
            if (dialog.ShowDialog() != DialogResult.OK)
                return;
            else
                structAccounts = dialog.StructAccounts;

            var SaveDialog = new SaveFileDialog { InitialDirectory = Environment.CurrentDirectory,
                Filter = @"Exel-Arbeitsmappe (*.xlsx)|*.xlsx|txt files (*.txt)|*.txt"
            };
            if (SaveDialog.ShowDialog() != DialogResult.OK) return;
            switch(SaveDialog.FilterIndex)
            {
                case 1: createDeptListExcel(structAccounts, SaveDialog.FileName); break;
                case 2: File.WriteAllText(SaveDialog.FileName, Program.Logic.GetUsersPrint(structAccounts)); break;
            }
        }

        private void createDeptListExcel(List<Account> structAccounts, string fileName)
        {
            Excel.Application XLApp;
            Excel._Workbook XLWorkBook;
            Excel._Worksheet XLWorkSheet;

            //Start Excel and get Application object.
            XLApp = new Excel.Application();
            if (XLApp == null)
            {
                MessageBox.Show(strings.ErrorExcelNotFound);
                return;
            }

            //Get a new workbook.
            XLWorkBook = (Excel._Workbook)XLApp.Workbooks.Add(Missing.Value);
            XLWorkSheet = (Excel._Worksheet)XLWorkBook.ActiveSheet;
            XLWorkSheet.Name = "Schuldnerliste";

            XLWorkSheet.Cells[1, 1] = "Name";
            XLWorkSheet.Cells[1, 2] = "Schulden/Guthaben";

            //Add Names and their value
            int rowCounter = 1;
            foreach (Account account in Program.Logic.GetAccounts.Where(
                        account => !(account is StructuralAccount) && account.AccountType == AccountTypes.User))
            {
                foreach (StructuralAccount structAcc in structAccounts)
                {
                    if (structAcc.Subaccounts.Contains(account))
                    {
                        rowCounter++;
                        XLWorkSheet.Cells[rowCounter, 1] = account.Name;
                        XLWorkSheet.Cells[rowCounter, 2] = (double)account.GetValue() / 100;
                        break;
                    }
                }
            }

            //autofit collumns to content
            XLWorkSheet.get_Range("A1", "B1").EntireColumn.AutoFit();


            XLWorkBook.SaveAs(fileName);
            XLApp.Quit();
        }
    }
}