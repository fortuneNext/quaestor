﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Quaestor.Logic;

namespace Quaestor.GUI
{
    public partial class SendEmails : Form
    {
        private List<UserAccount> _users;

        public SendEmails()
        {
            InitializeComponent();
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SendEmails_Load(object sender, EventArgs e)
        {
            _users = new List<UserAccount>();
            /*foreach (
                Account account in
                    Program.Logic.GetAccounts.Where(
                        account => (account is UserAccount) && Logic.Logic.IsValidEmail(((UserAccount) account).Email)))
                _users.Add((UserAccount) account);
            foreach (UserAccount user in _users)
                ChkListBox_receipants.Items.Add(user.Name + " (" + user.Email + ")");*/
            foreach (StructuralAccount account in Program.Logic.GetAccounts.Where(
                        account => (account.AccountType is AccountTypes.User) && (account is StructuralAccount)))
            {
                //Add headers to devide the  users by structual User Accounts
                ChkListBox_receipants.Items.Add(@"-----" + account.Name + @"-----");
                ChkListBox_receipants.SetDivider(ChkListBox_receipants.Items.Count-1, true);
                foreach (UserAccount sub in account.Subaccounts.Where(
                        sub => (sub is UserAccount) && Logic.Logic.IsValidEmail(((UserAccount)sub).Email)))
                {
                    ChkListBox_receipants.Items.Add(sub.Name + " (" + sub.Email + ")");
                    _users.Add(sub);
                }
            }
                

        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            var checkedUsers = new List<string>();
            for (int i = 0; i < ChkListBox_receipants.Items.Count; i++)
            {
                if (ChkListBox_receipants.GetItemChecked(i))
                {
                    //Remove all but the name and add that to List; -1 because of blanc in front of (
                    checkedUsers.Add(ChkListBox_receipants.Items[i].ToString().Remove(ChkListBox_receipants.Items[i].ToString().IndexOf('(') - 1));
                }
            }
            if (!Program.Logic.SendEmailToUsers(checkedUsers))
                MessageBox.Show(strings.NoEmailsSent);
            Close();
        }
    }
}