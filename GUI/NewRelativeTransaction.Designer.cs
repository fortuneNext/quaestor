﻿namespace Quaestor.GUI
{
    partial class NewRelativeTransaction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewRelativeTransaction));
            this.TxtBox_interest = new System.Windows.Forms.TextBox();
            this.Btn_OK = new System.Windows.Forms.Button();
            this.Btn_Cancel = new System.Windows.Forms.Button();
            this.Lbl_receiptNr = new System.Windows.Forms.Label();
            this.TxtBox_receiptNr = new System.Windows.Forms.TextBox();
            this.Dtp_timeStamp = new System.Windows.Forms.DateTimePicker();
            this.Lbl_category = new System.Windows.Forms.Label();
            this.TxtBox_category = new System.Windows.Forms.TextBox();
            this.Lbl_comment = new System.Windows.Forms.Label();
            this.TxtBox_comment = new System.Windows.Forms.TextBox();
            this.Lbl_ID = new System.Windows.Forms.Label();
            this.Lbl_showID = new System.Windows.Forms.Label();
            this.Grp_active = new System.Windows.Forms.GroupBox();
            this.Lbl_threshold = new System.Windows.Forms.Label();
            this.Lbl_interest = new System.Windows.Forms.Label();
            this.TxtBox_threshold = new System.Windows.Forms.TextBox();
            this.Lbl_timeStamp = new System.Windows.Forms.Label();
            this.ChkBox_sendEmails = new System.Windows.Forms.CheckBox();
            this.Grp_passive = new System.Windows.Forms.GroupBox();
            this.Lbl_SumPassive = new System.Windows.Forms.Label();
            this.LayoutPan_passive = new System.Windows.Forms.TableLayoutPanel();
            this.TxtBox_left1 = new System.Windows.Forms.TextBox();
            this.ComBox_left1 = new System.Windows.Forms.ComboBox();
            this.TxtBox_left2 = new System.Windows.Forms.TextBox();
            this.ComBox_left5 = new System.Windows.Forms.ComboBox();
            this.TxtBox_left4 = new System.Windows.Forms.TextBox();
            this.TxtBox_left3 = new System.Windows.Forms.TextBox();
            this.TxtBox_left5 = new System.Windows.Forms.TextBox();
            this.ComBox_left4 = new System.Windows.Forms.ComboBox();
            this.ComBox_left3 = new System.Windows.Forms.ComboBox();
            this.ComBox_left2 = new System.Windows.Forms.ComboBox();
            this.Btn_LeftAdd = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.ChkListBox_right1 = new Quaestor.CheckedListBoxWithDivider();
            this.Grp_active.SuspendLayout();
            this.Grp_passive.SuspendLayout();
            this.LayoutPan_passive.SuspendLayout();
            this.SuspendLayout();
            // 
            // TxtBox_interest
            // 
            resources.ApplyResources(this.TxtBox_interest, "TxtBox_interest");
            this.TxtBox_interest.Name = "TxtBox_interest";
            this.toolTip1.SetToolTip(this.TxtBox_interest, resources.GetString("TxtBox_interest.ToolTip"));
            this.TxtBox_interest.TextChanged += new System.EventHandler(this.TxtBox_interest_TextChanged);
            this.TxtBox_interest.Enter += new System.EventHandler(this.TxtBox_interest_Enter);
            this.TxtBox_interest.Validating += new System.ComponentModel.CancelEventHandler(this.TxtBox_interest_Validating);
            // 
            // Btn_OK
            // 
            resources.ApplyResources(this.Btn_OK, "Btn_OK");
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_OK_Click);
            // 
            // Btn_Cancel
            // 
            resources.ApplyResources(this.Btn_Cancel, "Btn_Cancel");
            this.Btn_Cancel.CausesValidation = false;
            this.Btn_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Btn_Cancel.Name = "Btn_Cancel";
            this.Btn_Cancel.UseVisualStyleBackColor = true;
            this.Btn_Cancel.Click += new System.EventHandler(this.Btn_Cancel_Click);
            // 
            // Lbl_receiptNr
            // 
            resources.ApplyResources(this.Lbl_receiptNr, "Lbl_receiptNr");
            this.Lbl_receiptNr.Name = "Lbl_receiptNr";
            this.toolTip1.SetToolTip(this.Lbl_receiptNr, resources.GetString("Lbl_receiptNr.ToolTip"));
            // 
            // TxtBox_receiptNr
            // 
            resources.ApplyResources(this.TxtBox_receiptNr, "TxtBox_receiptNr");
            this.TxtBox_receiptNr.Name = "TxtBox_receiptNr";
            this.toolTip1.SetToolTip(this.TxtBox_receiptNr, resources.GetString("TxtBox_receiptNr.ToolTip"));
            this.TxtBox_receiptNr.Validating += new System.ComponentModel.CancelEventHandler(this.TxtBox_receiptNr_Validating);
            // 
            // Dtp_timeStamp
            // 
            resources.ApplyResources(this.Dtp_timeStamp, "Dtp_timeStamp");
            this.Dtp_timeStamp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.Dtp_timeStamp.Name = "Dtp_timeStamp";
            // 
            // Lbl_category
            // 
            resources.ApplyResources(this.Lbl_category, "Lbl_category");
            this.Lbl_category.Name = "Lbl_category";
            // 
            // TxtBox_category
            // 
            resources.ApplyResources(this.TxtBox_category, "TxtBox_category");
            this.TxtBox_category.Name = "TxtBox_category";
            // 
            // Lbl_comment
            // 
            resources.ApplyResources(this.Lbl_comment, "Lbl_comment");
            this.Lbl_comment.Name = "Lbl_comment";
            // 
            // TxtBox_comment
            // 
            resources.ApplyResources(this.TxtBox_comment, "TxtBox_comment");
            this.TxtBox_comment.Name = "TxtBox_comment";
            // 
            // Lbl_ID
            // 
            resources.ApplyResources(this.Lbl_ID, "Lbl_ID");
            this.Lbl_ID.Name = "Lbl_ID";
            this.toolTip1.SetToolTip(this.Lbl_ID, resources.GetString("Lbl_ID.ToolTip"));
            // 
            // Lbl_showID
            // 
            resources.ApplyResources(this.Lbl_showID, "Lbl_showID");
            this.Lbl_showID.Name = "Lbl_showID";
            this.toolTip1.SetToolTip(this.Lbl_showID, resources.GetString("Lbl_showID.ToolTip"));
            // 
            // Grp_active
            // 
            resources.ApplyResources(this.Grp_active, "Grp_active");
            this.Grp_active.Controls.Add(this.Lbl_threshold);
            this.Grp_active.Controls.Add(this.Lbl_interest);
            this.Grp_active.Controls.Add(this.TxtBox_threshold);
            this.Grp_active.Controls.Add(this.ChkListBox_right1);
            this.Grp_active.Controls.Add(this.TxtBox_interest);
            this.Grp_active.Name = "Grp_active";
            this.Grp_active.TabStop = false;
            // 
            // Lbl_threshold
            // 
            resources.ApplyResources(this.Lbl_threshold, "Lbl_threshold");
            this.Lbl_threshold.Name = "Lbl_threshold";
            this.toolTip1.SetToolTip(this.Lbl_threshold, resources.GetString("Lbl_threshold.ToolTip"));
            // 
            // Lbl_interest
            // 
            resources.ApplyResources(this.Lbl_interest, "Lbl_interest");
            this.Lbl_interest.Name = "Lbl_interest";
            this.toolTip1.SetToolTip(this.Lbl_interest, resources.GetString("Lbl_interest.ToolTip"));
            // 
            // TxtBox_threshold
            // 
            resources.ApplyResources(this.TxtBox_threshold, "TxtBox_threshold");
            this.TxtBox_threshold.Name = "TxtBox_threshold";
            this.toolTip1.SetToolTip(this.TxtBox_threshold, resources.GetString("TxtBox_threshold.ToolTip"));
            this.TxtBox_threshold.TextChanged += new System.EventHandler(this.TxtBox_threshold_TextChanged);
            this.TxtBox_threshold.Validating += new System.ComponentModel.CancelEventHandler(this.TxtBox_threshold_Validating);
            // 
            // Lbl_timeStamp
            // 
            resources.ApplyResources(this.Lbl_timeStamp, "Lbl_timeStamp");
            this.Lbl_timeStamp.Name = "Lbl_timeStamp";
            // 
            // ChkBox_sendEmails
            // 
            resources.ApplyResources(this.ChkBox_sendEmails, "ChkBox_sendEmails");
            this.ChkBox_sendEmails.Name = "ChkBox_sendEmails";
            this.ChkBox_sendEmails.UseVisualStyleBackColor = true;
            // 
            // Grp_passive
            // 
            resources.ApplyResources(this.Grp_passive, "Grp_passive");
            this.Grp_passive.Controls.Add(this.Lbl_SumPassive);
            this.Grp_passive.Controls.Add(this.LayoutPan_passive);
            this.Grp_passive.Controls.Add(this.Btn_LeftAdd);
            this.Grp_passive.Name = "Grp_passive";
            this.Grp_passive.TabStop = false;
            this.toolTip1.SetToolTip(this.Grp_passive, resources.GetString("Grp_passive.ToolTip"));
            // 
            // Lbl_SumPassive
            // 
            resources.ApplyResources(this.Lbl_SumPassive, "Lbl_SumPassive");
            this.Lbl_SumPassive.Name = "Lbl_SumPassive";
            this.toolTip1.SetToolTip(this.Lbl_SumPassive, resources.GetString("Lbl_SumPassive.ToolTip"));
            // 
            // LayoutPan_passive
            // 
            resources.ApplyResources(this.LayoutPan_passive, "LayoutPan_passive");
            this.LayoutPan_passive.Controls.Add(this.TxtBox_left1, 1, 0);
            this.LayoutPan_passive.Controls.Add(this.ComBox_left1, 0, 0);
            this.LayoutPan_passive.Controls.Add(this.TxtBox_left2, 1, 1);
            this.LayoutPan_passive.Controls.Add(this.ComBox_left5, 0, 4);
            this.LayoutPan_passive.Controls.Add(this.TxtBox_left4, 1, 3);
            this.LayoutPan_passive.Controls.Add(this.TxtBox_left3, 1, 2);
            this.LayoutPan_passive.Controls.Add(this.TxtBox_left5, 1, 4);
            this.LayoutPan_passive.Controls.Add(this.ComBox_left4, 0, 3);
            this.LayoutPan_passive.Controls.Add(this.ComBox_left3, 0, 2);
            this.LayoutPan_passive.Controls.Add(this.ComBox_left2, 0, 1);
            this.LayoutPan_passive.Name = "LayoutPan_passive";
            // 
            // TxtBox_left1
            // 
            resources.ApplyResources(this.TxtBox_left1, "TxtBox_left1");
            this.TxtBox_left1.Name = "TxtBox_left1";
            // 
            // ComBox_left1
            // 
            resources.ApplyResources(this.ComBox_left1, "ComBox_left1");
            this.ComBox_left1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComBox_left1.FormattingEnabled = true;
            this.ComBox_left1.Name = "ComBox_left1";
            // 
            // TxtBox_left2
            // 
            resources.ApplyResources(this.TxtBox_left2, "TxtBox_left2");
            this.TxtBox_left2.Name = "TxtBox_left2";
            // 
            // ComBox_left5
            // 
            resources.ApplyResources(this.ComBox_left5, "ComBox_left5");
            this.ComBox_left5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComBox_left5.FormattingEnabled = true;
            this.ComBox_left5.Name = "ComBox_left5";
            // 
            // TxtBox_left4
            // 
            resources.ApplyResources(this.TxtBox_left4, "TxtBox_left4");
            this.TxtBox_left4.Name = "TxtBox_left4";
            // 
            // TxtBox_left3
            // 
            resources.ApplyResources(this.TxtBox_left3, "TxtBox_left3");
            this.TxtBox_left3.Name = "TxtBox_left3";
            // 
            // TxtBox_left5
            // 
            resources.ApplyResources(this.TxtBox_left5, "TxtBox_left5");
            this.TxtBox_left5.Name = "TxtBox_left5";
            // 
            // ComBox_left4
            // 
            resources.ApplyResources(this.ComBox_left4, "ComBox_left4");
            this.ComBox_left4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComBox_left4.FormattingEnabled = true;
            this.ComBox_left4.Name = "ComBox_left4";
            // 
            // ComBox_left3
            // 
            resources.ApplyResources(this.ComBox_left3, "ComBox_left3");
            this.ComBox_left3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComBox_left3.FormattingEnabled = true;
            this.ComBox_left3.Name = "ComBox_left3";
            // 
            // ComBox_left2
            // 
            resources.ApplyResources(this.ComBox_left2, "ComBox_left2");
            this.ComBox_left2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComBox_left2.FormattingEnabled = true;
            this.ComBox_left2.Name = "ComBox_left2";
            // 
            // Btn_LeftAdd
            // 
            resources.ApplyResources(this.Btn_LeftAdd, "Btn_LeftAdd");
            this.Btn_LeftAdd.Name = "Btn_LeftAdd";
            this.toolTip1.SetToolTip(this.Btn_LeftAdd, resources.GetString("Btn_LeftAdd.ToolTip"));
            this.Btn_LeftAdd.UseVisualStyleBackColor = true;
            this.Btn_LeftAdd.Click += new System.EventHandler(this.Btn_LeftAdd_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 8000;
            this.toolTip1.InitialDelay = 500;
            this.toolTip1.ReshowDelay = 100;
            // 
            // ChkListBox_right1
            // 
            resources.ApplyResources(this.ChkListBox_right1, "ChkListBox_right1");
            this.ChkListBox_right1.CheckOnClick = true;
            this.ChkListBox_right1.FormattingEnabled = true;
            this.ChkListBox_right1.Name = "ChkListBox_right1";
            this.toolTip1.SetToolTip(this.ChkListBox_right1, resources.GetString("ChkListBox_right1.ToolTip"));
            this.ChkListBox_right1.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ChkListBox_right1_ItemCheck);
            // 
            // NewRelativeTransaction
            // 
            this.AcceptButton = this.Btn_OK;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Btn_Cancel;
            this.Controls.Add(this.Grp_passive);
            this.Controls.Add(this.ChkBox_sendEmails);
            this.Controls.Add(this.Lbl_timeStamp);
            this.Controls.Add(this.Grp_active);
            this.Controls.Add(this.Lbl_showID);
            this.Controls.Add(this.Lbl_ID);
            this.Controls.Add(this.TxtBox_comment);
            this.Controls.Add(this.Lbl_comment);
            this.Controls.Add(this.TxtBox_category);
            this.Controls.Add(this.Lbl_category);
            this.Controls.Add(this.Dtp_timeStamp);
            this.Controls.Add(this.TxtBox_receiptNr);
            this.Controls.Add(this.Lbl_receiptNr);
            this.Controls.Add(this.Btn_Cancel);
            this.Controls.Add(this.Btn_OK);
            this.Name = "NewRelativeTransaction";
            this.Load += new System.EventHandler(this.NewRelativeTransaction_Load);
            this.Grp_active.ResumeLayout(false);
            this.Grp_active.PerformLayout();
            this.Grp_passive.ResumeLayout(false);
            this.LayoutPan_passive.ResumeLayout(false);
            this.LayoutPan_passive.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox TxtBox_interest;
        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.Button Btn_Cancel;
        private System.Windows.Forms.Label Lbl_receiptNr;
        private System.Windows.Forms.TextBox TxtBox_receiptNr;
        private System.Windows.Forms.DateTimePicker Dtp_timeStamp;
        private System.Windows.Forms.Label Lbl_category;
        private System.Windows.Forms.TextBox TxtBox_category;
        private System.Windows.Forms.Label Lbl_comment;
        private System.Windows.Forms.TextBox TxtBox_comment;
        private System.Windows.Forms.Label Lbl_ID;
        private System.Windows.Forms.Label Lbl_showID;
        private System.Windows.Forms.GroupBox Grp_active;
        private System.Windows.Forms.Label Lbl_timeStamp;
        private CheckedListBoxWithDivider ChkListBox_right1;
        private System.Windows.Forms.Label Lbl_threshold;
        private System.Windows.Forms.Label Lbl_interest;
        private System.Windows.Forms.TextBox TxtBox_threshold;
        private System.Windows.Forms.CheckBox ChkBox_sendEmails;
        private System.Windows.Forms.GroupBox Grp_passive;
        private System.Windows.Forms.Label Lbl_SumPassive;
        private System.Windows.Forms.TableLayoutPanel LayoutPan_passive;
        private System.Windows.Forms.TextBox TxtBox_left1;
        private System.Windows.Forms.ComboBox ComBox_left1;
        private System.Windows.Forms.TextBox TxtBox_left2;
        private System.Windows.Forms.ComboBox ComBox_left5;
        private System.Windows.Forms.TextBox TxtBox_left4;
        private System.Windows.Forms.TextBox TxtBox_left3;
        private System.Windows.Forms.TextBox TxtBox_left5;
        private System.Windows.Forms.ComboBox ComBox_left4;
        private System.Windows.Forms.ComboBox ComBox_left3;
        private System.Windows.Forms.ComboBox ComBox_left2;
        private System.Windows.Forms.Button Btn_LeftAdd;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}