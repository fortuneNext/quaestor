﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Quaestor.Logic;

namespace Quaestor.GUI
{
    public partial class EditAccount : Form
    {
        private readonly Account _account;

        public EditAccount()
        {
            InitializeComponent();
        }

        public EditAccount(Account account)
        {
            InitializeComponent();
            _account = account;

            //Tooltip when hovering over subbed count, would disappear above objects inside the panel
            toolTip_EditAcc.SetToolTip(panel1, strings.ToolTipAccSubbed);
            toolTip_EditAcc.SetToolTip(Lbl_countSubbed1, strings.ToolTipAccSubbed);
            toolTip_EditAcc.SetToolTip(Lbl_countSubbed2, strings.ToolTipAccSubbed);

        }

        private void RefreshEnabled()
        {
            TxtBox_email.Enabled = (Rbtn_user.Checked && !ChkBox_structuralAccount.Checked);
            Lbl_email.Enabled = TxtBox_email.Enabled;
            TxtBox_planValue.Enabled = !ChkBox_structuralAccount.Checked;
            Lbl_planValue.Enabled = !ChkBox_structuralAccount.Checked;
            ChkListBox_subaccounts.Enabled = ChkBox_structuralAccount.Checked;
            Lbl_subaccounts.Enabled = ChkListBox_subaccounts.Enabled;
        }

        private int CountSubOf()
        {
            int count = 0;
            foreach (Account account in Program.Logic.GetAccounts)
            {
                //Search in all structural accounts
                var structuralAccount = account as StructuralAccount;
                if (structuralAccount == null) continue;
                //count the times, the selected account is found in a subaccount
                foreach (Account subAccount in structuralAccount.Subaccounts)
                {
                    if (subAccount.Name == _account.Name)
                        count++;
                }
            }
            return count;
        }

        private void EditAccount_Load(object sender, EventArgs e)
        {
            Rbtn_active.Checked = true;
            foreach (Account account in Program.Logic.GetAccounts)
            {
                //Don't add Account to its own list (would cause error if selected)
                if (account == _account)
                    continue;
                ChkListBox_subaccounts.Items.Add(account.Name);
                var structuralAccount = _account as StructuralAccount;
                if (structuralAccount == null) continue;
                foreach (Account subAccount in structuralAccount.Subaccounts)
                {
                    if (subAccount.Name == account.Name)
                        ChkListBox_subaccounts.SetItemChecked(ChkListBox_subaccounts.Items.Count - 1, true);
                }
            }
            TxtBox_name.Text = _account.Name;
            Chkbox_printToOverview.Checked = _account.PrintToOverview;
            TxtBox_planValue.Text = (((double)(_account.GetPlanValue())) / 100).ToString(CultureInfo.InvariantCulture);

            switch (_account.AccountType)
            {
                case AccountTypes.Active:
                    Rbtn_active.Checked = true;
                    break;
                case AccountTypes.Passive:
                    Rbtn_passive.Checked = true;
                    break;
                case AccountTypes.User:
                    Rbtn_user.Checked = true;
                    break;
                default:
                    MessageBox.Show(strings.ErrorInvalidAccountType);
                    break;
            }
            ChkBox_structuralAccount.Checked = (_account is StructuralAccount);
            var userAccount = _account as UserAccount;
            if (userAccount != null)
                TxtBox_email.Text = userAccount.Email;

            //Show count of Accounts, this one is sub of
            Lbl_countSubbed2.Text = CountSubOf() + " Account";
            if (CountSubOf() > 1)
                Lbl_countSubbed2.Text += "s";


            RefreshEnabled();
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            //TODO NEW_ERA--
            //Set StartValues to zero

            if (ChkBox_structuralAccount.Checked)
            {
                List<Account> accounts =
                    (from object account in ChkListBox_subaccounts.CheckedItems
                        select Program.Logic.StrToAccount(account.ToString())).ToList();
                if (Rbtn_active.Checked)
                {
                    if (!Program.Logic.ReplaceWithStructuralAccount(_account.Name, TxtBox_name.Text.Trim(), accounts,
                            AccountTypes.Active, Chkbox_printToOverview.Checked))
                    {
                        MessageBox.Show(strings.ErrorEditFailed);
                        return;
                    }
                }
                else if (Rbtn_passive.Checked)
                {
                    if (!Program.Logic.ReplaceWithStructuralAccount(_account.Name, TxtBox_name.Text.Trim(), accounts,
                            AccountTypes.Passive, Chkbox_printToOverview.Checked))
                    {
                        MessageBox.Show(strings.ErrorEditFailed);
                        return;
                    }
                }
                else if (Rbtn_user.Checked)
                {
                    if (!Program.Logic.ReplaceWithStructuralAccount(_account.Name, TxtBox_name.Text.Trim(), accounts,
                            AccountTypes.User, Chkbox_printToOverview.Checked))
                    {
                        MessageBox.Show(strings.ErrorEditFailed);
                        return;
                    }
                }
            }
            else
            {
                if (Rbtn_active.Checked)
                {
                    if (!Program.Logic.ReplaceWithValueAccount(_account.Name,
                            MainForm.MoneyToInt(TxtBox_planValue.Text),
                            TxtBox_name.Text.Trim(), AccountTypes.Active, Chkbox_printToOverview.Checked))
                    {
                        MessageBox.Show(strings.ErrorEditFailed);
                        return;
                    }
                }
                else if (Rbtn_passive.Checked)
                {
                    if (!Program.Logic.ReplaceWithValueAccount(_account.Name,
                            MainForm.MoneyToInt(TxtBox_planValue.Text),
                            TxtBox_name.Text.Trim(), AccountTypes.Passive, Chkbox_printToOverview.Checked))
                    {
                        MessageBox.Show(strings.ErrorEditFailed);
                        return;
                    }
                }
                else if (Rbtn_user.Checked)
                {
                    if (!Program.Logic.ReplaceWithUserAccount(_account.Name,
                            MainForm.MoneyToInt(TxtBox_planValue.Text),
                            TxtBox_name.Text.Trim(), Chkbox_printToOverview.Checked, TxtBox_email.Text))
                    {
                        MessageBox.Show(strings.ErrorEditFailed);
                        return;
                    }
                }
            }

            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void ChkBox_structuralAccount_CheckedChanged(object sender, EventArgs e)
        {
            RefreshEnabled();
        }

        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Rbtn_user_CheckedChanged(object sender, EventArgs e)
        {
            RefreshEnabled();
        }

        private void TxtBox_email_Validating(object sender, CancelEventArgs e)
        {
            if (!(Logic.Logic.IsValidEmail(TxtBox_email.Text) || TxtBox_email.Text == ""))
                e.Cancel = true;
        }

        private void TxtBox_planValue_Validating(object sender, CancelEventArgs e)
        {
            TxtBox_planValue.Text = MainForm.CleanMoneyInput(TxtBox_planValue.Text);
        }

        private void TxtBox_name_Validating(object sender, CancelEventArgs e)
        {
            if (TxtBox_name.Text == "")
                e.Cancel = true;
            //so you cannot change the name to another existing one
            if (Program.Logic.GetAccounts.Any(account => account.Name == TxtBox_name.Text) && _account.Name != TxtBox_name.Text)
            {
                MessageBox.Show(strings.NameAlreadyInUse);
                e.Cancel = true;
            }
        }
    }
}