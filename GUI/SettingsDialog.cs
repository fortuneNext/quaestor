﻿using Quaestor.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Quaestor.GUI
{
    public partial class SettingsDialog : Form
    {

        public SettingsDialog()
        {
            InitializeComponent();
        }

        private void SettingsDialog_Load(object sender, EventArgs e)
        {
            //Initialize Text in Text boxes to previously saved values
            TxtBox_Server.Text = Settings.Default.SendMailServer;
            TxtBox_Port.Text = Settings.Default.SendMailPort.ToString();
            TxtBox_User.Text = Settings.Default.SendMailUsername;
            //Password is not shown to user
            TxtBox_Password.Text = "------";
            TxtBox_Sender.Text = Settings.Default.SendMailSenderAddress;
            TxtBox_Threads.Text = Settings.Default.SendMailClientCount.ToString();
            TxtBox_Subject.Text = Settings.Default.SendMailSubject;
        }

        private void SettingsDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Must reset inputs; if one first klicks "ok" when one input is not correct (i.e. empty)
            //other changes may be safed temporarily for the current session, but not permanently
            Settings.Default.Reload();
        }

        private void Btn_cancel_Click(object sender, EventArgs e) {
            //Must reset inputs; if one first klicks "ok" when one input is not correct (i.e. empty)
            //other changes may be safed temporarily for the current session, but not permanently
            Settings.Default.Reload();
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            int temp; //needed to convert strings to int

            //safe the text of the text boxes to the Settings file, if they are not empty
            //else ignore click
            if (TxtBox_Server.Text != "")
                Settings.Default.SendMailServer = TxtBox_Server.Text;
            else { return; }

            if (int.TryParse(TxtBox_Port.Text, out temp))
                Settings.Default.SendMailPort = temp;
            else { return; }

            if (TxtBox_User.Text != "")
                Settings.Default.SendMailUsername = TxtBox_User.Text;
            else { return; }

            //must check if new password was added, or textbox is not changed
            if (TxtBox_Password.Text != "")
            {
                if (TxtBox_Password.Text != "------")
                    Settings.Default.SendMailPassword = TxtBox_Password.Text;
#if DEBUG
                Console.WriteLine(@"Password is: " + Settings.Default.SendMailPassword);
#endif
            }
            else { return; }

            //always allow changes to the subject, sender and thread count
            if (TxtBox_Subject.Text != "")
                Settings.Default.SendMailSubject = TxtBox_Subject.Text;
            else { return; }

            if (TxtBox_Sender.Text != "")
                Settings.Default.SendMailSenderAddress = TxtBox_Sender.Text;
            else { return; }

            if (int.TryParse(TxtBox_Threads.Text, out temp))
                Settings.Default.SendMailClientCount = temp;
            else { return; }


            Settings.Default.Save();

            this.Close();
        }

        private void TxtBox_Password_Click(object sender, EventArgs e)
        {
            TxtBox_Password.SelectAll();
        }

        private void TxtBox_Password_TextChanged(object sender, EventArgs e)
        {
            if (TxtBox_Password.Text.Equals(@"bierjunge"))
                MessageBox.Show(@"Hängt, schärf auf!");
        }
    }
}
