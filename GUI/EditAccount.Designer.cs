﻿namespace Quaestor.GUI
{
    partial class EditAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditAccount));
            this.Btn_OK = new System.Windows.Forms.Button();
            this.Btn_Cancel = new System.Windows.Forms.Button();
            this.TxtBox_name = new System.Windows.Forms.TextBox();
            this.Lbl_name = new System.Windows.Forms.Label();
            this.TxtBox_email = new System.Windows.Forms.TextBox();
            this.Lbl_email = new System.Windows.Forms.Label();
            this.Chkbox_printToOverview = new System.Windows.Forms.CheckBox();
            this.Rbtn_active = new System.Windows.Forms.RadioButton();
            this.Rbtn_passive = new System.Windows.Forms.RadioButton();
            this.Rbtn_user = new System.Windows.Forms.RadioButton();
            this.Lbl_subaccounts = new System.Windows.Forms.Label();
            this.ChkListBox_subaccounts = new System.Windows.Forms.CheckedListBox();
            this.ChkBox_structuralAccount = new System.Windows.Forms.CheckBox();
            this.Grp_accountType = new System.Windows.Forms.GroupBox();
            this.Lbl_planValue = new System.Windows.Forms.Label();
            this.TxtBox_planValue = new System.Windows.Forms.TextBox();
            this.toolTip_EditAcc = new System.Windows.Forms.ToolTip(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.Lbl_countSubbed2 = new System.Windows.Forms.Label();
            this.Lbl_countSubbed1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.Grp_accountType.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Btn_OK
            // 
            this.Btn_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_OK.Location = new System.Drawing.Point(12, 396);
            this.Btn_OK.Name = "Btn_OK";
            this.Btn_OK.Size = new System.Drawing.Size(136, 23);
            this.Btn_OK.TabIndex = 16;
            this.Btn_OK.Text = "OK";
            this.Btn_OK.UseVisualStyleBackColor = true;
            this.Btn_OK.Click += new System.EventHandler(this.Btn_OK_Click);
            // 
            // Btn_Cancel
            // 
            this.Btn_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Cancel.CausesValidation = false;
            this.Btn_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Btn_Cancel.Location = new System.Drawing.Point(154, 396);
            this.Btn_Cancel.Name = "Btn_Cancel";
            this.Btn_Cancel.Size = new System.Drawing.Size(136, 23);
            this.Btn_Cancel.TabIndex = 17;
            this.Btn_Cancel.Text = "Abbrechen";
            this.Btn_Cancel.UseVisualStyleBackColor = true;
            this.Btn_Cancel.Click += new System.EventHandler(this.Btn_Cancel_Click);
            // 
            // TxtBox_name
            // 
            this.TxtBox_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtBox_name.Location = new System.Drawing.Point(154, 6);
            this.TxtBox_name.Name = "TxtBox_name";
            this.TxtBox_name.Size = new System.Drawing.Size(136, 20);
            this.TxtBox_name.TabIndex = 0;
            this.TxtBox_name.Validating += new System.ComponentModel.CancelEventHandler(this.TxtBox_name_Validating);
            // 
            // Lbl_name
            // 
            this.Lbl_name.AutoSize = true;
            this.Lbl_name.Location = new System.Drawing.Point(12, 9);
            this.Lbl_name.Name = "Lbl_name";
            this.Lbl_name.Size = new System.Drawing.Size(35, 13);
            this.Lbl_name.TabIndex = 0;
            this.Lbl_name.Text = "Name";
            // 
            // TxtBox_email
            // 
            this.TxtBox_email.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtBox_email.Enabled = false;
            this.TxtBox_email.Location = new System.Drawing.Point(154, 111);
            this.TxtBox_email.Name = "TxtBox_email";
            this.TxtBox_email.Size = new System.Drawing.Size(136, 20);
            this.TxtBox_email.TabIndex = 9;
            this.TxtBox_email.Validating += new System.ComponentModel.CancelEventHandler(this.TxtBox_email_Validating);
            // 
            // Lbl_email
            // 
            this.Lbl_email.AutoSize = true;
            this.Lbl_email.Enabled = false;
            this.Lbl_email.Location = new System.Drawing.Point(12, 114);
            this.Lbl_email.Name = "Lbl_email";
            this.Lbl_email.Size = new System.Drawing.Size(32, 13);
            this.Lbl_email.TabIndex = 8;
            this.Lbl_email.Text = "Email";
            // 
            // Chkbox_printToOverview
            // 
            this.Chkbox_printToOverview.AutoSize = true;
            this.Chkbox_printToOverview.Location = new System.Drawing.Point(154, 32);
            this.Chkbox_printToOverview.Name = "Chkbox_printToOverview";
            this.Chkbox_printToOverview.Size = new System.Drawing.Size(136, 17);
            this.Chkbox_printToOverview.TabIndex = 3;
            this.Chkbox_printToOverview.Text = "Auf Übersicht anzeigen";
            this.toolTip_EditAcc.SetToolTip(this.Chkbox_printToOverview, "Soll das Konto auf der Übersicht angezeigt werden");
            this.Chkbox_printToOverview.UseVisualStyleBackColor = true;
            // 
            // Rbtn_active
            // 
            this.Rbtn_active.AutoSize = true;
            this.Rbtn_active.Location = new System.Drawing.Point(6, 19);
            this.Rbtn_active.Name = "Rbtn_active";
            this.Rbtn_active.Size = new System.Drawing.Size(49, 17);
            this.Rbtn_active.TabIndex = 5;
            this.Rbtn_active.TabStop = true;
            this.Rbtn_active.Text = "Aktiv";
            this.Rbtn_active.UseVisualStyleBackColor = true;
            // 
            // Rbtn_passive
            // 
            this.Rbtn_passive.AutoSize = true;
            this.Rbtn_passive.Location = new System.Drawing.Point(105, 19);
            this.Rbtn_passive.Name = "Rbtn_passive";
            this.Rbtn_passive.Size = new System.Drawing.Size(56, 17);
            this.Rbtn_passive.TabIndex = 6;
            this.Rbtn_passive.TabStop = true;
            this.Rbtn_passive.Text = "Passiv";
            this.Rbtn_passive.UseVisualStyleBackColor = true;
            // 
            // Rbtn_user
            // 
            this.Rbtn_user.AutoSize = true;
            this.Rbtn_user.Location = new System.Drawing.Point(205, 19);
            this.Rbtn_user.Name = "Rbtn_user";
            this.Rbtn_user.Size = new System.Drawing.Size(67, 17);
            this.Rbtn_user.TabIndex = 7;
            this.Rbtn_user.TabStop = true;
            this.Rbtn_user.Text = "Benutzer";
            this.Rbtn_user.UseVisualStyleBackColor = true;
            this.Rbtn_user.CheckedChanged += new System.EventHandler(this.Rbtn_user_CheckedChanged);
            // 
            // Lbl_subaccounts
            // 
            this.Lbl_subaccounts.AutoSize = true;
            this.Lbl_subaccounts.Enabled = false;
            this.Lbl_subaccounts.Location = new System.Drawing.Point(12, 163);
            this.Lbl_subaccounts.Name = "Lbl_subaccounts";
            this.Lbl_subaccounts.Size = new System.Drawing.Size(66, 13);
            this.Lbl_subaccounts.TabIndex = 14;
            this.Lbl_subaccounts.Text = "Unterkonten";
            // 
            // ChkListBox_subaccounts
            // 
            this.ChkListBox_subaccounts.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ChkListBox_subaccounts.CheckOnClick = true;
            this.ChkListBox_subaccounts.Enabled = false;
            this.ChkListBox_subaccounts.FormattingEnabled = true;
            this.ChkListBox_subaccounts.Location = new System.Drawing.Point(154, 163);
            this.ChkListBox_subaccounts.Name = "ChkListBox_subaccounts";
            this.ChkListBox_subaccounts.Size = new System.Drawing.Size(136, 229);
            this.ChkListBox_subaccounts.TabIndex = 15;
            this.toolTip_EditAcc.SetToolTip(this.ChkListBox_subaccounts, "Die Unterkonten, wenn es sich um ein Strukturkonto handelt");
            // 
            // ChkBox_structuralAccount
            // 
            this.ChkBox_structuralAccount.AutoSize = true;
            this.ChkBox_structuralAccount.Location = new System.Drawing.Point(15, 32);
            this.ChkBox_structuralAccount.Name = "ChkBox_structuralAccount";
            this.ChkBox_structuralAccount.Size = new System.Drawing.Size(90, 17);
            this.ChkBox_structuralAccount.TabIndex = 2;
            this.ChkBox_structuralAccount.Text = "Strukturkonto";
            this.toolTip_EditAcc.SetToolTip(this.ChkBox_structuralAccount, "Handelt es sich um ein Konto mit Unterkonten, ohne eigene Werte");
            this.ChkBox_structuralAccount.UseVisualStyleBackColor = true;
            this.ChkBox_structuralAccount.CheckedChanged += new System.EventHandler(this.ChkBox_structuralAccount_CheckedChanged);
            // 
            // Grp_accountType
            // 
            this.Grp_accountType.Controls.Add(this.Rbtn_active);
            this.Grp_accountType.Controls.Add(this.Rbtn_passive);
            this.Grp_accountType.Controls.Add(this.Rbtn_user);
            this.Grp_accountType.Location = new System.Drawing.Point(12, 55);
            this.Grp_accountType.Name = "Grp_accountType";
            this.Grp_accountType.Size = new System.Drawing.Size(278, 50);
            this.Grp_accountType.TabIndex = 4;
            this.Grp_accountType.TabStop = false;
            this.Grp_accountType.Text = "Kontotyp";
            this.toolTip_EditAcc.SetToolTip(this.Grp_accountType, "Art des Kontos");
            // 
            // Lbl_planValue
            // 
            this.Lbl_planValue.AutoSize = true;
            this.Lbl_planValue.Location = new System.Drawing.Point(12, 140);
            this.Lbl_planValue.Name = "Lbl_planValue";
            this.Lbl_planValue.Size = new System.Drawing.Size(74, 13);
            this.Lbl_planValue.TabIndex = 12;
            this.Lbl_planValue.Text = "Haushaltsplan";
            this.toolTip_EditAcc.SetToolTip(this.Lbl_planValue, "Der prognostizierte Endwert im Semester");
            // 
            // TxtBox_planValue
            // 
            this.TxtBox_planValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtBox_planValue.Location = new System.Drawing.Point(154, 137);
            this.TxtBox_planValue.Name = "TxtBox_planValue";
            this.TxtBox_planValue.Size = new System.Drawing.Size(136, 20);
            this.TxtBox_planValue.TabIndex = 13;
            this.TxtBox_planValue.Text = "0";
            this.toolTip_EditAcc.SetToolTip(this.TxtBox_planValue, "Der prognostizierte Endwert im Semester");
            this.TxtBox_planValue.Validating += new System.ComponentModel.CancelEventHandler(this.TxtBox_planValue_Validating);
            // 
            // toolTip_EditAcc
            // 
            this.toolTip_EditAcc.AutoPopDelay = 8000;
            this.toolTip_EditAcc.InitialDelay = 500;
            this.toolTip_EditAcc.ReshowDelay = 100;
            this.toolTip_EditAcc.ShowAlways = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Lbl_countSubbed2);
            this.panel1.Controls.Add(this.Lbl_countSubbed1);
            this.panel1.Location = new System.Drawing.Point(11, 213);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(132, 33);
            this.panel1.TabIndex = 1;
            // 
            // Lbl_countSubbed2
            // 
            this.Lbl_countSubbed2.AutoSize = true;
            this.Lbl_countSubbed2.Location = new System.Drawing.Point(0, 19);
            this.Lbl_countSubbed2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lbl_countSubbed2.Name = "Lbl_countSubbed2";
            this.Lbl_countSubbed2.Size = new System.Drawing.Size(62, 13);
            this.Lbl_countSubbed2.TabIndex = 20;
            this.Lbl_countSubbed2.Text = "X Accounts";
            // 
            // Lbl_countSubbed1
            // 
            this.Lbl_countSubbed1.AutoSize = true;
            this.Lbl_countSubbed1.Location = new System.Drawing.Point(0, 3);
            this.Lbl_countSubbed1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Lbl_countSubbed1.Name = "Lbl_countSubbed1";
            this.Lbl_countSubbed1.Size = new System.Drawing.Size(133, 13);
            this.Lbl_countSubbed1.TabIndex = 19;
            this.Lbl_countSubbed1.Text = "Dieser Account is Sub von";
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button2.Location = new System.Drawing.Point(32, 713);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(136, 23);
            this.button2.TabIndex = 18;
            this.button2.Text = "OK";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // EditAccount
            // 
            this.AcceptButton = this.Btn_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Btn_Cancel;
            this.ClientSize = new System.Drawing.Size(304, 431);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.TxtBox_planValue);
            this.Controls.Add(this.Lbl_planValue);
            this.Controls.Add(this.Grp_accountType);
            this.Controls.Add(this.Lbl_subaccounts);
            this.Controls.Add(this.ChkListBox_subaccounts);
            this.Controls.Add(this.ChkBox_structuralAccount);
            this.Controls.Add(this.Chkbox_printToOverview);
            this.Controls.Add(this.Lbl_email);
            this.Controls.Add(this.TxtBox_email);
            this.Controls.Add(this.Lbl_name);
            this.Controls.Add(this.TxtBox_name);
            this.Controls.Add(this.Btn_Cancel);
            this.Controls.Add(this.Btn_OK);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(320, 343);
            this.Name = "EditAccount";
            this.Text = "Konto bearbeiten";
            this.Load += new System.EventHandler(this.EditAccount_Load);
            this.Grp_accountType.ResumeLayout(false);
            this.Grp_accountType.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_OK;
        private System.Windows.Forms.Button Btn_Cancel;
        private System.Windows.Forms.TextBox TxtBox_name;
        private System.Windows.Forms.Label Lbl_name;
        private System.Windows.Forms.TextBox TxtBox_email;
        private System.Windows.Forms.Label Lbl_email;
        private System.Windows.Forms.CheckBox Chkbox_printToOverview;
        private System.Windows.Forms.RadioButton Rbtn_active;
        private System.Windows.Forms.RadioButton Rbtn_passive;
        private System.Windows.Forms.RadioButton Rbtn_user;
        private System.Windows.Forms.Label Lbl_subaccounts;
        private System.Windows.Forms.CheckedListBox ChkListBox_subaccounts;
        private System.Windows.Forms.CheckBox ChkBox_structuralAccount;
        private System.Windows.Forms.GroupBox Grp_accountType;
        private System.Windows.Forms.Label Lbl_planValue;
        private System.Windows.Forms.TextBox TxtBox_planValue;
        private System.Windows.Forms.ToolTip toolTip_EditAcc;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label Lbl_countSubbed2;
        private System.Windows.Forms.Label Lbl_countSubbed1;
        private System.Windows.Forms.Button button2;
    }
}