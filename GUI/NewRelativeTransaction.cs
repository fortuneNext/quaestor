﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Quaestor.Logic;
using Quaestor.Properties;

namespace Quaestor.GUI
{
    public partial class NewRelativeTransaction : Form
    {

        private readonly List<ComboBox> _leftComBoxes;
        private readonly List<TextBox> _leftTxtBoxes;

        //used so TxtBox_interest_Enter event only activates if not already in the textbox
        private bool interestValidatingFinished = true;

        public NewRelativeTransaction()
        {
            InitializeComponent();
            _leftComBoxes = new List<ComboBox> { ComBox_left1, ComBox_left2, ComBox_left3, ComBox_left4, ComBox_left5 };

            _leftTxtBoxes = new List<TextBox> { TxtBox_left1, TxtBox_left2, TxtBox_left3, TxtBox_left4, TxtBox_left5 };

            foreach (TextBox txtBox in _leftTxtBoxes)
                txtBox.Validating += txtBox_value_Validator;

            //so scrollbars are the correct way in the panels
            LayoutPan_passive.Padding = new Padding(0, 0, SystemInformation.VerticalScrollBarWidth, 0);
        }

        private void ActualizeSum()
        {
            int allInterests = 0;

            foreach (object account in ChkListBox_right1.CheckedItems)
            {
                try
                {
                    int interest =
                        Program.Logic.GetInterest((UserAccount) Program.Logic.StrToAccount(account.ToString()),
                            Convert.ToInt32(TxtBox_threshold.Text),
                            Convert.ToSingle(convertPercentage(false), CultureInfo.InvariantCulture.NumberFormat));
                    allInterests += interest;
                }
                catch
                {
                    return;
                }
            }

            TxtBox_left1.Text = ((double)allInterests / 100.0).ToString(CultureInfo.InvariantCulture);
            TxtBox_left1.Text = MainForm.CleanMoneyInput(TxtBox_left1.Text);
            //actuallize sum
            Lbl_SumPassive.Text = _leftTxtBoxes.Sum(TxtBox => Convert.ToInt32(MainForm.MoneyToInt(TxtBox.Text)) / 100.0).ToString("C");
        }

        private void NewRelativeTransaction_Load(object sender, EventArgs e)
        {
            // Fülle Comboboxes mit Accounts, erstes leerer String
            foreach (ComboBox comBox in _leftComBoxes)
            {
                comBox.Items.Add("");
            }
            // Iteriere durch alle Accounts
            foreach (ValueAccount account in Program.Logic.GetAccounts.OfType<ValueAccount>())
            {
                //LegacyAccount, which replaces Start Values cannot be modified by the user
                //left side, add passive accounts that are not Structural accounts
                if (account.AccountType == AccountTypes.Passive && account.Name != Settings.Default.LegacyAccount)
                {
                    // Fülle jede Combobox (in Listen)
                    foreach (ComboBox comBox in _leftComBoxes)
                        comBox.Items.Add(account.Name);
                }
            }

            //Right side, add User Accounts sorted by Structural Accounts
            //for each structural User account
            foreach (StructuralAccount account in Program.Logic.GetAccounts.OfType<StructuralAccount>().Where(account => (account.AccountType == AccountTypes.User)))
            {
                //Add headers to devide the  users by structual User Account
                ChkListBox_right1.Items.Add(@"-----" + account.Name + @"-----");
                ChkListBox_right1.SetDivider(ChkListBox_right1.Items.Count - 1, true);
                foreach (UserAccount sub in account.Subaccounts.Where(
                        sub => (sub is UserAccount)))
                    ChkListBox_right1.Items.Add(sub.Name);
            }
            Lbl_showID.Text = Program.Logic.GetNewTransactionId().ToString(CultureInfo.InvariantCulture);
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            int receiptNr;
            Int32.TryParse(TxtBox_receiptNr.Text, out receiptNr);
            int threshold;
            Int32.TryParse(TxtBox_threshold.Text, out threshold);
            Single interest;
            Single.TryParse(convertPercentage(false), NumberStyles.Float, CultureInfo.InvariantCulture.NumberFormat, out interest);

            var accounts = new List<ValueAccount>();
            var values = new List<int>();
            var emailAccounts = new List<string>();

            for (int i = 0; i < _leftComBoxes.Count; i++)
            {
                // Falls Combox nicht leer
                if (_leftComBoxes[i].Text == "" || _leftTxtBoxes[i].Text == "0" || _leftTxtBoxes[i].Text == "0.00") continue;
                // Korrigiere Zahlen
                _leftTxtBoxes[i].Text = MainForm.CleanMoneyInput(_leftTxtBoxes[i].Text);
                // Füge Eintrag hinzu
                accounts.Add((ValueAccount)Program.Logic.StrToAccount(_leftComBoxes[i].Text));
                values.Add(Convert.ToInt32(MainForm.MoneyToInt(_leftTxtBoxes[i].Text)));
            }

            foreach (Object account in ChkListBox_right1.CheckedItems)
            {
                if (Program.Logic.StrToAccount(account.ToString()).GetValue() != 0)
                    emailAccounts.Add(account.ToString());
                int value = Program.Logic.GetInterest((UserAccount) Program.Logic.StrToAccount(account.ToString()),
                    threshold, interest);
                if (value <= 0) continue;
                accounts.Add((ValueAccount) Program.Logic.StrToAccount(account.ToString()));
                values.Add(value);
            }

            if (Program.Logic.NewTransaction(accounts, values, Dtp_timeStamp.Value, receiptNr, TxtBox_category.Text,
                TxtBox_comment.Text))
            {
                if (ChkBox_sendEmails.Checked)
                    Program.Logic.SendEmailToUsers(emailAccounts);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
                MessageBox.Show(strings.InvalidTransaction);
        }

        /// <summary>
        /// reads value from interest TextBox, convers it and returns result string
        /// </summary>
        /// <param name="toPercentage">true to convert value to percentage, false to convert percentage to value</param>
        /// <returns>string representing numerical value as Percentage if true, else Percentage as numerical value</returns>
        private string convertPercentage(bool toPercentage)
        {
            string text = TxtBox_interest.Text;
            Single interest;

            if (toPercentage)
            {
                if (!text.Contains(@"%"))
                {
                    Single.TryParse(text, NumberStyles.Float, CultureInfo.InvariantCulture.NumberFormat, out interest);
                    text = (interest * 100).ToString() + @"%";
                }
                else
                    text = TxtBox_interest.Text;
            }
            else
            {
                if (text.Contains(@"%"))
                {
                    text = (Convert.ToSingle(text.Replace(@"%", "")) / 100.0).ToString().Replace(",", ".");
                }
                else
                    text = TxtBox_interest.Text;
            }
            return text;
        }

        private void ComboBoxLeft_Add()
        {
            // Init new Combobox
            var newComBox = new ComboBox
            {
                Dock = DockStyle.Fill,
                DropDownStyle = ComboBoxStyle.DropDownList,
                Name = "ComBox_left" + (_leftComBoxes.Count + 1)
            };
            foreach (object item in ComBox_left1.Items)
                newComBox.Items.Add(item);
            LayoutPan_passive.Controls.Add(newComBox, 0, _leftComBoxes.Count + 1);
            _leftComBoxes.Add(newComBox);

            // Init new Textbox
            var newTxtBox = new TextBox
            {
                Dock = DockStyle.Fill,
                Text = @"0",
                Name = "TxtBox_left" + (_leftTxtBoxes.Count + 1)
            };
            LayoutPan_passive.Controls.Add(newTxtBox, 1, _leftTxtBoxes.Count + 1);
            _leftTxtBoxes.Add(newTxtBox);
            newTxtBox.Validating += txtBox_value_Validator;

        }

        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void TxtBox_interest_TextChanged(object sender, EventArgs e)
        {
            ActualizeSum();
        }

        private void TxtBox_threshold_TextChanged(object sender, EventArgs e)
        {
            ActualizeSum();
        }

        private void TxtBox_receiptNr_Validating(object sender, CancelEventArgs e)
        {
            int receiptNr;
            if (!(Int32.TryParse(TxtBox_receiptNr.Text, out receiptNr)))
                e.Cancel = true;
        }

        private void TxtBox_threshold_Validating(object sender, CancelEventArgs e)
        {
            int threshold;
            if (!(Int32.TryParse(TxtBox_threshold.Text, out threshold)))
                e.Cancel = true;
        }

        private void TxtBox_interest_Validating(object sender, CancelEventArgs e)
        {
            Single interest;
            TxtBox_interest.Text = TxtBox_interest.Text.Replace(',', '.');
            if (!(Single.TryParse(TxtBox_interest.Text, NumberStyles.Float, CultureInfo.InvariantCulture.NumberFormat,
                    out interest)))
                e.Cancel = true;
            else //Change Text from number to percentage
            {
                TxtBox_interest.Text = convertPercentage(true);
                interestValidatingFinished = true;
            }
        }

        private void txtBox_value_Validator(object sender, CancelEventArgs e)
        {
            ((TextBox)sender).Text = MainForm.CleanMoneyInput(((TextBox)sender).Text);
            Lbl_SumPassive.Text = _leftTxtBoxes.Sum(TxtBox => Convert.ToInt32(MainForm.MoneyToInt(TxtBox.Text)) / 100.0).ToString("C");
        }

        private void Btn_LeftAdd_Click(object sender, EventArgs e)
        {
            ComboBoxLeft_Add();
        }

        private void ChkListBox_right1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            //ItemCheck is invoked before the item is checked; BeginInvoke starts Method after item is checked
            this.BeginInvoke((MethodInvoker)delegate { ActualizeSum(); });
        }

        private void TxtBox_interest_Enter(object sender, EventArgs e)
        {
            if(interestValidatingFinished)
            {
                TxtBox_interest.Text = convertPercentage(false);
                interestValidatingFinished = false;
            }
        }
    }
}