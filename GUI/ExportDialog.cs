﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Quaestor.Logic;

namespace Quaestor.GUI
{
    public partial class ExportDialog : Form
    {
        public List<Account> StructAccounts;

        public ExportDialog()
        {
            InitializeComponent();
        }

        private void ExportDialog_Load(object sender, EventArgs e)
        {
            StructAccounts = new List<Account>(Program.Logic.GetAccounts.Where(
                        account => (account.AccountType is AccountTypes.User) && (account is StructuralAccount)));

            foreach(Account account in StructAccounts)
            {
                ChkListBox_StructAccounts.Items.Add(account.Name);
                ChkListBox_StructAccounts.SetItemChecked(ChkListBox_StructAccounts.Items.Count - 1, true);
            }
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            List<Account> tempAccounts = new List<Account>(StructAccounts);//cannot change a list that is used in foreach
            foreach (Account account in tempAccounts)
            {
                if (!ChkListBox_StructAccounts.CheckedItems.Contains(account.Name))
                    StructAccounts.Remove(account);
            }

            Close();
        }
    }
}
