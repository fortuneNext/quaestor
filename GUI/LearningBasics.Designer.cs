﻿namespace Quaestor.GUI
{
    partial class LearningBasics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LearningBasics));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.showButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.backgroundButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.Grp_passive = new System.Windows.Forms.GroupBox();
            this.Lbl_SumActive = new System.Windows.Forms.Label();
            this.LayoutPan_passive = new System.Windows.Forms.TableLayoutPanel();
            this.TxtBox_left1 = new System.Windows.Forms.TextBox();
            this.ComBox_left1 = new System.Windows.Forms.ComboBox();
            this.TxtBox_left2 = new System.Windows.Forms.TextBox();
            this.ComBox_left5 = new System.Windows.Forms.ComboBox();
            this.TxtBox_left4 = new System.Windows.Forms.TextBox();
            this.TxtBox_left3 = new System.Windows.Forms.TextBox();
            this.TxtBox_left5 = new System.Windows.Forms.TextBox();
            this.ComBox_left4 = new System.Windows.Forms.ComboBox();
            this.ComBox_left3 = new System.Windows.Forms.ComboBox();
            this.ComBox_left2 = new System.Windows.Forms.ComboBox();
            this.Btn_leftAdd = new System.Windows.Forms.Button();
            this.But_Test = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.Grp_passive.SuspendLayout();
            this.LayoutPan_passive.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.checkBox1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(535, 430);
            this.tableLayoutPanel1.TabIndex = 0;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tableLayoutPanel1.SetColumnSpan(this.pictureBox1, 2);
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(529, 381);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(3, 390);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(60, 17);
            this.checkBox1.TabIndex = 1;
            this.checkBox1.Text = "Stretch";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.showButton);
            this.flowLayoutPanel1.Controls.Add(this.clearButton);
            this.flowLayoutPanel1.Controls.Add(this.backgroundButton);
            this.flowLayoutPanel1.Controls.Add(this.closeButton);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(83, 390);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(449, 37);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // showButton
            // 
            this.showButton.AutoSize = true;
            this.showButton.Location = new System.Drawing.Point(366, 3);
            this.showButton.Name = "showButton";
            this.showButton.Size = new System.Drawing.Size(80, 23);
            this.showButton.TabIndex = 0;
            this.showButton.Text = "Bild anzeigen";
            this.showButton.UseVisualStyleBackColor = true;
            this.showButton.Click += new System.EventHandler(this.showButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.AutoSize = true;
            this.clearButton.Location = new System.Drawing.Point(285, 3);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(75, 23);
            this.clearButton.TabIndex = 1;
            this.clearButton.Text = "Bild löschen";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // backgroundButton
            // 
            this.backgroundButton.AutoSize = true;
            this.backgroundButton.Location = new System.Drawing.Point(137, 3);
            this.backgroundButton.Name = "backgroundButton";
            this.backgroundButton.Size = new System.Drawing.Size(142, 23);
            this.backgroundButton.TabIndex = 2;
            this.backgroundButton.Text = "Hintergrundfarbe festlegen";
            this.backgroundButton.UseVisualStyleBackColor = true;
            this.backgroundButton.Click += new System.EventHandler(this.backgroundButton_Click);
            // 
            // closeButton
            // 
            this.closeButton.AutoSize = true;
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton.Location = new System.Drawing.Point(56, 3);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 3;
            this.closeButton.Text = "Schließen";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "JPEG Files (*.jpg)|*.jpg|PNG Files (*.png)|*.png|BMP Files (*.bmp)|*.bmp|All file" +
    "s (*.*)|*.*";
            this.openFileDialog1.Title = "Select a picture file";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(563, 232);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(272, 186);
            this.listBox1.TabIndex = 59;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // Grp_passive
            // 
            this.Grp_passive.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Grp_passive.Controls.Add(this.Lbl_SumActive);
            this.Grp_passive.Controls.Add(this.LayoutPan_passive);
            this.Grp_passive.Controls.Add(this.Btn_leftAdd);
            this.Grp_passive.Location = new System.Drawing.Point(807, 29);
            this.Grp_passive.Name = "Grp_passive";
            this.Grp_passive.Size = new System.Drawing.Size(272, 186);
            this.Grp_passive.TabIndex = 60;
            this.Grp_passive.TabStop = false;
            this.Grp_passive.Text = "Passiva";
            // 
            // Lbl_SumActive
            // 
            this.Lbl_SumActive.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Lbl_SumActive.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.Lbl_SumActive.Location = new System.Drawing.Point(9, 159);
            this.Lbl_SumActive.Name = "Lbl_SumActive";
            this.Lbl_SumActive.Size = new System.Drawing.Size(254, 17);
            this.Lbl_SumActive.TabIndex = 57;
            this.Lbl_SumActive.Text = "0.00 €";
            this.Lbl_SumActive.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LayoutPan_passive
            // 
            this.LayoutPan_passive.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LayoutPan_passive.AutoScroll = true;
            this.LayoutPan_passive.ColumnCount = 2;
            this.LayoutPan_passive.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.LayoutPan_passive.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.LayoutPan_passive.Controls.Add(this.TxtBox_left1, 1, 0);
            this.LayoutPan_passive.Controls.Add(this.ComBox_left1, 0, 0);
            this.LayoutPan_passive.Controls.Add(this.TxtBox_left2, 1, 1);
            this.LayoutPan_passive.Controls.Add(this.ComBox_left5, 0, 4);
            this.LayoutPan_passive.Controls.Add(this.TxtBox_left4, 1, 3);
            this.LayoutPan_passive.Controls.Add(this.TxtBox_left3, 1, 2);
            this.LayoutPan_passive.Controls.Add(this.TxtBox_left5, 1, 4);
            this.LayoutPan_passive.Controls.Add(this.ComBox_left4, 0, 3);
            this.LayoutPan_passive.Controls.Add(this.ComBox_left3, 0, 2);
            this.LayoutPan_passive.Controls.Add(this.ComBox_left2, 0, 1);
            this.LayoutPan_passive.Location = new System.Drawing.Point(6, 16);
            this.LayoutPan_passive.Name = "LayoutPan_passive";
            this.LayoutPan_passive.RowCount = 5;
            this.LayoutPan_passive.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.LayoutPan_passive.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.LayoutPan_passive.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.LayoutPan_passive.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.LayoutPan_passive.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.LayoutPan_passive.Size = new System.Drawing.Size(260, 113);
            this.LayoutPan_passive.TabIndex = 56;
            // 
            // TxtBox_left1
            // 
            this.TxtBox_left1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TxtBox_left1.Location = new System.Drawing.Point(159, 3);
            this.TxtBox_left1.Name = "TxtBox_left1";
            this.TxtBox_left1.Size = new System.Drawing.Size(98, 20);
            this.TxtBox_left1.TabIndex = 47;
            this.TxtBox_left1.Text = "0";
            // 
            // ComBox_left1
            // 
            this.ComBox_left1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComBox_left1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComBox_left1.FormattingEnabled = true;
            this.ComBox_left1.Location = new System.Drawing.Point(3, 3);
            this.ComBox_left1.Name = "ComBox_left1";
            this.ComBox_left1.Size = new System.Drawing.Size(150, 21);
            this.ComBox_left1.TabIndex = 46;
            // 
            // TxtBox_left2
            // 
            this.TxtBox_left2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TxtBox_left2.Location = new System.Drawing.Point(159, 30);
            this.TxtBox_left2.Name = "TxtBox_left2";
            this.TxtBox_left2.Size = new System.Drawing.Size(98, 20);
            this.TxtBox_left2.TabIndex = 48;
            this.TxtBox_left2.Text = "0";
            // 
            // ComBox_left5
            // 
            this.ComBox_left5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComBox_left5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComBox_left5.FormattingEnabled = true;
            this.ComBox_left5.Location = new System.Drawing.Point(3, 111);
            this.ComBox_left5.Name = "ComBox_left5";
            this.ComBox_left5.Size = new System.Drawing.Size(150, 21);
            this.ComBox_left5.TabIndex = 53;
            // 
            // TxtBox_left4
            // 
            this.TxtBox_left4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TxtBox_left4.Location = new System.Drawing.Point(159, 84);
            this.TxtBox_left4.Name = "TxtBox_left4";
            this.TxtBox_left4.Size = new System.Drawing.Size(98, 20);
            this.TxtBox_left4.TabIndex = 52;
            this.TxtBox_left4.Text = "0";
            // 
            // TxtBox_left3
            // 
            this.TxtBox_left3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TxtBox_left3.Location = new System.Drawing.Point(159, 57);
            this.TxtBox_left3.Name = "TxtBox_left3";
            this.TxtBox_left3.Size = new System.Drawing.Size(98, 20);
            this.TxtBox_left3.TabIndex = 50;
            this.TxtBox_left3.Text = "0";
            // 
            // TxtBox_left5
            // 
            this.TxtBox_left5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtBox_left5.Location = new System.Drawing.Point(159, 111);
            this.TxtBox_left5.Name = "TxtBox_left5";
            this.TxtBox_left5.Size = new System.Drawing.Size(98, 20);
            this.TxtBox_left5.TabIndex = 54;
            this.TxtBox_left5.Text = "0";
            // 
            // ComBox_left4
            // 
            this.ComBox_left4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComBox_left4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComBox_left4.FormattingEnabled = true;
            this.ComBox_left4.Location = new System.Drawing.Point(3, 84);
            this.ComBox_left4.Name = "ComBox_left4";
            this.ComBox_left4.Size = new System.Drawing.Size(150, 21);
            this.ComBox_left4.TabIndex = 51;
            // 
            // ComBox_left3
            // 
            this.ComBox_left3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComBox_left3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComBox_left3.FormattingEnabled = true;
            this.ComBox_left3.Location = new System.Drawing.Point(3, 57);
            this.ComBox_left3.Name = "ComBox_left3";
            this.ComBox_left3.Size = new System.Drawing.Size(150, 21);
            this.ComBox_left3.TabIndex = 49;
            // 
            // ComBox_left2
            // 
            this.ComBox_left2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComBox_left2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComBox_left2.FormattingEnabled = true;
            this.ComBox_left2.Location = new System.Drawing.Point(3, 30);
            this.ComBox_left2.Name = "ComBox_left2";
            this.ComBox_left2.Size = new System.Drawing.Size(150, 21);
            this.ComBox_left2.TabIndex = 47;
            // 
            // Btn_leftAdd
            // 
            this.Btn_leftAdd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_leftAdd.Location = new System.Drawing.Point(6, 133);
            this.Btn_leftAdd.Name = "Btn_leftAdd";
            this.Btn_leftAdd.Size = new System.Drawing.Size(258, 23);
            this.Btn_leftAdd.TabIndex = 55;
            this.Btn_leftAdd.Text = "+";
            this.Btn_leftAdd.UseVisualStyleBackColor = true;
            this.Btn_leftAdd.Click += new System.EventHandler(this.button1_Click);
            // 
            // But_Test
            // 
            this.But_Test.Location = new System.Drawing.Point(894, 337);
            this.But_Test.Name = "But_Test";
            this.But_Test.Size = new System.Drawing.Size(75, 23);
            this.But_Test.TabIndex = 61;
            this.But_Test.Text = "Test Button";
            this.But_Test.UseVisualStyleBackColor = true;
            this.But_Test.Click += new System.EventHandler(this.But_Test_Click);
            // 
            // LearningBasics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1149, 430);
            this.Controls.Add(this.But_Test);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.Grp_passive);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(889, 39);
            this.Name = "LearningBasics";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "Zu Testzwecken; nicht in release enthalten";
            this.Load += new System.EventHandler(this.SettingsDialog_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.Grp_passive.ResumeLayout(false);
            this.LayoutPan_passive.ResumeLayout(false);
            this.LayoutPan_passive.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button showButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button backgroundButton;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.GroupBox Grp_passive;
        private System.Windows.Forms.Label Lbl_SumActive;
        private System.Windows.Forms.TableLayoutPanel LayoutPan_passive;
        private System.Windows.Forms.TextBox TxtBox_left1;
        private System.Windows.Forms.ComboBox ComBox_left1;
        private System.Windows.Forms.TextBox TxtBox_left2;
        private System.Windows.Forms.ComboBox ComBox_left5;
        private System.Windows.Forms.TextBox TxtBox_left4;
        private System.Windows.Forms.TextBox TxtBox_left3;
        private System.Windows.Forms.TextBox TxtBox_left5;
        private System.Windows.Forms.ComboBox ComBox_left4;
        private System.Windows.Forms.ComboBox ComBox_left3;
        private System.Windows.Forms.ComboBox ComBox_left2;
        private System.Windows.Forms.Button Btn_leftAdd;
        private System.Windows.Forms.Button But_Test;
    }
}