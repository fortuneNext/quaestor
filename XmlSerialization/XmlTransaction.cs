﻿using System;
using Quaestor.Logic;

namespace Quaestor.XmlSerialization
{
    public class XmlTransaction
    {
        public XmlTransaction()
        {
        }

        public XmlTransaction(Transaction origin)
        {
            Id = origin.Id;
            Accounts = new string[origin.Accounts.Count];
            Values = new int[origin.Values.Count];
            TimeStamp = origin.TimeStamp;
            ReceiptNr = origin.ReceiptNr;
            Category = origin.Category;
            Comment = origin.Comment;

            for (int i = 0; i < origin.Accounts.Count; i++)
            {
                Accounts[i] = origin.Accounts[i].Name;
                Values[i] = origin.Values[i];
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            XmlTransaction trans = (XmlTransaction)obj;
            if (Id != trans.Id || TimeStamp != trans.TimeStamp || ReceiptNr != trans.ReceiptNr || Category != trans.Category || Comment != trans.Comment)
                return false;
            if (Accounts.Length != trans.Accounts.Length)
                return false;
            for (int i = 0; i < Accounts.Length; i++)
                if (Accounts[i] != trans.Accounts[i] || Values[i] != trans.Values[i])
                    return false;
            return true;
        }

        public int Id { get; set; }

        public string[] Accounts { get; set; }

        public int[] Values { get; set; }

        public DateTime TimeStamp { get; set; }

        public int ReceiptNr { get; set; }

        public string Category { get; set; }

        public string Comment { get; set; }
    }
}