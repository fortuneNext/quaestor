﻿using Quaestor.Logic;

namespace Quaestor.XmlSerialization
{
    public class XmlUserAccount : XmlValueAccount
    {
        public XmlUserAccount()
        {
        }

        public XmlUserAccount(UserAccount origin)
        {
            Name = origin.Name;
            AccountType = origin.AccountType;
            StartValue = origin.GetStartValue();
            PlanValue = origin.GetPlanValue();
            Email = origin.Email;
            PrintToOverview = origin.PrintToOverview;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            XmlUserAccount acc = (XmlUserAccount)obj;
            if (Email != acc.Email)
                return false;
            return base.Equals(obj);
        }

        public string Email { get; set; }
    }
}