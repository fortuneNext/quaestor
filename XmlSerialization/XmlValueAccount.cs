﻿using System.Xml.Serialization;
using Quaestor.Logic;

namespace Quaestor.XmlSerialization
{
    [XmlInclude(typeof (XmlUserAccount))]
    public class XmlValueAccount : XmlAccount
    {
        public XmlValueAccount()
        {
        }

        public XmlValueAccount(ValueAccount origin)
        {
            Name = origin.Name;
            AccountType = origin.AccountType;
            StartValue = origin.GetStartValue();
            PlanValue = origin.GetPlanValue();
            PrintToOverview = origin.PrintToOverview;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            XmlValueAccount acc = (XmlValueAccount)obj;
            if (StartValue != acc.StartValue)
                return false;
            if (PlanValue != acc.PlanValue)
                return false;
            return base.Equals(obj);
        }

        public int StartValue { get; set; }

        public int PlanValue { get; set; }
    }
}