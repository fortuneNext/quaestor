﻿using System.Xml.Serialization;
using Quaestor.Logic;

namespace Quaestor.XmlSerialization
{
    [XmlInclude(typeof (XmlStructuralAccount)), XmlInclude(typeof (XmlValueAccount))]
    public abstract class XmlAccount
    {
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            XmlAccount acc = (XmlAccount)obj;
            if (Name != acc.Name || AccountType != acc.AccountType || PrintToOverview != acc.PrintToOverview)
                return false;
            return true;
        }

        public bool PrintToOverview;
        public string Name { get; set; }

        public AccountTypes AccountType { get; set; }
    }
}