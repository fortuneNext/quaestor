﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Quaestor.XmlSerialization
{
    [XmlInclude(typeof (XmlAccount)), XmlInclude(typeof (XmlTransaction))]
    public class XmlLogic
    {
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            XmlLogic logic = (XmlLogic)obj;
            if (!XmlTimeName.Equals(logic.XmlTimeName) || !XmlAdminName.Equals(logic.XmlAdminName))//This one works
                return false;

            if (XmlAccounts.Count != logic.XmlAccounts.Count)
                return false;
            for (int i = 0; i < XmlAccounts.Count; i++)
                if (!XmlAccounts[i].Equals(logic.XmlAccounts[i]))
                    return false;

            if (XmlOverviewColumns.Count != logic.XmlOverviewColumns.Count)
                return false;
            for (int i = 0; i < XmlOverviewColumns.Count; i++)
                if (XmlOverviewColumns[i] != logic.XmlOverviewColumns[i])
                    return false;

            if (XmlTransactions.Count != logic.XmlTransactions.Count)
                return false;
            for (int i = 0; i < XmlTransactions.Count; i++)
                if (!XmlTransactions[i].Equals(logic.XmlTransactions[i]))
                    return false;
            return true;
        }

        public string XmlAdminName;
        public string XmlTimeName;
        public List<XmlAccount> XmlAccounts { get; set; }
        public List<XmlTransaction> XmlTransactions { get; set; }
        public List<DateTime> XmlOverviewColumns { get; set; }
    }
}