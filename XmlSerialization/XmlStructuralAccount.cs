﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Quaestor.Logic;

namespace Quaestor.XmlSerialization
{
    [XmlInclude(typeof (List<string>))]
    public class XmlStructuralAccount : XmlAccount
    {
        public XmlStructuralAccount()
        {
        }

        public XmlStructuralAccount(StructuralAccount origin)
        {
            Name = origin.Name;
            AccountType = origin.AccountType;
            Subaccounts = new List<string>();
            foreach (Account account in origin.Subaccounts)
            {
                Subaccounts.Add(account.Name);
            }
            PrintToOverview = origin.PrintToOverview;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            XmlStructuralAccount acc = (XmlStructuralAccount)obj;
            if (Subaccounts.Count != acc.Subaccounts.Count)
                return false;
            for(int i = 0; i < Subaccounts.Count; i++)
            if (!Subaccounts[i].Equals(acc.Subaccounts[i]))
                return false;
            return base.Equals(obj);
        }

        public List<string> Subaccounts { get; set; }
    }
}